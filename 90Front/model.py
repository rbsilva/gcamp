import pandas as pd
import os
import sys
sys.path.insert(1, './98Funcao')
import pFuncoes as p
import sFuncoes as f



#===================================================================================================== 
    # Incializações
    #===================================================================================================== 
diratual   = os.path.dirname(__file__)    
direntrada = p.MontaDirAbaixo(diratual,'02Entrada')
dirlog     = p.MontaDirAbaixo(diratual,'99Log')    
arqjson    = os.path.join(direntrada,'conexaoGCamp.json')      

    #===================================================================================================== 
    # Dados (Entrada)
    #=====================================================================================================         
parametros  = f.leituraJson(arqjson,False)     
    #===================================================================================================== 
    # Banco de dados
    #===================================================================================================== 
host_bd      = parametros['host']
database_bd  = parametros['database']
user_bd      = parametros['user']
password_bd  = parametros['password']
port_bd      = parametros['porta']
enderco_log  = parametros['enderecoLog']

######################################## PROJETO #######################################################

def listaProjeto():
    select_projeto = '''SELECT cdProjeto,dsProjeto, nmConexaoA5D FROM public.projeto order by cdProjeto desc'''
    lista,res = p.consulta_bd(select_projeto, host_bd,database_bd,user_bd,password_bd)
    print(res)
    df = pd.DataFrame(lista, columns=["id", "nome","Conexao"])
    tamanho = len(df)
    id = []
    nome = []
    conexao = []
    for i in range(0, tamanho):
        id.append(df['id'][i])
        nome.append(df['nome'][i])
        conexao.append(df['Conexao'][i])

    return id, nome, conexao

def inserirProjeto(dsProjeto,nmConexaoA5D,nmA5DHost,nmA5DDatabase,nmA5DTabela,nmA5DPorta,nmA5DLogin,nmA5DSenha,cdA5DCampanha,dsSelect):
    insert = f'''INSERT INTO public.projeto(
	dsprojeto, nmconexaoA5D, nmA5Dhost, nmA5Ddatabase, nmA5Dtabela, nmA5Dporta, nmA5Dlogin, nmA5Dsenha, cdA5Dcampanha, dsA5Dselect)
	VALUES ('{dsProjeto}', '{nmConexaoA5D}', '{nmA5DHost}', '{nmA5DDatabase}', '{nmA5DTabela}', '{nmA5DPorta}', '{nmA5DLogin}', '{nmA5DSenha}', {cdA5DCampanha},  '{dsSelect}');'''
    resInsert = p.inserir_bd(insert, host_bd,database_bd,user_bd,password_bd)
    return resInsert

def detalheProjeto(id):
    query = f'''SELECT dsProjeto,nmConexaoA5D,nmA5DHost,nmA5DDatabase,nmA5DTabela,nmA5DPorta,nmA5DLogin,nmA5DSenha,cdA5DCampanha,dsa5dSelect from Projeto where cdProjeto = {id} '''

    lista,res = p.consulta_bd(query, host_bd,database_bd,user_bd,password_bd)
    print(res)
    df = pd.DataFrame(lista, columns=['dsProjeto','nmConexaoA5D','nmA5DHost','nmA5DDatabase','nmA5DTabela','nmA5DPorta','nmA5DLogin','nmA5DSenha','cdA5DCampanha','dsSelect'])

    tamanho = len(df)
    dsProjeto=[]
    nmConexaoA5D=[]
    nmA5DHost=[]
    nmA5DDatabase=[]
    nmA5DTabela=[]
    nmA5DPorta=[]
    nmA5DLogin=[]
    nmA5DSenha=[]
    cdA5DCampanha=[]
    dsSelect=[]


    for i in range(0, tamanho):
        dsProjeto.append(df['dsProjeto'][i])
        nmConexaoA5D.append(df['nmConexaoA5D'][i])
        nmA5DHost.append(df['nmA5DHost'][i])
        nmA5DDatabase.append(df['nmA5DDatabase'][i])
        nmA5DTabela.append(df['nmA5DTabela'][i])
        nmA5DPorta.append(df['nmA5DPorta'][i])
        nmA5DLogin.append(df['nmA5DLogin'][i])
        nmA5DSenha.append(df['nmA5DSenha'][i])
        cdA5DCampanha.append(df['cdA5DCampanha'][i])
        dsSelect.append(df['dsSelect'][i])


    return dsProjeto,nmConexaoA5D,nmA5DHost,nmA5DDatabase,nmA5DTabela,nmA5DPorta,nmA5DLogin,nmA5DSenha,cdA5DCampanha,dsSelect

def updateProjeto(id, dsProjeto,nmConexaoA5D,nmA5DHost,nmA5DDatabase,nmA5DTabela,nmA5DPorta,nmA5DLogin,nmA5DSenha,cdA5DCampanha,dsSelect):
    update = f'''UPDATE public.projeto set
	dsprojeto='{dsProjeto}', nmconexaoA5D='{nmConexaoA5D}', nmA5Dhost='{nmA5DHost}', nmA5Ddatabase='{nmA5DDatabase}', nmA5Dtabela='{nmA5DTabela}', nmA5Dporta='{nmA5DPorta}', nmA5Dlogin='{nmA5DLogin}', nmA5Dsenha='{nmA5DSenha}', cdA5Dcampanha={cdA5DCampanha}, dsA5Dselect='{dsSelect}' where cdprojeto = {id}'''
	
    resInsert = p.inserir_bd(update, host_bd,database_bd,user_bd,password_bd)
    return resInsert


def deleteProjeto(id):
    delete_projeto = f"DELETE FROM public.projeto WHERE cdprojeto = {id}"
    delete = p.inserir_bd(delete_projeto,host_bd,database_bd,user_bd,password_bd)
    return delete
######################################## CAMPANHA #######################################################
def listaCampanha():
    select_projeto = '''SELECT cdcampanha,dscampanha, dtinicio, dtfim FROM public.campanha order by cdcampanha desc'''
    lista,res = p.consulta_bd(select_projeto, host_bd,database_bd,user_bd,password_bd)
    print(res)
    df = pd.DataFrame(lista, columns=["cdcampanha", "dscampanha","dtinicio","dtfim"])
    tamanho = len(df)
    cdcampanha = []
    dscampanha = []
    dtinicio = []
    dtfim = []
    for i in range(0, tamanho):
        cdcampanha.append(df['cdcampanha'][i])
        dscampanha.append(df['dscampanha'][i])
        dtinicio.append(df['dtinicio'][i])
        dtfim.append(df['dtfim'][i])

    return cdcampanha,dscampanha,dtinicio,dtfim


def inserirCampanha(cdcampanha, dscampanha, dtinicio, dtfim, hrinicio, hrfim, cdtipoacesso, dsdiscopath, dsdiscofile, nmbasehost, nmbasedatabase, nmbasedtabela, nmbasedporta, nmbasedlogin, nmbasedsenha, qtcelulas, qtleads, qtexecutados, cdtipocampanha, cdstatus, dsarqseparador):
    insert = f'''INSERT INTO public.campanha(
	cdcampanha, dscampanha, dtinicio, dtfim, hrinicio, hrfim, cdtipoacesso, dsdiscopath, dsdiscofile, nmbasehost, nmbasedatabase, nmbasedtabela, nmbasedporta, nmbasedlogin, nmbasedsenha, qtcelulas, qtleads, qtexecutados, cdtipocampanha, cdstatus, dsarqseparador)
	VALUES ({cdcampanha}, '{dscampanha}', '{dtinicio}', '{dtfim}', '{hrinicio}', '{hrfim}', {cdtipoacesso}, '{dsdiscopath}', '{dsdiscofile}', '{nmbasehost}', '{nmbasedatabase}', '{nmbasedtabela}', '{nmbasedporta}', '{nmbasedlogin}', '{nmbasedsenha}', '{qtcelulas}', '{qtleads}', '{qtexecutados}', {cdtipocampanha}, {cdstatus}, '{dsarqseparador}');'''
    resInsert = p.inserir_bd(insert,host_bd,database_bd,user_bd,password_bd)
    return resInsert


def detalheCampanha(id):
    query = f'''SELECT cdcampanha, dscampanha,dtinicio,dtfim,hrinicio,hrfim,cdtipoacesso,dsdiscopath,dsdiscofile,nmbasehost,nmbasedatabase,nmbasedtabela,nmbasedporta,nmbasedlogin,nmbasedsenha,qtcelulas,qtleads,qtexecutados,cdtipocampanha,cdstatus,dsarqseparador from campanha where cdcampanha =  {id} '''
    

    lista,res = p.consulta_bd(query, host_bd,database_bd,user_bd,password_bd)
    print(res)
    df = pd.DataFrame(lista, columns=['cdcampanha','dscampanha','dtinicio','dtfim','hrinicio','hrfim','cdtipoacesso','dsdiscopath','dsdiscofile','nmbasehost','nmbasedatabase','nmbasedtabela','nmbasedporta','nmbasedlogin','nmbasedsenha','qtcelulas','qtleads','qtexecutados','cdtipocampanha','cdstatus','dsarqseparador'])

    tamanho = len(df)
    cdcampanha=[]
    dscampanha=[]
    dtinicio=[]
    dtfim=[]
    hrinicio=[]
    hrfim=[]
    cdtipoacesso=[]
    dsdiscopath=[]
    dsdiscofile=[]
    nmbasehost=[]
    nmbasedatabase=[]
    nmbasedtabela=[]
    nmbasedporta=[]
    nmbasedlogin=[]
    nmbasedsenha=[]
    qtcelulas=[]
    qtleads=[]
    qtexecutados=[]
    cdtipocampanha=[]
    cdstatus=[]
    dsarqseparador=[]



    for i in range(0, tamanho):
        cdcampanha.append(df['cdcampanha'][i])
        dscampanha.append(df['dscampanha'][i])
        dtinicio.append(df['dtinicio'][i])
        dtfim.append(df['dtfim'][i])
        hrinicio.append(df['hrinicio'][i])
        hrfim.append(df['hrfim'][i])
        cdtipoacesso.append(df['cdtipoacesso'][i])
        dsdiscopath.append(df['dsdiscopath'][i])
        dsdiscofile.append(df['dsdiscofile'][i])
        nmbasehost.append(df['nmbasehost'][i])
        nmbasedatabase.append(df['nmbasedatabase'][i])
        nmbasedtabela.append(df['nmbasedtabela'][i])
        nmbasedporta.append(df['nmbasedporta'][i])
        nmbasedlogin.append(df['nmbasedlogin'][i])
        nmbasedsenha.append(df['nmbasedsenha'][i])
        qtcelulas.append(df['qtcelulas'][i])
        qtleads.append(df['qtleads'][i])
        qtexecutados.append(df['qtexecutados'][i])
        cdtipocampanha.append(df['cdtipocampanha'][i])
        cdstatus.append(df['cdstatus'][i])
        dsarqseparador.append(df['dsarqseparador'][i])



    return cdcampanha,dscampanha,dtinicio,dtfim,hrinicio,hrfim,cdtipoacesso,dsdiscopath,dsdiscofile,nmbasehost,nmbasedatabase,nmbasedtabela,nmbasedporta,nmbasedlogin,nmbasedsenha,qtcelulas,qtleads,qtexecutados,cdtipocampanha,cdstatus,dsarqseparador

def updateCampanha(id,cdcampanha, dscampanha,dtinicio,dtfim,hrinicio,hrfim,cdtipoacesso,dsdiscopath,dsdiscofile,nmbasehost,nmbasedatabase,nmbasedtabela,nmbasedporta,nmbasedlogin,nmbasedsenha,qtcelulas,qtleads,qtexecutados,cdtipocampanha,cdstatus,dsarqseparador):
    update = f'''UPDATE public.campanha set cdcampanha={cdcampanha},
	dscampanha='{dscampanha}', dtinicio='{dtinicio}', dtfim='{dtfim}', hrinicio='{hrinicio}', hrfim='{hrfim}', cdtipoacesso={cdtipoacesso}, dsdiscopath='{dsdiscopath}', dsdiscofile='{dsdiscofile}', nmbasehost='{nmbasehost}', nmbasedatabase='{nmbasedatabase}', nmbasedtabela='{nmbasedtabela}', nmbasedporta='{nmbasedporta}', nmbasedlogin='{nmbasedlogin}', nmbasedsenha='{nmbasedsenha}', qtcelulas='{qtcelulas}', qtleads='{qtleads}', qtexecutados='{qtexecutados}', cdtipocampanha={cdtipocampanha}, cdstatus={cdstatus}, dsarqseparador='{dsarqseparador}' where cdcampanha = {id}'''
	
    resInsert = p.inserir_bd(update, host_bd,database_bd,user_bd,password_bd)
    return resInsert




def deleteCampanha(id):
    delete_projeto = f"DELETE FROM public.campanha WHERE cdcampanha = {id}"
    delete = p.inserir_bd(delete_projeto,host_bd,database_bd,user_bd,password_bd)
    return delete

######################################## CÉLULA #######################################################
def listaCelula():
    select_projeto = '''SELECT cdcelula,dscelula,cdcampanha, dtinicio, dtfim FROM public.celula order by cdcelula desc'''
    lista,res = p.consulta_bd(select_projeto,host_bd,database_bd,user_bd,password_bd)
    print(res)
    df = pd.DataFrame(lista, columns=["cdcelula", "dscelula","cdcampanha","dtinicio","dtfim"])
    tamanho = len(df)
    cdcelula = []
    dscelula = []
    cdcampanha = []
    dtinicio = []
    dtfim = []
    for i in range(0, tamanho):
        cdcelula.append(df['cdcelula'][i])
        dscelula.append(df['dscelula'][i])
        cdcampanha.append(df['cdcampanha'][i])
        dtinicio.append(df['dtinicio'][i])
        dtfim.append(df['dtfim'][i])

    return cdcelula,dscelula,cdcampanha,dtinicio,dtfim


def inserirCelula(cdcampanha,dscelula, dtinicio, dtfim, hrinicio, hrfim, qtexecutados, qtleads, qtdiscagens, cdestrategiadiscagem, cdtipocelula, cdcanal, cdstatus):
    qtleads = 0
    qtdiscagens = 0
    insert = f'''INSERT INTO public.celula(
	cdcampanha, dscelula, dtinicio, dtfim, hrinicio, hrfim, qtexecutados, qtleads, qtdiscagens, cdestrategiadiscagem, cdtipocelula, cdcanal, cdstatus)
	VALUES ({cdcampanha}, '{dscelula}', '{dtinicio}', '{dtfim}', '{hrinicio}', '{hrfim}', '{qtexecutados}', '{qtleads}', '{qtdiscagens}', {cdestrategiadiscagem}, {cdtipocelula}, {cdcanal}, {cdstatus});'''
    resInsert = p.inserir_bd(insert,host_bd,database_bd,user_bd,password_bd)
    
    return resInsert    


def detalheCelula(id):
    query = f'''SELECT cdcampanha, cdcelula, dscelula, dtinicio, dtfim, hrinicio, hrfim, qtexecutados, qtleads, qtdiscagens, cdestrategiadiscagem, cdtipocelula, cdcanal, cdstatus from celula where cdcelula =  {id} '''
    

    lista,res = p.consulta_bd(query, host_bd,database_bd,user_bd,password_bd)
    print(res)
    df = pd.DataFrame(lista, columns=['cdcampanha','cdcelula','dscelula','dtinicio','dtfim','hrinicio','hrfim','qtexecutados','qtleads','qtdiscagens','cdestrategiadiscagem','cdtipocelula','cdcanal','cdstatus'])

    tamanho = len(df)
    cdcampanha=[]
    cdcelula=[]
    dscelula=[]
    dtinicio=[]
    dtfim=[]
    hrinicio=[]
    hrfim=[]
    qtexecutados=[]
    qtleads=[]
    qtdiscagens=[]
    cdestrategiadiscagem=[]
    cdtipocelula=[]
    cdcanal=[]
    cdstatus=[]

    for i in range(0, tamanho):
        cdcampanha.append(df['cdcampanha'][i])
        cdcelula.append(df['cdcelula'][i])
        dscelula.append(df['dscelula'][i])
        dtinicio.append(df['dtinicio'][i])
        dtfim.append(df['dtfim'][i])
        hrinicio.append(df['hrinicio'][i])
        hrfim.append(df['hrfim'][i])
        qtexecutados.append(df['qtexecutados'][i])
        qtleads.append(df['qtleads'][i])
        qtdiscagens.append(df['qtdiscagens'][i])
        cdestrategiadiscagem.append(df['cdestrategiadiscagem'][i])
        cdtipocelula.append(df['cdtipocelula'][i])
        cdcanal.append(df['cdcanal'][i])
        cdstatus.append(df['cdstatus'][i])


    return cdcampanha, cdcelula, dscelula, dtinicio, dtfim, hrinicio, hrfim, qtexecutados, qtleads, qtdiscagens, cdestrategiadiscagem, cdtipocelula, cdcanal, cdstatus

def updateCelula(id,cdcampanha, cdcelula, dscelula, dtinicio, dtfim, hrinicio, hrfim, qtexecutados, qtleads, qtdiscagens, cdestrategiadiscagem, cdtipocelula, cdcanal, cdstatus):
    update = f'''UPDATE public.celula set cdcampanha = {cdcampanha} , cdcelula = {cdcelula}, dscelula='{dscelula}', dtinicio= '{dtinicio}', dtfim='{dtfim}', hrinicio='{hrinicio}', hrfim='{hrfim}', qtexecutados='{qtexecutados}', qtleads='{qtleads}', qtdiscagens='{qtdiscagens}', cdestrategiadiscagem={cdestrategiadiscagem}, cdtipocelula={cdtipocelula}, cdcanal={cdcanal}, cdstatus={cdstatus} where cdcelula= {id};'''
    print(update)
    resInsert = p.inserir_bd(update, host_bd,database_bd,user_bd,password_bd)
    return resInsert



def deleteCelula(id):
    delete_projeto = f"DELETE FROM public.celula WHERE cdcelula = {id}"
    delete = p.inserir_bd(delete_projeto,host_bd,database_bd,user_bd,password_bd)
    return delete

######################################## RELACIONAMENTO #######################################################
def listaRelacionamento():
    select_projeto = '''SELECT cdrelacionamento, cdcampanha, cdcelula, cdcelulapredecessora, dsquery, cdfiltro, cdprioridade FROM public.relacionamentocelulas order by cdrelacionamento desc'''
    lista, res = p.consulta_bd(select_projeto,host_bd,database_bd,user_bd,password_bd)
    print(res)
    df = pd.DataFrame(lista, columns=["cdrelacionamento","cdcampanha", "cdcelula", "cdcelulapredecessora", "dsquery", "cdfiltro", "cdprioridade"])
    tamanho = len(df)
    cdrelacionamento = []
    cdcampanha = []
    cdcelula  = []
    cdcelulapredecessora = []
    dsquery = []
    cdfiltro = []
    cdprioridade = []
    for i in range(0, tamanho):
        cdrelacionamento.append(df['cdrelacionamento'][i])
        cdcampanha.append(df['cdcampanha'][i])
        cdcelula.append(df['cdcelula'][i])
        cdcelulapredecessora.append(df['cdcelulapredecessora'][i])
        dsquery.append(df['dsquery'][i])
        cdfiltro.append(df['cdfiltro'][i])
        cdprioridade.append(df['cdprioridade'][i])
    return cdrelacionamento,cdcampanha,cdcelula,cdcelulapredecessora,dsquery,cdfiltro,cdprioridade


def inserirRelacionamento(cdcampanha,cdcelula,cdcelulapredecessora,dsquery,cdfiltro,cdprioridade):
    q = dsquery.split("'")
    query = "''".join(q)
    

    insert = f'''INSERT INTO public.relacionamentocelulas(
	cdcampanha,cdcelula,cdcelulapredecessora,dsquery,cdfiltro,cdprioridade)
	VALUES ('{cdcampanha}','{cdcelula}','{cdcelulapredecessora}','{query}','{cdfiltro}','{cdprioridade}');'''
    print(insert)
    resInsert = p.inserir_bd(insert,host_bd,database_bd,user_bd,password_bd)
   
    return resInsert       


def detalheRelacionamento(id):
    select_projeto = f'''SELECT cdcampanha, cdcelula, cdcelulapredecessora, dsquery, cdfiltro, cdprioridade FROM public.relacionamentocelulas where cdrelacionamento = {id}'''
    lista, res = p.consulta_bd(select_projeto,host_bd,database_bd,user_bd,password_bd)
    print(res)
    df = pd.DataFrame(lista, columns=["cdcampanha", "cdcelula", "cdcelulapredecessora", "dsquery", "cdfiltro", "cdprioridade"])
    tamanho = len(df)
    cdcampanha = []
    cdcelula  = []
    cdcelulapredecessora = []
    dsquery = []
    cdfiltro = []
    cdprioridade = []
    for i in range(0, tamanho):
        cdcampanha.append(df['cdcampanha'][i])
        cdcelula.append(df['cdcelula'][i])
        cdcelulapredecessora.append(df['cdcelulapredecessora'][i])
        dsquery.append(df['dsquery'][i])
        cdfiltro.append(df['cdfiltro'][i])
        cdprioridade.append(df['cdprioridade'][i])
    return cdcampanha,cdcelula,cdcelulapredecessora,dsquery,cdfiltro,cdprioridade

def updateRelacionamento(id, cdcampanha,cdcelula,cdcelulapredecessora,dsquery,cdfiltro,cdprioridade):
    q = dsquery.split("'")
    query = "''".join(q)
    insert = f'''UPDATE public.relacionamentocelulas set
	cdcampanha='{cdcampanha}',cdcelula='{cdcelula}',cdcelulapredecessora='{cdcelulapredecessora}',dsquery='{query}',cdfiltro='{cdfiltro}',cdprioridade='{cdprioridade}' WHERE cdrelacionamento = {id};'''
    resInsert = p.inserir_bd(insert,host_bd,database_bd,user_bd,password_bd)
    print(insert)
    return resInsert 


def deleteRelacionamento(id):
    delete_projeto = f"DELETE FROM public.relacionamentocelulas WHERE cdrelacionamento = {id}"
    delete = p.inserir_bd(delete_projeto,host_bd,database_bd,user_bd,password_bd)
    return delete

######################################## ESTRATÉGIA DISCAGEM #######################################################
def listaEstrategia():
    select_projeto = '''SELECT cdestrategiadiscagem, dsmovimento, qtligacoestel01, qtligacoestel02, qtligacoestel03 FROM public.estrategiadiscagem;'''
    lista, res = p.consulta_bd(select_projeto,host_bd,database_bd,user_bd,password_bd)
    print(res)
    df = pd.DataFrame(lista, columns=["cdestrategiadiscagem", "dsmovimento", "qtligacoestel01", "qtligacoestel02", "qtligacoestel03"])
    tamanho = len(df)
    cdestrategiadiscagem = []
    dsmovimento  = []
    qtligacoestel01 = []
    qtligacoestel02 = []
    qtligacoestel03 = []
    for i in range(0, tamanho):
        cdestrategiadiscagem.append(df['cdestrategiadiscagem'][i])
        dsmovimento.append(df['dsmovimento'][i])
        qtligacoestel01.append(df['qtligacoestel01'][i])
        qtligacoestel02.append(df['qtligacoestel02'][i])
        qtligacoestel03.append(df['qtligacoestel03'][i])
        
    return cdestrategiadiscagem,dsmovimento,qtligacoestel01,qtligacoestel02,qtligacoestel03


def inserirEstrategia(dsmovimento, qtligacoestel01, qtligacoestel02, qtligacoestel03, qtligacoestel04, qtligacoestel05, qtligacoestel06):
    insert = f'''INSERT INTO public.estrategiadiscagem(
	dsmovimento, qtligacoestel01, qtligacoestel02, qtligacoestel03, qtligacoestel04, qtligacoestel05, qtligacoestel06)
	VALUES ('{dsmovimento}', '{qtligacoestel01}', '{qtligacoestel02}', '{qtligacoestel03}', '{qtligacoestel04}', '{qtligacoestel05}', '{qtligacoestel06}');'''
    resInsert = p.inserir_bd(insert,host_bd,database_bd,user_bd,password_bd)
    return resInsert           


def detalheEstrategia(id):
    select_projeto = '''SELECT cdestrategiadiscagem, dsmovimento, qtligacoestel01, qtligacoestel02, qtligacoestel03, qtligacoestel04, qtligacoestel05,qtligacoestel06 FROM public.estrategiadiscagem;'''
    lista, res = p.consulta_bd(select_projeto,host_bd,database_bd,user_bd,password_bd)
    print(res)
    df = pd.DataFrame(lista, columns=["cdestrategiadiscagem", "dsmovimento", "qtligacoestel01", "qtligacoestel02", "qtligacoestel03","qtligacoestel04","qtligacoestel05","qtligacoestel06"])
    tamanho = len(df)
    cdestrategiadiscagem = []
    dsmovimento  = []
    qtligacoestel01 = []
    qtligacoestel02 = []
    qtligacoestel03 = []
    qtligacoestel04 = []
    qtligacoestel05 = []
    qtligacoestel06 = []
    for i in range(0, tamanho):
        cdestrategiadiscagem.append(df['cdestrategiadiscagem'][i])
        dsmovimento.append(df['dsmovimento'][i])
        qtligacoestel01.append(df['qtligacoestel01'][i])
        qtligacoestel02.append(df['qtligacoestel02'][i])
        qtligacoestel03.append(df['qtligacoestel03'][i])
        qtligacoestel04.append(df['qtligacoestel04'][i])
        qtligacoestel05.append(df['qtligacoestel05'][i])
        qtligacoestel06.append(df['qtligacoestel06'][i])
        
    return cdestrategiadiscagem,dsmovimento,qtligacoestel01,qtligacoestel02,qtligacoestel03,qtligacoestel04,qtligacoestel05,qtligacoestel06

def updateEstrategia(id,dsmovimento, qtligacoestel01, qtligacoestel02, qtligacoestel03, qtligacoestel04, qtligacoestel05, qtligacoestel06):
    insert = f'''UPDATE public.estrategiadiscagem SET
	dsmovimento='{dsmovimento}', qtligacoestel01='{qtligacoestel01}', qtligacoestel02='{qtligacoestel02}', qtligacoestel03='{qtligacoestel03}', qtligacoestel04='{qtligacoestel04}', qtligacoestel05='{qtligacoestel05}', qtligacoestel06='{qtligacoestel06}' WHERE cdestrategiadiscagem = {id};'''
    resInsert = p.inserir_bd(insert,host_bd,database_bd,user_bd,password_bd)
    return resInsert  

def deleteEstrategia(id):
    delete_projeto = f"DELETE FROM public.estrategiadiscagem WHERE cdestrategiadiscagem = {id}"
    delete = p.inserir_bd(delete_projeto,host_bd,database_bd,user_bd,password_bd)
    return delete

######################################## CANAL #######################################################
def listaCanal():
    select_projeto = '''SELECT cdcanal, dscanal FROM public.canal;'''
    lista, res = p.consulta_bd(select_projeto,host_bd,database_bd,user_bd,password_bd)
    print(res)
    df = pd.DataFrame(lista, columns=["cdcanal", "dscanal"])
    tamanho = len(df)
    cdcanal = []
    dscanal  = []
   
    for i in range(0, tamanho):
        cdcanal.append(df['cdcanal'][i])
        dscanal.append(df['dscanal'][i])
        
        
    return cdcanal, dscanal


def inserirCanal(dscanal):
    insert = f'''INSERT INTO public.canal(
	dscanal)
	VALUES ('{dscanal}');'''
    resInsert = p.inserir_bd(insert,host_bd,database_bd,user_bd,password_bd)
    return resInsert


def detalheCanal(id):
    select_projeto = f'''SELECT cdcanal, dscanal FROM public.canal where cdcanal = {id};'''
    lista, res = p.consulta_bd(select_projeto,host_bd,database_bd,user_bd,password_bd)
    print(res)
    df = pd.DataFrame(lista, columns=["cdcanal", "dscanal"])
    tamanho = len(df)
    cdcanal = []
    dscanal  = []
   
    for i in range(0, tamanho):
        cdcanal.append(df['cdcanal'][i])
        dscanal.append(df['dscanal'][i])
        
        
    return cdcanal, dscanal


def updateCanal(id,dscanal):
    insert = f'''Update public.canal set
	dscanal = '{dscanal}' where cdcanal = {id};'''
    resInsert = p.inserir_bd(insert,host_bd,database_bd,user_bd,password_bd)
    return resInsert


def deleteCanal(id):
    delete_projeto = f"DELETE FROM public.canal WHERE cdcanal = {id}"
    delete = p.inserir_bd(delete_projeto,host_bd,database_bd,user_bd,password_bd)
    return delete

######################################## status #######################################################
def listaStatus():
    select_projeto = '''SELECT cdstatus, dsstatus FROM public.status;'''
    lista, res = p.consulta_bd(select_projeto,host_bd,database_bd,user_bd,password_bd)
    print(res)
    df = pd.DataFrame(lista, columns=["cdstatus", "dsstatus"])
    tamanho = len(df)
    cdstatus = []
    dsstatus  = []
   
    for i in range(0, tamanho):
        cdstatus.append(df['cdstatus'][i])
        dsstatus.append(df['dsstatus'][i])
        
        
    return cdstatus, dsstatus


def inserirStatus(dsstatus):
    insert = f'''INSERT INTO public.status(
	dsstatus)
	VALUES ('{dsstatus}');'''
    resInsert = p.inserir_bd(insert,host_bd,database_bd,user_bd,password_bd)
    return resInsert


def detalheStatus(id):
    select_projeto = f'''SELECT cdstatus, dsstatus FROM public.status where cdstatus = {id};'''
    lista, res = p.consulta_bd(select_projeto,host_bd,database_bd,user_bd,password_bd)
    print(res)
    df = pd.DataFrame(lista, columns=["cdstatus", "dsstatus"])
    tamanho = len(df)
    cdstatus = []
    dsstatus  = []
   
    for i in range(0, tamanho):
        cdstatus.append(df['cdstatus'][i])
        dsstatus.append(df['dsstatus'][i])
        
        
    return cdstatus, dsstatus


def updateStatus(id,dsstatus):
    insert = f'''Update public.status set
	dsstatus = '{dsstatus}' where cdstatus = {id};'''
    resInsert = p.inserir_bd(insert,host_bd,database_bd,user_bd,password_bd)
    return resInsert


def deleteStatus(id):
    delete_projeto = f"DELETE FROM public.status WHERE cdstatus = {id}"
    delete = p.inserir_bd(delete_projeto,host_bd,database_bd,user_bd,password_bd)
    return delete

######################################## tipoacesso #######################################################
def listatipoacesso():
    select_projeto = '''SELECT cdtipoacesso, dstipoacesso FROM public.tipoacesso;'''
    lista, res = p.consulta_bd(select_projeto,host_bd,database_bd,user_bd,password_bd)
    print(res)
    df = pd.DataFrame(lista, columns=["cdtipoacesso", "dstipoacesso"])
    tamanho = len(df)
    cdtipoacesso = []
    dstipoacesso  = []
   
    for i in range(0, tamanho):
        cdtipoacesso.append(df['cdtipoacesso'][i])
        dstipoacesso.append(df['dstipoacesso'][i])
        
        
    return cdtipoacesso, dstipoacesso


def inserirtipoacesso(dstipoacesso):
    insert = f'''INSERT INTO public.tipoacesso(
	dstipoacesso)
	VALUES ('{dstipoacesso}');'''
    resInsert = p.inserir_bd(insert,host_bd,database_bd,user_bd,password_bd)
    return resInsert    

def detalhetipoacesso(id):
    select_projeto = f'''SELECT cdtipoacesso, dstipoacesso FROM public.tipoacesso where cdtipoacesso = {id};'''
    lista, res = p.consulta_bd(select_projeto,host_bd,database_bd,user_bd,password_bd)
    print(res)
    df = pd.DataFrame(lista, columns=["cdtipoacesso", "dstipoacesso"])
    tamanho = len(df)
    cdtipoacesso = []
    dstipoacesso  = []
   
    for i in range(0, tamanho):
        cdtipoacesso.append(df['cdtipoacesso'][i])
        dstipoacesso.append(df['dstipoacesso'][i])
        
        
    return cdtipoacesso, dstipoacesso


def updatetipoacesso(id,dstipoacesso):
    insert = f'''Update public.tipoacesso set
	dstipoacesso = '{dstipoacesso}' where cdtipoacesso = {id};'''
    resInsert = p.inserir_bd(insert,host_bd,database_bd,user_bd,password_bd)
    return resInsert

def deletetipoacesso(id):
    delete_projeto = f"DELETE FROM public.tipoacesso WHERE cdtipoacesso = {id}"
    delete = p.inserir_bd(delete_projeto,host_bd,database_bd,user_bd,password_bd)
    return delete

######################################## tipocelula #######################################################
def listatipocelula():
    select_projeto = '''SELECT cdtipocelula, dstipocelula FROM public.tipocelula;'''
    lista, res = p.consulta_bd(select_projeto,host_bd,database_bd,user_bd,password_bd)
    print(res)
    df = pd.DataFrame(lista, columns=["cdtipocelula", "dstipocelula"])
    tamanho = len(df)
    cdtipocelula = []
    dstipocelula  = []
   
    for i in range(0, tamanho):
        cdtipocelula.append(df['cdtipocelula'][i])
        dstipocelula.append(df['dstipocelula'][i])
        
        
    return cdtipocelula, dstipocelula


def inserirtipocelula(dstipocelula):
    insert = f'''INSERT INTO public.tipocelula(
	dstipocelula)
	VALUES ('{dstipocelula}');'''
    resInsert = p.inserir_bd(insert,host_bd,database_bd,user_bd,password_bd)
    return resInsert 


def detalhetipocelula(id):
    select_projeto = f'''SELECT cdtipocelula, dstipocelula FROM public.tipocelula where cdtipocelula = {id};'''
    lista, res = p.consulta_bd(select_projeto,host_bd,database_bd,user_bd,password_bd)
    print(res)
    df = pd.DataFrame(lista, columns=["cdtipocelula", "dstipocelula"])
    tamanho = len(df)
    cdtipocelula = []
    dstipocelula  = []
   
    for i in range(0, tamanho):
        cdtipocelula.append(df['cdtipocelula'][i])
        dstipocelula.append(df['dstipocelula'][i])
        
        
    return cdtipocelula, dstipocelula


def updatetipocelula(id,dstipocelula):
    insert = f'''Update public.tipocelula set
	dstipocelula = '{dstipocelula}' where cdtipocelula = {id};'''
    resInsert = p.inserir_bd(insert,host_bd,database_bd,user_bd,password_bd)
    return resInsert


def deletetipocelula(id):
    delete_projeto = f"DELETE FROM public.tipocelula WHERE cdtipocelula = {id}"
    delete = p.inserir_bd(delete_projeto,host_bd,database_bd,user_bd,password_bd)
    return delete    



def quantidadeKPI(tabela):
    select = f'''SELECT count(*) FROM public.{tabela};'''
    lista, res = p.consulta_bd(select,host_bd,database_bd,user_bd,password_bd)
    print(res)
    df = pd.DataFrame(lista, columns=["qtd"])
    tamanho = len(df)
    qtd = []
   
   
    for i in range(0, tamanho):
        qtd.append(df['qtd'][i])
        

    return qtd