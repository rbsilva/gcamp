#====================================================================================
# Aplicação Front-end para o CRUD das tabelas do projeto
# Dev: Romulo Brandão
# Date: 06/06/2022
#====================================================================================
from flask import Flask, render_template, request, redirect, session, flash, url_for
import sys
sys.path.insert(1, './98Funcao')
import sFuncoes as f
import pFuncoes as p
import pandas as pd
import model as m

app = Flask(__name__)
app.secret_key = 'EasyCampaign'

@app.route('/', methods=['GET','POST'])
def login():
    if request.method == "GET":
        session['usuario'] = None
        projeto = m.quantidadeKPI('projeto')
        campanha = m.quantidadeKPI('campanha')
        celula = m.quantidadeKPI('celula')
        relacionamento = m.quantidadeKPI('relacionamentocelulas')
        estrategiadiscagem = m.quantidadeKPI('estrategiadiscagem')
        canal = m.quantidadeKPI('canal')
        status = m.quantidadeKPI('status')
        tipoacesso = m.quantidadeKPI('tipoacesso')
        tipocelula = m.quantidadeKPI('tipocelula')
        return render_template('home.html', titulo = 'EasyCampaign', relacionamento=relacionamento, projeto=projeto, campanha=campanha, celula=celula, estrategiadiscagem=estrategiadiscagem, canal=canal, status=status,tipoacesso=tipoacesso,tipocelula=tipocelula)

##################### PROJETO - LISTAR OK, CADASTRAR OK, DELETAR OK E ALTERAR OK #########################################
@app.route("/projeto")
def projetoTela():
    id, nome, conexao = m.listaProjeto()
    tamanho = len(id)
    return render_template('projeto.html', titulo = 'Tela de Projetos', id = id, nome = nome, conexao = conexao, tamanho = tamanho)

@app.route("/projeto/cadastro", methods=['GET','POST'])
def projetoCadastro():
    if request.method == "GET":
        return render_template('projeto_cadastro.html', titulo = "Cadastro de Projetos")
    else:
        dsProjeto      = request.form.get('dsprojeto')
        nmConexaoA5D   = request.form.get('nmConexaoA5D')
        nmA5DHost      = request.form.get('nmA5DHost')
        nmA5DDatabase  = request.form.get('nmA5DDatabase')
        nmA5DTabela    = request.form.get('nmA5DTabela')
        nmA5DPorta     = request.form.get('nmA5DPorta')
        nmA5DLogin     = request.form.get('nmA5DLogin')
        nmA5DSenha     = request.form.get('nmA5DSenha')
        cdA5DCampanha  = request.form.get('cdA5DCampanha')
        dsSelect       = request.form.get('dsSelect')
        insert         = m.inserirProjeto(dsProjeto,
                                          nmConexaoA5D,
                                          nmA5DHost,
                                          nmA5DDatabase,
                                          nmA5DTabela,
                                          nmA5DPorta,
                                          nmA5DLogin,
                                          nmA5DSenha,
                                          cdA5DCampanha,
                                          dsSelect)
        #flash('Acesso negado! Usuário ou senha não coincidem.')
        if insert == 'Ok':
            flash('Projeto criado com sucesso!')
            return redirect(url_for('projetoTela'))  
        else:
            flash('Erro ao incluir! Revise os campos!')
            return render_template('projeto_cadastro.html', titulo = "Cadastro de Projetos", err = 'erro')    
        
@app.route("/projeto/alterar/<string:id>", methods=['GET','POST'])
def projetoEditar(id):
    if request.method == "GET":
        dsProjeto,nmConexaoA5D,nmA5DHost,nmA5DDatabase,nmA5DTabela,nmA5DPorta,nmA5DLogin,nmA5DSenha,cdA5DCampanha,dsSelect = m.detalheProjeto(id)
        return render_template('projeto_editar.html', titulo = "Alteração de Projetos",id=id, dsProjeto=dsProjeto,
                                                                                        nmConexaoA5D=nmConexaoA5D,
                                                                                        nmA5DHost=nmA5DHost,
                                                                                        nmA5DDatabase=nmA5DDatabase,
                                                                                        nmA5DTabela=nmA5DTabela,
                                                                                        nmA5DPorta=nmA5DPorta,
                                                                                        nmA5DLogin=nmA5DLogin,
                                                                                        nmA5DSenha=nmA5DSenha,
                                                                                        cdA5DCampanha=cdA5DCampanha,
                                                                                        dsSelect=dsSelect)
    else:
        dsProjeto      = request.form.get('dsprojeto')
        nmConexaoA5D   = request.form.get('nmConexaoA5D')
        nmA5DHost      = request.form.get('nmA5DHost')
        nmA5DDatabase  = request.form.get('nmA5DDatabase')
        nmA5DTabela    = request.form.get('nmA5DTabela')
        nmA5DPorta     = request.form.get('nmA5DPorta')
        nmA5DLogin     = request.form.get('nmA5DLogin')
        nmA5DSenha     = request.form.get('nmA5DSenha')
        cdA5DCampanha  = request.form.get('cdA5DCampanha')
        dsSelect       = request.form.get('dsSelect')
        update         = m.updateProjeto(id,dsProjeto,
                                          nmConexaoA5D,
                                          nmA5DHost,
                                          nmA5DDatabase,
                                          nmA5DTabela,
                                          nmA5DPorta,
                                          nmA5DLogin,
                                          nmA5DSenha,
                                          cdA5DCampanha,
                                          dsSelect)
        if update == 'Ok':
            flash('Projeto alterado com sucesso!')
            return redirect(url_for('projetoTela'))  
        else:
            flash('Erro ao atualizar! Revise os campos!')
            return render_template('projeto.html', titulo = "Alteração de Projetos", err = 'erro', id=id)                                   

@app.route("/projeto/deletar/<string:id>")
def projetoDeletar(id):
    result = m.deleteProjeto(id)
    return redirect(url_for('projetoTela'))

##################### CAMPANHA - LISTAR OK, CADASTRAR OK, DELETAR OK E ALTERAR OK #########################################

@app.route("/campanha")
def campanhaTela():
    id, nome, dtinicio, dtfim = m.listaCampanha()
    
    tamanho = len(id)
    return render_template('campanha.html', titulo = 'Tela de Campanha',id = id, nome = nome, dtinicio = dtinicio, dtfim=dtfim, tamanho = tamanho)

@app.route("/campanha/cadastro", methods=['GET','POST'])
def campanhaCadastro():
    if request.method == "GET":
        cdtipoacesso,dstipoacesso = m.listatipoacesso()
        tamTipoAcesso = len(cdtipoacesso)
        cdstatus,dsstatus = m.listaStatus()
        tamStatus = len(cdtipoacesso)
        return render_template('campanha_cadastro.html', titulo = "Cadastro de Campanha", tamStatus=tamStatus, cdstatus=cdstatus, dsstatus=dsstatus, tamTipoAcesso=tamTipoAcesso, cdtipoacesso=cdtipoacesso, dstipoacesso=dstipoacesso)
    else:        
        cdcampanha = request.form.get('cdcampanha')
        dscampanha = request.form.get('dscampanha')
        dtinicio = request.form.get('dtinicio')
        dtfim = request.form.get('dtfim')
        hrinicio = request.form.get('hrinicio')
        hrfim = request.form.get('hrfim')
        cdtipoacesso = request.form.get('cdtipoacesso')
        dsdiscopath = request.form.get('dsdiscopath')
        dsdiscofile = request.form.get('dsdiscofile')
        nmbasehost = request.form.get('nmbasehost')
        nmbasedatabase = request.form.get('nmbasedatabase')
        nmbasedtabela = request.form.get('nmbasedtabela')
        nmbasedporta = request.form.get('nmbasedporta')
        nmbasedlogin = request.form.get('nmbasedlogin')
        nmbasedsenha = request.form.get('nmbasedsenha')
        qtcelulas = request.form.get('qtcelulas')
        qtleads = request.form.get('qtleads')
        qtexecutados = request.form.get('qtexecutados')
        cdtipocampanha = request.form.get('cdtipocampanha')
        cdstatus = request.form.get('cdstatus')
        dsarqseparador = request.form.get('dsarqseparador')
        insert = m.inserirCampanha(cdcampanha, dscampanha, dtinicio, dtfim, hrinicio, hrfim, cdtipoacesso, dsdiscopath, dsdiscofile, nmbasehost, nmbasedatabase, nmbasedtabela, nmbasedporta, nmbasedlogin, nmbasedsenha, qtcelulas, qtleads, qtexecutados, cdtipocampanha, cdstatus, dsarqseparador)       
        if insert == 'Ok':
            flash('Campanha criado com sucesso!')
            return redirect(url_for('campanhaTela'))  
        else:
            flash('Erro ao incluir! Revise os campos!')
            return render_template('campanha_cadastro.html', titulo = "Cadastro de Campanha", err = 'erro')    
        

@app.route("/campanha/alterar/<string:id>", methods=['GET','POST'])
def campanhaEditar(id):
    if request.method == "GET":
        cdcampanha,dscampanha,dtinicio,dtfim,hrinicio,hrfim,cdtipoacesso,dsdiscopath,dsdiscofile,nmbasehost,nmbasedatabase,nmbasedtabela,nmbasedporta,nmbasedlogin,nmbasedsenha,qtcelulas,qtleads,qtexecutados,cdtipocampanha,cdstatus,dsarqseparador = m.detalheCampanha(id)
        cdTA_out, dsTA_out = m.listatipoacesso()
        tamTA = len(cdTA_out)
        cdST_out, dsST_out = m.listaStatus()
        tamST = len(cdST_out)
        return render_template('campanha_editar.html', titulo = "Alteração de Campanha", tamST=tamST, cdST_out=cdST_out,dsST_out=dsST_out,
                                                                                        tamTA=tamTA, cdTA_out=cdTA_out,dsTA_out=dsTA_out,
                                                                                        id=id, 
                                                                                        cdcampanha=cdcampanha,
                                                                                        dscampanha=dscampanha,
                                                                                        dtinicio=dtinicio,
                                                                                        dtfim=dtfim,
                                                                                        hrinicio=hrinicio,
                                                                                        hrfim=hrfim,
                                                                                        cdtipoacesso=cdtipoacesso,
                                                                                        dsdiscopath=dsdiscopath,
                                                                                        dsdiscofile=dsdiscofile,
                                                                                        nmbasehost=nmbasehost,
                                                                                        nmbasedatabase=nmbasedatabase,
                                                                                        nmbasedtabela=nmbasedtabela,
                                                                                        nmbasedporta=nmbasedporta,
                                                                                        nmbasedlogin=nmbasedlogin,
                                                                                        nmbasedsenha=nmbasedsenha,
                                                                                        qtcelulas=qtcelulas,
                                                                                        qtleads=qtleads,
                                                                                        qtexecutados=qtexecutados,
                                                                                        cdtipocampanha=cdtipocampanha,
                                                                                        cdstatus=cdstatus,
                                                                                        dsarqseparador=dsarqseparador)
    else:
        cdcampanha = request.form.get('cdcampanha')
        dscampanha = request.form.get('dscampanha')
        dtinicio = request.form.get('dtinicio')
        dtfim = request.form.get('dtfim')
        hrinicio = request.form.get('hrinicio')
        hrfim = request.form.get('hrfim')
        cdtipoacesso = request.form.get('cdtipoacesso')
        dsdiscopath = request.form.get('dsdiscopath')
        dsdiscofile = request.form.get('dsdiscofile')
        nmbasehost = request.form.get('nmbasehost')
        nmbasedatabase = request.form.get('nmbasedatabase')
        nmbasedtabela = request.form.get('nmbasedtabela')
        nmbasedporta = request.form.get('nmbasedporta')
        nmbasedlogin = request.form.get('nmbasedlogin')
        nmbasedsenha = request.form.get('nmbasedsenha')
        qtcelulas = request.form.get('qtcelulas')
        qtleads = request.form.get('qtleads')
        qtexecutados = request.form.get('qtexecutados')
        cdtipocampanha = request.form.get('cdtipocampanha')
        cdstatus = request.form.get('cdstatus')
        dsarqseparador = request.form.get('dsarqseparador')
        update         = m.updateCampanha(id,cdcampanha,dscampanha,dtinicio,dtfim,hrinicio,hrfim,cdtipoacesso,dsdiscopath,dsdiscofile,nmbasehost,nmbasedatabase,nmbasedtabela,nmbasedporta,nmbasedlogin,nmbasedsenha,qtcelulas,qtleads,qtexecutados,cdtipocampanha,cdstatus,dsarqseparador)
        if update == 'Ok':
            flash('Projeto alterado com sucesso!')
            return redirect(url_for('campanhaTela'))  
        else:
            flash('Erro ao atualizar! Revise os campos!')
            return render_template('campanha.html', titulo = "Tela de Campanha", err = 'erro', id=id)                                   


@app.route("/campanha/deletar/<string:id>")
def campanhaDeletar(id):
    result = m.deleteCampanha(id)
    return redirect(url_for('campanhaTela'))
##################### CELULA - LISTAR OK, CADASTRAR OK, DELETAR E ALTERAR #########################################

@app.route("/celula")
def celulaTela():
    id, nome, cdcampanha, dtinicio, dtfim = m.listaCelula()
    tamanho = len(id)
    return render_template('celula.html', titulo = 'Tela de Células', id = id, nome = nome, cdcampanha=cdcampanha, dtinicio = dtinicio, dtfim=dtfim, tamanho = tamanho)

@app.route("/celula/cadastro", methods=['GET','POST'])
def celulaCadastro():
    if request.method == "GET":
        cdcamp,dscamp,dtini,dtf = m.listaCampanha()
        cdest,dsmov,qtligacoestel01,qtligacoestel02,qtligacoestel03 = m.listaEstrategia()
        cdTC, dsTC = m.listatipocelula()
        cdCN, dsCN = m.listaCanal()
        cdST, dsST = m.listaStatus()
        tamCamp = len(cdcamp)
        tamEst = len(cdest)
        tamTC = len(cdTC)
        tamCN = len(cdCN)
        tamST = len(cdST)
        return render_template('celula_cadastro.html', titulo = "Cadastro de Célula", tamCamp=tamCamp,tamEst=tamEst, tamTC=tamTC, tamST=tamST, tamCN=tamCN, cdST=cdST, dsST=dsST,cdTC=cdTC, dsTC=dsTC, cdCN=cdCN, dsCN=dsCN, cdcamp=cdcamp, dscamp=dscamp,cdest=cdest, dsmov=dsmov )
    else:        
        cdcampanha=request.form.get('cdcampanha')
        dscelula=request.form.get('dscelula')
        dtinicio=request.form.get('dtinicio')
        dtfim=request.form.get('dtfim')
        hrinicio=request.form.get('hrinicio')
        hrfim=request.form.get('hrfim')
        qtexecutados=request.form.get('qtexecutados')
        qtleads=request.form.get('qtleads')
        qtdiscagens=request.form.get('qtdiscagens')
        cdestrategiadiscagem=request.form.get('cdestrategiadiscagem')
        cdtipocelula=request.form.get('cdtipocelula')
        cdcanal=request.form.get('cdcanal')
        cdstatus=request.form.get('cdstatus')
      
        insert = m.inserirCelula(cdcampanha,dscelula,dtinicio,dtfim,hrinicio,hrfim,qtexecutados,qtleads,qtdiscagens,cdestrategiadiscagem,cdtipocelula,cdcanal,cdstatus)
        
        if insert == 'Ok':
            flash('Célula criada com sucesso!')
            return redirect(url_for('celulaTela'))  
        else:
            flash('Erro ao incluir! Revise os campos!')
            return render_template('celula_cadastro.html', titulo = "Cadastro de Célula", err = 'erro')    
        
@app.route("/celula/alterar/<string:id>", methods=['GET','POST'])
def celulaEditar(id):
    if request.method == "GET":
        cdcampanha, cdcelula, dscelula, dtinicio, dtfim, hrinicio, hrfim, qtexecutados, qtleads, qtdiscagens, cdestrategiadiscagem, cdtipocelula, cdcanal, cdstatus = m.detalheCelula(id)
        cdcamp,dscamp,dtini,dtf = m.listaCampanha()
        cdest,dsmov,qtligacoestel01,qtligacoestel02,qtligacoestel03 = m.listaEstrategia()
        cdTC, dsTC = m.listatipocelula()
        cdCN, dsCN = m.listaCanal()
        cdST, dsST = m.listaStatus()
        tamCamp = len(cdcamp)
        tamEst = len(cdest)
        tamTC = len(cdTC)
        tamCN = len(cdCN)
        tamST = len(cdST)
        
        
        return render_template('celula_editar.html', titulo = "Alteração de Celula",id=id, cdST=cdST, dsST=dsST,cdTC=cdTC, dsTC=dsTC, cdCN=cdCN, dsCN=dsCN, cdcamp=cdcamp, dscamp=dscamp,cdest=cdest, dsmov=dsmov,
                                                                                        tamCamp=tamCamp,
                                                                                        tamEst = tamEst,tamTC = tamTC,
                                                                                        tamCN = tamCN,
                                                                                        tamST = tamST, 
                                                                                        cdcampanha=cdcampanha,
                                                                                        cdcelula=cdcelula,
                                                                                        dscelula=dscelula,
                                                                                        dtinicio=dtinicio,
                                                                                        dtfim=dtfim,
                                                                                        hrinicio=hrinicio,
                                                                                        hrfim=hrfim,
                                                                                        qtexecutados=qtexecutados,
                                                                                        qtleads=qtleads,
                                                                                        qtdiscagens=qtdiscagens,
                                                                                        cdestrategiadiscagem=cdestrategiadiscagem,
                                                                                        cdtipocelula=cdtipocelula,
                                                                                        cdcanal=cdcanal,
                                                                                        cdstatus=cdstatus)
    else:
        cdcampanha=request.form.get('cdcampanha')
        cdcelula=request.form.get('cdcelula')
        dscelula=request.form.get('dscelula')
        dtinicio=request.form.get('dtinicio')
        dtfim=request.form.get('dtfim')
        hrinicio=request.form.get('hrinicio')
        hrfim=request.form.get('hrfim')
        qtexecutados=request.form.get('qtexecutados')
        qtleads=request.form.get('qtleads')
        qtdiscagens=request.form.get('qtdiscagens')
        cdestrategiadiscagem=request.form.get('cdestrategiadiscagem')
        cdtipocelula=request.form.get('cdtipocelula')
        cdcanal=request.form.get('cdcanal')
        cdstatus=request.form.get('cdstatus')
        print(cdcelula)
        update         = m.updateCelula(id, cdcampanha, cdcelula, dscelula, dtinicio, dtfim, hrinicio, hrfim, qtexecutados, qtleads, qtdiscagens, cdestrategiadiscagem, cdtipocelula, cdcanal, cdstatus)
        if update == 'Ok':
            flash('Projeto alterado com sucesso!')
            return redirect(url_for('celulaTela'))  
        else:
            flash('Erro ao atualizar! Revise os campos!')
            return render_template('celula.html', titulo = "Tela de Celula", err = 'erro', id=id)                                   


@app.route("/celula/deletar/<string:id>")
def celulaDeletar(id):
    result = m.deleteCelula(id)
    return redirect(url_for('celulaTela'))



  

##################### RELACIONAMENTOCELULAS - LISTAR OK, CADASTRAR OK, DELETAR E ALTERAR #########################################

@app.route("/relacionamento")
def relacionamentoTela():
    cdrelacionamento,cdcampanha,cdcelula,cdcelulapredecessora,dsquery,cdfiltro,cdprioridade = m.listaRelacionamento()
    tamanho = len(cdcampanha)
    return render_template('relacionamento.html', titulo = 'Tela de Relacionamento', cdrelacionamento=cdrelacionamento, cdcelula = cdcelula, cdcelulapredecessora = cdcelulapredecessora, cdcampanha=cdcampanha, dsquery = dsquery, cdfiltro=cdfiltro, cdprioridade=cdprioridade, tamanho = tamanho)

@app.route("/relacionamento/cadastro", methods=['GET','POST'])
def relacionamentoCadastro():
    if request.method == "GET":
        cdcamp,dscamp,dtini,dtf = m.listaCampanha()
        tamCamp = len(cdcamp)
        cdcel,dscel, cdc,dti,dtfi = m.listaCelula()
        tamCel = len(cdcel)
        return render_template('relacionamento_cadastro.html', titulo = "Cadastro de Relacionamento", cdcamp=cdcamp,dscamp=dscamp,tamCamp=tamCamp,cdcel=cdcel,dscel=dscel,tamCel=tamCel)
    else:        
        cdcampanha=request.form.get('cdcampanha')
        cdcelula=request.form.get('cdcelula')
        cdcelulapredecessora=request.form.get('cdcelulapredecessora')
        dsquery=request.form.get('dsquery')
        cdfiltro=request.form.get('cdfiltro')
        cdprioridade=request.form.get('cdprioridade')

        insert = m.inserirRelacionamento(cdcampanha,cdcelula,cdcelulapredecessora,dsquery,cdfiltro,cdprioridade)
        
        if insert == 'Ok':
            flash('Relacionamento criada com sucesso!')
            return redirect(url_for('relacionamentoTela'))  
        else:
            flash('Erro ao incluir! Revise os campos!')
            return render_template('relacionamento_cadastro.html', titulo = "Cadastro de Relacionamento", err = 'erro')    
        

@app.route("/relacionamento/alterar/<string:id>", methods=['GET','POST'])
def relacionamentoEditar(id):
    if request.method == "GET":
        cdcampanha,cdcelula,cdcelulapredecessora,dsquery,cdfiltro,cdprioridade = m.detalheRelacionamento(id)
        cdcamp,dscamp,dtini,dtf = m.listaCampanha()
        tamCamp = len(cdcamp)
        cdcel,dscel, cdc,dti,dtfi = m.listaCelula()
        tamCel = len(cdcel)
        return render_template('relacionamento_editar.html', titulo="Alteração de Relacionamento", id=id,
        cdcamp=cdcamp,dscamp=dscamp,tamCamp=tamCamp,cdcel=cdcel,dscel=dscel,tamCel=tamCel,cdcampanha=cdcampanha,cdcelula=cdcelula,cdcelulapredecessora=cdcelulapredecessora,dsquery=dsquery,cdfiltro=cdfiltro,cdprioridade=cdprioridade)
    else: 
        cdcampanha=request.form.get('cdcampanha')
        cdcelula=request.form.get('cdcelula')
        cdcelulapredecessora=request.form.get('cdcelulapredecessora')
        dsquery=request.form.get('dsquery')
        cdfiltro=request.form.get('cdfiltro')
        cdprioridade=request.form.get('cdprioridade')

        insert = m.updateRelacionamento(id,cdcampanha,cdcelula,cdcelulapredecessora,dsquery,cdfiltro,cdprioridade)
        
        if insert == 'Ok':
            flash('Relacionamento alterada com sucesso!')
            return redirect(url_for('relacionamentoTela'))  
        else:
            flash('Erro ao incluir! Revise os campos!')
            return render_template('relacionamento.html', titulo = "Cadastro de Relacionamento", err = 'erro')    
        

@app.route("/relacionamento/deletar/<string:id>")
def relacionamentoDeletar(id):
    result = m.deleteRelacionamento(id)
    return redirect(url_for('relacionamentoTela'))

##################### ESTRATEGIADISCAGEM - LISTAR OK, CADASTRAR  OK, DELETAR E ALTERAR #########################################

@app.route("/estrategiadiscagem")
def estrategiadiscagemTela():
    cdestrategiadiscagem,dsmovimento,qtligacoestel01,qtligacoestel02,qtligacoestel03 = m.listaEstrategia()
    tamanho = len(cdestrategiadiscagem)
    return render_template('estrategiadiscagem.html', titulo = 'Tela de Estratégia', 
    cdestrategiadiscagem = cdestrategiadiscagem, dsmovimento = dsmovimento, qtligacoestel01=qtligacoestel01,
    qtligacoestel02 = qtligacoestel02, qtligacoestel03=qtligacoestel03,tamanho = tamanho)

@app.route("/estrategiadiscagem/cadastro", methods=['GET','POST'])
def estrategiadiscagemCadastro():
    if request.method == "GET":
        return render_template('estrategiadiscagem_cadastro.html', titulo = "Cadastro de Estratégia")
    else:        
        dsmovimento=request.form.get('dsmovimento')
        qtligacoestel01=request.form.get('qtligacoestel01')
        qtligacoestel02=request.form.get('qtligacoestel02')
        qtligacoestel03=request.form.get('qtligacoestel03')
        qtligacoestel04=request.form.get('qtligacoestel04')
        qtligacoestel05=request.form.get('qtligacoestel05')
        qtligacoestel06=request.form.get('qtligacoestel06')

        insert = m.inserirEstrategia(dsmovimento,qtligacoestel01,qtligacoestel02,qtligacoestel03,qtligacoestel04,qtligacoestel05,qtligacoestel06)

        if insert == 'Ok':
            flash('Estratégia de Discagem criada com sucesso!')
            return redirect(url_for('estrategiadiscagemTela'))  
        else:
            flash('Erro ao incluir! Revise os campos!')
            return render_template('estrategiadiscagem_cadastro.html', titulo = "Cadastro de Estratégia de Discagem", err = 'erro')    
        

@app.route("/estrategiadiscagem/alterar/<string:id>", methods=['GET','POST'])
def estrategiadiscagemEditar(id):
    if request.method == "GET":
        cdestrategiadiscagem,dsmovimento,qtligacoestel01,qtligacoestel02,qtligacoestel03,qtligacoestel04,qtligacoestel05,qtligacoestel06 = m.detalheEstrategia(id)
        
        return render_template('estrategiadiscagem_editar.html', titulo="Alteração de Estratégia", id=id,
        cdestrategiadiscagem=cdestrategiadiscagem,dsmovimento=dsmovimento,qtligacoestel01=qtligacoestel01,qtligacoestel02=qtligacoestel02,qtligacoestel03=qtligacoestel03,qtligacoestel04=qtligacoestel04,qtligacoestel05=qtligacoestel05,qtligacoestel06=qtligacoestel06)
    else: 
        cdestrategiadiscagem=request.form.get('cdestrategiadiscagem')
        dsmovimento=request.form.get('dsmovimento')
        qtligacoestel01=request.form.get('qtligacoestel01')
        qtligacoestel02=request.form.get('qtligacoestel02')
        qtligacoestel03=request.form.get('qtligacoestel03')
        qtligacoestel04=request.form.get('qtligacoestel04')
        qtligacoestel05=request.form.get('qtligacoestel05')
        qtligacoestel06=request.form.get('qtligacoestel06')

        insert = m.updateEstrategia(id,dsmovimento, qtligacoestel01, qtligacoestel02, qtligacoestel03, qtligacoestel04, qtligacoestel05, qtligacoestel06)
        
        if insert == 'Ok':
            flash('Relacionamento alterada com sucesso!')
            return redirect(url_for('estrategiaTela'))  
        else:
            flash('Erro ao incluir! Revise os campos!')
            return render_template('estrategiadiscagem.html', titulo = "Cadastro de Estratégia", err = 'erro')    
        


@app.route("/estrategiadiscagem/deletar/<string:id>")
def estrategiadiscagemDeletar(id):
    result = m.deleteEstrategia(id)
    return redirect(url_for('estrategiadiscagemTela'))



##################### CANAL - LISTAR OK, CADASTRAR  OK, DELETAR E ALTERAR #########################################

@app.route("/canal")
def canalTela():
    cdcanal, dscanal = m.listaCanal()
    tamanho = len(cdcanal)
    return render_template('canal.html', titulo = 'Tela de Canal', 
    cdcanal = cdcanal, dscanal = dscanal,tamanho = tamanho)

@app.route("/canal/cadastro", methods=['GET','POST'])
def canalCadastro():
    if request.method == "GET":
        return render_template('canal_cadastro.html', titulo = "Cadastro de Canal")
    else:        
        dscanal=request.form.get('dscanal')
        insert = m.inserirCanal(dscanal)

        if insert == 'Ok':
            flash('Canal criada com sucesso!')
            return redirect(url_for('canalTela'))  
        else:
            flash('Erro ao incluir! Revise os campos!')
            return render_template('canal_cadastro.html', titulo = "Cadastro de Canal", err = 'erro')    
        


@app.route("/canal/alterar/<string:id>", methods=['GET','POST'])
def canalEditar(id):
    if request.method == "GET":
        cdcanal,dscanal = m.detalheCanal(id)
        
        return render_template('canal_editar.html', titulo="Alteração de Canal", id=id,
        cdcanal=cdcanal,dscanal=dscanal)
    else: 
        cdcanal=request.form.get('cdcanal')
        dscanal=request.form.get('dscanal')
        

        insert = m.updateCanal(id, dscanal)
        
        if insert == 'Ok':
            flash('Canal alterado com sucesso!')
            return redirect(url_for('canalTela'))  
        else:
            flash('Erro ao incluir! Revise os campos!')
            return render_template('canal.html', titulo = "Cadastro de Canal", err = 'erro')    
        



@app.route("/canal/deletar/<string:id>")
def canalDeletar(id):
    result = m.deleteCanal(id)
    return redirect(url_for('canalTela'))



##################### Status - LISTAR OK, CADASTRAR  OK, DELETAR E ALTERAR #########################################

@app.route("/status")
def statusTela():
    cdstatus, dsstatus = m.listaStatus()
    tamanho = len(cdstatus)
    return render_template('status.html', titulo = 'Tela de status', 
    cdstatus = cdstatus, dsstatus = dsstatus,tamanho = tamanho)

@app.route("/status/cadastro", methods=['GET','POST'])
def statusCadastro():
    if request.method == "GET":
        return render_template('status_cadastro.html', titulo = "Cadastro de status")
    else:        
        dsstatus=request.form.get('dsstatus')
        insert = m.inserirStatus(dsstatus)

        if insert == 'Ok':
            flash('status criada com sucesso!')
            return redirect(url_for('statusTela'))  
        else:
            flash('Erro ao incluir! Revise os campos!')
            return render_template('status_cadastro.html', titulo = "Cadastro de status", err = 'erro')    
        

@app.route("/status/alterar/<string:id>", methods=['GET','POST'])
def statusEditar(id):
    if request.method == "GET":
        cdstatus,dsstatus = m.detalheStatus(id)
        
        return render_template('status_editar.html', titulo="Alteração de status", id=id,
        cdstatus=cdstatus,dsstatus=dsstatus)
    else: 
        cdstatus=request.form.get('cdstatus')
        dsstatus=request.form.get('dsstatus')
        

        insert = m.updateStatus(id, dsstatus)
        
        if insert == 'Ok':
            flash('status alterado com sucesso!')
            return redirect(url_for('statusTela'))  
        else:
            flash('Erro ao incluir! Revise os campos!')
            return render_template('status.html', titulo = "Cadastro de status", err = 'erro') 

@app.route("/status/deletar/<string:id>")
def statusDeletar(id):
    result = m.deleteStatus(id)
    return redirect(url_for('statusTela'))    



##################### tipoacesso - LISTAR OK, CADASTRAR  OK, DELETAR E ALTERAR #########################################

@app.route("/tipoacesso")
def tipoacessoTela():
    cdtipoacesso, dstipoacesso = m.listatipoacesso()
    tamanho = len(cdtipoacesso)
    return render_template('tipoacesso.html', titulo = 'Tela de Tipo de acesso', 
    cdtipoacesso = cdtipoacesso, dstipoacesso = dstipoacesso,tamanho = tamanho)

@app.route("/tipoacesso/cadastro", methods=['GET','POST'])
def tipoacessoCadastro():
    if request.method == "GET":
        return render_template('tipoacesso_cadastro.html', titulo = "Cadastro de Tipo de Acesso")
    else:        
        dstipoacesso=request.form.get('dstipoacesso')
        insert = m.inserirtipoacesso(dstipoacesso)

        if insert == 'Ok':
            flash('tipoacesso criada com sucesso!')
            return redirect(url_for('tipoacessoTela'))  
        else:
            flash('Erro ao incluir! Revise os campos!')
            return render_template('tipoacesso_cadastro.html', titulo = "Cadastro de tipoacesso", err = 'erro')    
        

@app.route("/tipoacesso/alterar/<string:id>", methods=['GET','POST'])
def tipoacessoEditar(id):
    if request.method == "GET":
        cdtipoacesso,dstipoacesso = m.detalhetipoacesso(id)
        
        return render_template('tipoacesso_editar.html', titulo="Alteração de tipoacesso", id=id,
        cdtipoacesso=cdtipoacesso,dstipoacesso=dstipoacesso)
    else: 
        cdtipoacesso=request.form.get('cdtipoacesso')
        dstipoacesso=request.form.get('dstipoacesso')
        

        insert = m.updatetipoacesso(id, dstipoacesso)
        
        if insert == 'Ok':
            flash('tipoacesso alterado com sucesso!')
            return redirect(url_for('tipoacessoTela'))  
        else:
            flash('Erro ao incluir! Revise os campos!')
            return render_template('tipoacesso.html', titulo = "Cadastro de tipoacesso", err = 'erro') 

@app.route("/tipoacesso/deletar/<string:id>")
def tipoacessoDeletar(id):
    result = m.deletetipoacesso(id)
    return redirect(url_for('tipoacessoTela'))


##################### tipocelula - LISTAR OK, CADASTRAR  OK, DELETAR E ALTERAR #########################################

@app.route("/tipocelula")
def tipocelulaTela():
    cdtipocelula, dstipocelula = m.listatipocelula()
    tamanho = len(cdtipocelula)
    return render_template('tipocelula.html', titulo = 'Tela de Tipo de Celula', 
    cdtipocelula = cdtipocelula, dstipocelula = dstipocelula,tamanho = tamanho)

@app.route("/tipocelula/cadastro", methods=['GET','POST'])
def tipocelulaCadastro():
    if request.method == "GET":
        return render_template('tipocelula_cadastro.html', titulo = "Cadastro de Tipo de Celula")
    else:        
        dstipocelula=request.form.get('dstipocelula')
        insert = m.inserirtipocelula(dstipocelula)

        if insert == 'Ok':
            flash('tipocelula criada com sucesso!')
            return redirect(url_for('tipocelulaTela'))  
        else:
            flash('Erro ao incluir! Revise os campos!')
            return render_template('tipocelula_cadastro.html', titulo = "Cadastro de Tipo de Celula", err = 'erro')    
        

@app.route("/tipocelula/alterar/<string:id>", methods=['GET','POST'])
def tipocelulaEditar(id):
    if request.method == "GET":
        cdtipocelula,dstipocelula = m.detalhetipocelula(id)
        
        return render_template('tipocelula_editar.html', titulo="Alteração de tipocelula", id=id,
        cdtipocelula=cdtipocelula,dstipocelula=dstipocelula)
    else: 
        cdtipocelula=request.form.get('cdtipocelula')
        dstipocelula=request.form.get('dstipocelula')
        

        insert = m.updatetipocelula(id, dstipocelula)
        
        if insert == 'Ok':
            flash('tipocelula alterado com sucesso!')
            return redirect(url_for('tipocelulaTela'))  
        else:
            flash('Erro ao incluir! Revise os campos!')
            return render_template('tipocelula.html', titulo = "Cadastro de tipocelula", err = 'erro') 

@app.route("/tipocelula/deletar/<string:id>")
def tipocelulaDeletar(id):
    result = m.deletetipocelula(id)
    return redirect(url_for('tipocelulaTela'))


app.run(debug=True, port=5010)            