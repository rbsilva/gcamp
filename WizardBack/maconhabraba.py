#===================================================================================================== 
# Desenvolvido por Romulo Brandão Silva
#===================================================================================================== 

import pandas as pd
from pandas import json_normalize
import os
import sys
sys.path.insert(1, './98Funcao')
import sFuncoes as f
import wFuncoes as w
import json, requests
import psycopg2
import psycopg2.extras

try:
    #===================================================================================================== 
    # Incializações
    #===================================================================================================== 

    dirjson  = '\..\\00Entrada'               
    dirlog   = '\..\\80Logs'
    diratual   = os.path.dirname(__file__)    
    direntrada = w.MontaDirAbaixo(diratual,'02Entrada')
    dirlog     = w.MontaDirAbaixo(diratual,'99Log')    
    arqjson    = os.path.join(direntrada,'conexaoWizard.json')      

    #===================================================================================================== 
    # Dados (Entrada)
    #=====================================================================================================         
    parametros  = f.leituraJson(arqjson,False)     
    #===================================================================================================== 
    # Banco de dados
    #===================================================================================================== 
    host_bd      = parametros['host']
    database_bd  = parametros['database']
    user_bd      = parametros['user']
    password_bd  = parametros['password']
    port_bd      = parametros['porta']
    enderco_log  = parametros['enderecoLog']
    #=================================================
    # Abertura do Log
    #=================================================
    locallog     = os.path.join(dirlog,enderco_log)
    plog         = f.Log(locallog)
    if (parametros['zeralog']):
        plog.log_arq_novo()
    plog.log_escreve('Processo do Wizard')
    plog.log_escreve('')
    #=================================================
    # Teste de Conectividade
    #=================================================
    
    con,res=w.conecta_bd(host_bd,database_bd,user_bd,password_bd)
    if res=="Erro" :
        plog.log_escreve('Erro na Conexão')
        plog.log_escreve(con)
        exit()
    else:
        plog.log_escreve('Conexão Funcionando!')
        
    #=================================================
    # Gravando fluxo de campanha no banco
    #=================================================
  
    df = w.FluxoDF()
    dfRel = w.RelacionamentoDF()
    # y=0
    # for i in range(len(df)):
    #     #Verifica a chave de Entrada - Se for receptivo, criará um serviço
    #     if df['Chave'][i] == '1' and df['Tipo_bot'][i] == 'Receptivo':
    #         print("Criação do Serviço inicial na tabela Service In")
    #     #Verifica a chave de Entrada - Se for ativo, criará uma campanha    
    #     elif df['Chave'][i] == '1' and df['Tipo_bot'][i] == 'Ativo':
    #         print('Criar campanha na tabela Campaign')
    #     #Verifica se o tipo de elemento é um menu - Se for um menu, criará na tabela menu    			
    #     if 	df['Tipo'][i] == 'Menu':
    #         print("Criação de Menu na tabela menu")
    #     #Verifica se o tipo de elemento é um condicional e se está ligado a um menu    
    #     if df['Tipo'][i] == 'Condicional' and df['Tipo'][i-1]=='Menu':		
    #         qtd = int(df['Qtd_Respostas'][i])
    #     #Verifica a quantidade de respostas da condicional para montar o menu_options da ingenium 
    #         print(f'Criação de {qtd} Menus Options')
    #     #Montagem da Query do Menu_Options
    #         for x in range(i+1, (i+1)+qtd):
    #             y = y + 1
    #             for z in range(len(dfRel)):
    #                 if dfRel['chave'][z] == df['Chave'][x]:
    #                     print(f"{y}- Chave {df['Chave'][x]} / Sucessor {dfRel['sucessor'][z]}")
    #                     for w in range(i+x, (x+i)+1):
    #                         print(f"Mensagem que será tocada/enviada {df['caminhoArq'][w]}")
    # dfEntrada = pd.DataFrame([],columns=['Cliente', 'Fluxo', 'Tipo_bot', 'Canal', 'Classe', 'Tipo', 'Chave',
    #    'Menu', 'Nome', 'Tempoespera', 'NumRepet', 'Interacao', 'Qtd_Respostas',
    #    'caminhoArq', 'Mensagem', 'Msg_erro', 'Msg_maxRepet', 'FimdaChamada'])
    # dfMenu = pd.DataFrame([],columns=['Cliente', 'Fluxo', 'Tipo_bot', 'Canal', 'Classe', 'Tipo', 'Chave',
    #    'Menu', 'Nome', 'Tempoespera', 'NumRepet', 'Interacao', 'Qtd_Respostas',
    #    'caminhoArq', 'Mensagem', 'Msg_erro', 'Msg_maxRepet', 'FimdaChamada'])
    # dfCondicional = pd.DataFrame([],columns=['Cliente', 'Fluxo', 'Tipo_bot', 'Canal', 'Classe', 'Tipo', 'Chave',
    #    'Menu', 'Nome', 'Tempoespera', 'NumRepet', 'Interacao', 'Qtd_Respostas',
    #    'caminhoArq', 'Mensagem', 'Msg_erro', 'Msg_maxRepet', 'FimdaChamada'])   
    # dfResposta = pd.DataFrame([],columns=['Cliente', 'Fluxo', 'Tipo_bot', 'Canal', 'Classe', 'Tipo', 'Chave',
    #    'Menu', 'Nome', 'Tempoespera', 'NumRepet', 'Interacao', 'Qtd_Respostas',
    #    'caminhoArq', 'Mensagem', 'Msg_erro', 'Msg_maxRepet', 'FimdaChamada'])
    # dfMensagem = pd.DataFrame([],columns=['Cliente', 'Fluxo', 'Tipo_bot', 'Canal', 'Classe', 'Tipo', 'Chave',
    #    'Menu', 'Nome', 'Tempoespera', 'NumRepet', 'Interacao', 'Qtd_Respostas',
    #    'caminhoArq', 'Mensagem', 'Msg_erro', 'Msg_maxRepet', 'FimdaChamada'])
    # dfHumano =  pd.DataFrame([],columns=['Cliente', 'Fluxo', 'Tipo_bot', 'Canal', 'Classe', 'Tipo', 'Chave',
    #    'Menu', 'Nome', 'Tempoespera', 'NumRepet', 'Interacao', 'Qtd_Respostas',
    #    'caminhoArq', 'Mensagem', 'Msg_erro', 'Msg_maxRepet', 'FimdaChamada'])             
    
    #Verifica integridade de chave 
    linhas = 0              
    for index, row in dfRel.iterrows():
        for ix, rw in df.iterrows():
            if row['chave'] == rw['Chave']:
                linhas = linhas + 1
    if len(dfRel) > linhas:
        plog.log_escreve('Erro! Registro não encotrado na tabela de Elementos')
        exit()            
    else:
        plog.log_escreve('Verificação de Integridade concluída')          

    dfAcoes = pd.DataFrame([], columns=['chave', 'tipo', 'acao', 'tabela'])
    #Manda dados para DataFrame de Ações
    # def sucessor():

    for ix, rw in df.iterrows():
        #DataFrame com as ações a se fazer
        if rw['Classe']=="Inicio" and rw['Tipo_bot']=="Receptivo":
            dfAcoes.at[ix, 'chave'] = rw['Chave']
            dfAcoes.at[ix, 'tipo'] = rw['Tipo']
            dfAcoes.at[ix, 'acao'] = 'Inserir'
            dfAcoes.at[ix, 'tabela'] = 'servicein'

        elif rw['Classe']=="Inicio" and rw['Tipo_bot']=="Ativo":
            dfAcoes.at[ix, 'chave'] = rw['Chave']
            dfAcoes.at[ix, 'tipo'] = rw['Tipo']
            dfAcoes.at[ix, 'acao'] = 'Inserir'
            dfAcoes.at[ix, 'tabela'] = 'campanha'
        if rw['Tipo']=="Menu":
            dfAcoes.at[ix, 'chave'] = rw['Chave']
            dfAcoes.at[ix, 'tipo'] = rw['Tipo']
            dfAcoes.at[ix, 'acao'] = 'Inserir'
            dfAcoes.at[ix, 'tabela'] = 'menu'
        if rw['Tipo']=="Condicional":
            dfAcoes.at[ix, 'chave'] = rw['Chave']
            dfAcoes.at[ix, 'tipo'] = rw['Tipo']
            dfAcoes.at[ix, 'acao'] = 'Inserir'
            dfAcoes.at[ix, 'tabela'] = 'menu_options'    
        if rw['Tipo']=="Humano":
            dfAcoes.at[ix, 'chave'] = rw['Chave']
            dfAcoes.at[ix, 'tipo'] = rw['Tipo']
            dfAcoes.at[ix, 'acao'] = 'Inserir'
            dfAcoes.at[ix, 'tabela'] = 'servicein'
    
    qtd_sucessor = 0
    opcao = ''
    opcao_mensagem = ''
    menu = ''
    prox = ''
    fila = False
    for i, r in dfAcoes.iterrows():
        #for index, row in df.iterrows():
            #Executando as ações do DF de ações
            if r['acao'] == "Inserir" and r['tipo']=='Entrada':
                #A coluna 'tabela' informará se é ativo ou receptivo
                #Se a tabela for servicein = receptivo
                #Se a tabela for campaign = ativo
                plog.log_escreve(f'Inserindo na tabela {r["tabela"]}')
                for index, row in df.iterrows():
                    if r['chave'] == row['Chave']:
                        plog.log_escreve(f'Query insert na tabela {r["tabela"]}') 
                        break    
                next

            elif r['tipo'] ==  "Menu": 

                for x, linha in dfRel.iterrows(): 
                        if r['chave'] == linha['chave']: #Verifica quem é o sucessor do menu
                            suc = linha['sucessor'] #Armazena o sucessor em uma variavel para comparar
                            break   

                for index, row in df.iterrows(): #Verifica qual o tipo do sucessor! Precisa ser condicional
                    if row['Chave'] == suc:
                        if row['Tipo'] == 'Condicional':
                            cond = True #Se o sucessor for condicional, essas variavel será True
                            plog.log_escreve(f'O sucessor do menu é uma condicional')
                            break
                #Verifica os tipos de Menu
                if cond == True:   
                    plog.log_escreve(f'Inserindo na tabela {r["tabela"]}')
                    for index, row in df.iterrows():
                        
                        if r['chave'] == row['Chave']: #Consulta no DF principal os dados da chave para fazer a inclusão
                            if row["Canal"] == "Voz": #Condicional para incluir na tabela de voz
                                #Montando a query para inclusão local
                                sql = f'''INSERT INTO public.menuvoz( 
                                cdmenu, dsmenu, dstempoespera, dsnomearq, dsrepeticao, dstipo_resposta)
                                VALUES ({row["Chave"]}, '{row["Nome"]}', '{row["Tempoespera"]}', '{row["caminhoArq"]}', '{row["NumRepet"]}', '{row["Interacao"]}');'''
                                #res = w.inserir_bd(sql,host_bd,database_bd,user_bd,password_bd)
                                plog.log_escreve(f'Registro inserido com sucesso!')
                            else: #Condicional para incluir na tabela de rede social
                                plog.log_escreve(f'Inserindo na tabela menuredesocial - {row["Canal"]}')
                                sql = f'''INSERT INTO public.menuredesocial(
                                        cdmenu, dsmenu, dstempoespera, dsrepeticao, dsmsg, dstipo_resposta, dsmsgerro, dsmsgmaxrepeticao)
                                        VALUES ({row["Chave"]}, '{row["Nome"]}', '{row["Tempoespera"]}',  '{row["NumRepet"]}', '{row["Mensagem"]}', '{row["Interacao"]}', '{row["Msg_erro"]}', '{row["Msg_maxRepet"]}');'''
                                #res = w.inserir_bd(sql,host_bd,database_bd,user_bd,password_bd)
                                plog.log_escreve(f'Registro inserido com sucesso!')
                            break     
                    next
                else:
                    plog.log_escreve(f'Erro! O sucessor do menu não é uma condicional')
                    exit()

            elif r['tipo'] == "Condicional": #Verifica se o tipo de elemento é condicional
                for x, linha in dfRel.iterrows():
                    if r['chave'] == linha['chave']:
                        menu = linha['predecessor']
                        opcao = linha['sucessor'] #Verifica quantos sucessores ela tem
                        plog.log_escreve(f'Resposta da condicional - {r["chave"]}')
                        next
                    if  linha['chave'] == opcao:
                        opcao_mensagem = linha['sucessor']     
                        plog.log_escreve(f'Conteudo da resposta - {r["chave"]}') 
                        next
                
                    

                if opcao_mensagem != '':
                    for x, linha in dfRel.iterrows():
                        if opcao_mensagem == linha['chave']:
                            prox = linha['sucessor']

                    for index, row in df.iterrows():
                        if row['Chave'] == prox:
                            if row['Tipo'] == 'Humano':
                                fila = True

                    for index, row in df.iterrows():
                        
                        if row['Chave'] == opcao_mensagem:
                            
                            sql = f''' INSERT INTO public.resposta(
	                                  cdmenu, cdmenusuc, dsresposta, dsname, dsdesliga, dsfila, dsmsgresposta, dscuradoria)
	                                  VALUES ({menu}, {prox}, '{row["Menu"]}', '{row["Nome"]}', {row["FimdaChamada"]}, {fila}, '{row["FimdaChamada"]}', '{row["Curadoria"]}');'''
                            print(sql)          

                
                # for index, row in df.iterrows():
                #     if r['chave'] == row['Chave']:
                        #plog.log_escreve(f'Chave da Condicional Encontrada!')

                next



            elif r['tipo'] == "Humano":
                plog.log_escreve(f'Inserindo na tabela {r["tabela"]}')
                next

except (Exception, psycopg2.DatabaseError) as error:
    plog.log_escreve(error) 