#===================================================================================================== 
# Processo de Criação Básica do Ambiente de Dados do Gerenciador de Campanha
# Romulo    - 01/06/2022 - Construção do Módulo Básico
# Fábio     - 02/06/2022 - Estruturação do Processo
#===================================================================================================== 
import pandas as pd
import sys
sys.path.insert(1, './98Funcao')
import sFuncoes as f
import pFuncoes as p
import json, requests
import psycopg2
import psycopg2.extras
import os

try:
    #===================================================================================================== 
    # Incializações
    #===================================================================================================== 
    diratual   = os.path.dirname(__file__)    
    direntrada = p.MontaDirAbaixo(diratual,'02Entrada')
    dirlog     = p.MontaDirAbaixo(diratual,'99Log')    
    arqjson    = os.path.join(direntrada,'conexaoWizard.json')      

    #===================================================================================================== 
    # Dados (Entrada)
    #=====================================================================================================         
    parametros  = f.leituraJson(arqjson,False)     
    #===================================================================================================== 
    # Banco de dados
    #===================================================================================================== 
    host_bd      = parametros['host']
    database_bd  = parametros['database']
    user_bd      = parametros['user']
    password_bd  = parametros['password']
    port_bd      = parametros['porta']
    enderco_log  = parametros['enderecoLog']

    #=================================================
    # Abertura do Log
    #=================================================
    locallog     = os.path.join(dirlog,enderco_log)
    plog         = f.Log(locallog)
    if (parametros['zeralog']):
        plog.log_arq_novo()
    plog.log_escreve('Iniciando estrutura do banco de dados do Wizard')
    plog.log_escreve('')
    #=================================================
    # Teste de Conectividade
    #=================================================
    con,res=p.conecta_bd(host_bd,database_bd,user_bd,password_bd)
    if res=="Erro" :
        plog.log_escreve('Erro na Conexão')
        plog.log_escreve(con)
        exit()
    else:
        plog.log_escreve('Conexão Funcionando!')


##################################    Fluxo     ##################################################
    tabela = "Fluxo"
    sql_db = '''
        DROP TABLE IF EXISTS public.fluxo;


CREATE SEQUENCE fluxo_cdfluxo_seq;



CREATE TABLE IF NOT EXISTS public.fluxo
(
    cdfluxo integer NOT NULL DEFAULT nextval('fluxo_cdfluxo_seq'),
    dsfluxo character varying(30) COLLATE pg_catalog."default",
    dscliente character varying(30) COLLATE pg_catalog."default",
    dstipocampanha character varying(30) COLLATE pg_catalog."default",
    dscanal character varying(30) COLLATE pg_catalog."default",
    dscaminhojson character varying(100) COLLATE pg_catalog."default",
    dsjson character varying(100) COLLATE pg_catalog."default",
    nmbasehost character varying(100) COLLATE pg_catalog."default",
    nmbasedatabase character varying(100) COLLATE pg_catalog."default",
    nmbasetabela character varying(100) COLLATE pg_catalog."default",
    nmbaseporta character varying(100) COLLATE pg_catalog."default",
    nmbaselogin character varying(100) COLLATE pg_catalog."default",
    nmbasesenha character varying(100) COLLATE pg_catalog."default"
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.fluxo
    OWNER to postgres;

ALTER SEQUENCE fluxo_cdfluxo_seq
OWNED BY fluxo.cdfluxo;
        
        
        '''
    res=p.inserir_bd(sql_db,host_bd,database_bd,user_bd,password_bd)
    
    if res=="Erro":
        plog.log_escreve(f'Não foi possivel criar a tabela {tabela}')
    else:
        plog.log_escreve(f'Tabela {tabela} criada com sucesso!')



##################################    Elementos     ##################################################
    tabela = "Elementos"
    sql_db = '''
        DROP TABLE IF EXISTS public.elementos;


CREATE SEQUENCE elementos_cdelementos_seq;



CREATE TABLE IF NOT EXISTS public.elementos
(
    cdelementos integer NOT NULL DEFAULT nextval('elementos_cdelementos_seq'),
    cliente character varying(30) COLLATE pg_catalog."default",
    fluxo character varying(30) COLLATE pg_catalog."default",
    tipo_bot character varying(30) COLLATE pg_catalog."default",
    canal character varying(100) COLLATE pg_catalog."default",
    classe character varying(100) COLLATE pg_catalog."default",
    tipo character varying(100) COLLATE pg_catalog."default",
    chave character varying(100) COLLATE pg_catalog."default",
    menu character varying(100) COLLATE pg_catalog."default",
    nome character varying(100) COLLATE pg_catalog."default",
    tempoespera character varying(100) COLLATE pg_catalog."default",
    numrepet character varying(100) COLLATE pg_catalog."default",
    tipo_interacao character varying(100) COLLATE pg_catalog."default",
    qtd_respostas character varying(100) COLLATE pg_catalog."default",
    caminhoarq character varying(100) COLLATE pg_catalog."default",
    mensagem character varying(100) COLLATE pg_catalog."default",
    msg_erro character varying(100) COLLATE pg_catalog."default",
    msg_maxrepet character varying(100) COLLATE pg_catalog."default",
    fimdachamada character varying(100) COLLATE pg_catalog."default"
    
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.elementos
    OWNER to postgres;

ALTER SEQUENCE elementos_cdelementos_seq
OWNED BY elementos.cdelementos;
        
        
        '''
    res=p.inserir_bd(sql_db,host_bd,database_bd,user_bd,password_bd)
    
    if res=="Erro":
        plog.log_escreve(f'Não foi possivel criar a tabela {tabela}')
    else:
        plog.log_escreve(f'Tabela {tabela} criada com sucesso!')


##################################    Menu_wpp     ##################################################
    tabela = "Menu_zap"
    sql_db = '''
        DROP TABLE IF EXISTS public.menuredesocial;


CREATE SEQUENCE menuredesocial_cdmenu_seq;



CREATE TABLE IF NOT EXISTS public.menuredesocial
(
    cdmenu integer NOT NULL DEFAULT nextval('menuredesocial_cdmenu_seq'),
    dsmenu character varying(30) COLLATE pg_catalog."default",
    dstempoespera integer,
    dsrepeticao integer,
    dsmsg character varying(400) COLLATE pg_catalog."default",
    dstipo_resposta character varying(100) COLLATE pg_catalog."default",
    dsmsgerro character varying(400) COLLATE pg_catalog."default",
    dsmsgmaxrepeticao character varying(400) COLLATE pg_catalog."default"
    
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.menuredesocial
    OWNER to postgres;

ALTER SEQUENCE menuredesocial_cdmenu_seq
OWNED BY menuredesocial.cdmenu;
        
        
        '''
    res=p.inserir_bd(sql_db,host_bd,database_bd,user_bd,password_bd)
    
    if res=="Erro":
        plog.log_escreve(f'Não foi possivel criar a tabela {tabela}')
    else:
        plog.log_escreve(f'Tabela {tabela} criada com sucesso!')



##################################    Menu_voz     ##################################################
    tabela = "Menu_voz"
    sql_db = '''
        DROP TABLE IF EXISTS public.menuvoz;


CREATE SEQUENCE menuvoz_cdmenu_seq;



CREATE TABLE IF NOT EXISTS public.menuvoz
(
    cdmenu integer NOT NULL DEFAULT nextval('menuvoz_cdmenu_seq'),
    dsmenu character varying(30) COLLATE pg_catalog."default",
    dstempoespera character varying(30) COLLATE pg_catalog."default",
    dsnomearq character varying(30) COLLATE pg_catalog."default",
    dsrepeticao character varying(100) COLLATE pg_catalog."default",
    dstipo_resposta character varying(100) COLLATE pg_catalog."default"  
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.menuvoz
    OWNER to postgres;

ALTER SEQUENCE menuvoz_cdmenu_seq
OWNED BY menuvoz.cdmenu;
        
        
        '''
    res=p.inserir_bd(sql_db,host_bd,database_bd,user_bd,password_bd)
    
    if res=="Erro":
        plog.log_escreve(f'Não foi possivel criar a tabela {tabela}')
    else:
        plog.log_escreve(f'Tabela {tabela} criada com sucesso!')


##################################    Relacionamento     ##################################################
    tabela = "Relacionamento"
    sql_db = '''
        DROP TABLE IF EXISTS public.relacionamento;


CREATE SEQUENCE relacionamento_cdrelacionamento_seq;



CREATE TABLE IF NOT EXISTS public.relacionamento
(
    cdrelacionamento integer NOT NULL DEFAULT nextval('relacionamento_cdrelacionamento_seq'),
    cdmenu integer,
    cdmenupred integer,
    cdmenusuc integer


)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.relacionamento
    OWNER to postgres;

ALTER SEQUENCE relacionamento_cdrelacionamento_seq
OWNED BY relacionamento.cdrelacionamento;
        
        
        '''
    res=p.inserir_bd(sql_db,host_bd,database_bd,user_bd,password_bd)
    
    if res=="Erro":
        plog.log_escreve(f'Não foi possivel criar a tabela {tabela}')
    else:
        plog.log_escreve(f'Tabela {tabela} criada com sucesso!')


##################################    Respostas     ##################################################
    tabela = "Respostas"
    sql_db = '''
        DROP TABLE IF EXISTS public.resposta;


CREATE SEQUENCE resposta_cdresposta_seq;



CREATE TABLE IF NOT EXISTS public.resposta
(
    cdresposta integer NOT NULL DEFAULT nextval('resposta_cdresposta_seq'),
    cdmenu integer,
    cdmenupred integer,
    cdmenusuc integer,
    dsresposta character varying(20) COLLATE pg_catalog."default",
    dsname character varying(20) COLLATE pg_catalog."default",
    dsdesliga boolean,
    dsfila boolean,
    dsmsgresposta character varying(400) COLLATE pg_catalog."default",
    dscuradoria character varying(400) COLLATE pg_catalog."default"
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.resposta
    OWNER to postgres;

ALTER SEQUENCE resposta_cdresposta_seq
OWNED BY resposta.cdresposta;
        
        
        '''
    res=p.inserir_bd(sql_db,host_bd,database_bd,user_bd,password_bd)
    
    if res=="Erro":
        plog.log_escreve(f'Não foi possivel criar a tabela {tabela}')
    else:
        plog.log_escreve(f'Tabela {tabela} criada com sucesso!')
except (Exception, psycopg2.DatabaseError) as error:
    print(error)    