#===================================================================================================== 
# Desenvolvido por Romulo Brandão Silva
# 1 - Voz
#   A - Receptivo
#   B - Ativo
# 2 - Texto
#   A - Receptivo    
#   B - Ativo
# 3 - Integração
#   A - Com Api
#   B - Com BD
#   C - Sem Integração
# 4 - Gravação em Tabelas Auxiliares
#   A - Com 
#   B - Sem
#     
#===================================================================================================== 

import pandas as pd
from pandas import json_normalize
import os
import sys
sys.path.insert(1, './98Funcao')
import sFuncoes as f
import wFuncoes as w
import json, requests
import psycopg2
import psycopg2.extras

try:
    #===================================================================================================== 
    # Incializações
    #===================================================================================================== 

    dirjson  = '\..\\00Entrada'               
    dirlog   = '\..\\80Logs'
    diratual   = os.path.dirname(__file__)    
    direntrada = w.MontaDirAbaixo(diratual,'02Entrada')
    dirlog     = w.MontaDirAbaixo(diratual,'99Log')    
    arqjson    = os.path.join(direntrada,'conexaoWizard.json')      

    #===================================================================================================== 
    # Dados (Entrada)
    #=====================================================================================================         
    parametros  = f.leituraJson(arqjson,False)     
    #===================================================================================================== 
    # Banco de dados
    #===================================================================================================== 
    host_bd      = parametros['host']
    database_bd  = parametros['database']
    user_bd      = parametros['user']
    password_bd  = parametros['password']
    port_bd      = parametros['porta']
    enderco_log  = parametros['enderecoLog']
    #=================================================
    # Abertura do Log
    #=================================================
    locallog     = os.path.join(dirlog,enderco_log)
    plog         = f.Log(locallog)
    if (parametros['zeralog']):
        plog.log_arq_novo()
    plog.log_escreve('Processo do Wizard')
    plog.log_escreve('')
    #=================================================
    # Teste de Conectividade
    #=================================================    
    con,res=w.conecta_bd(host_bd,database_bd,user_bd,password_bd)
    if res=="Erro" :
        plog.log_escreve('Erro na Conexão')
        plog.log_escreve(con)
        exit()
    else:
        plog.log_escreve('Conexão Funcionando!')    

    #==================================================
    #   Separar a Entrada
    #==================================================
    colunas=['Chave',
             'Nome',
             'Acao',
             'Tabela',
             'servicecode'
            ]
    entrada=pd.DataFrame([],columns=colunas)
    elemento       = w.FluxoDF()
    relacionamento = w.RelacionamentoDF()
    cont=0
    for index, rw_ele in elemento.iterrows():
        if rw_ele['Tipo']=='Entrada':
            entrada.at[cont,'Chave']=rw_ele['Chave']
            entrada.at[cont,'Nome'] =rw_ele['Nome']            
            cont=cont+1
            if rw_ele['Tipo_bot']  =='Receptivo':
                entrada.at[cont,'Acao']        ='Inserir'
                entrada.at[cont,'Tabela']      ='ingenium.servicein'
                entrada.at[cont,'servicecode'] =1
            elif rw_ele['Tipo_bot']=='Ativo':
                entrada.at[cont,'Acao']   ='Inserir'
                entrada.at[cont,'Tabela'] ='markings.campaign'
            else:
                plog.log_escreve('Erro Tipo Bot')
                exit()
            #----------------------------------------------

except (Exception, psycopg2.DatabaseError) as error:
    plog.log_escreve(error) 