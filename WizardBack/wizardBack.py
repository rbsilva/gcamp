#===================================================================================================== 
# Desenvolvido por Romulo Brandão Silva
#===================================================================================================== 
import pandas as pd
from pandas import json_normalize
import os
import sys
sys.path.insert(1, './98Funcao')
import sFuncoes as f
import wFuncoes as w
import json, requests
import psycopg2
import psycopg2.extras

try:
    #===================================================================================================== 
    # Incializações
    #===================================================================================================== 

    dirjson  = '\..\\00Entrada'               
    dirlog   = '\..\\80Logs'
    diratual   = os.path.dirname(__file__)    
    direntrada = w.MontaDirAbaixo(diratual,'02Entrada')
    dirlog     = w.MontaDirAbaixo(diratual,'99Log')    
    arqjson    = os.path.join(direntrada,'conexaoWizard.json')      

    #===================================================================================================== 
    # Dados (Entrada)
    #=====================================================================================================         
    parametros  = f.leituraJson(arqjson,False)     
    #===================================================================================================== 
    # Banco de dados
    #===================================================================================================== 
    host_bd      = parametros['host']
    database_bd  = parametros['database']
    user_bd      = parametros['user']
    password_bd  = parametros['password']
    port_bd      = parametros['porta']
    enderco_log  = parametros['enderecoLog']
    #=================================================
    # Abertura do Log
    #=================================================
    locallog     = os.path.join(dirlog,enderco_log)
    plog         = f.Log(locallog)
    if (parametros['zeralog']):
        plog.log_arq_novo()
    plog.log_escreve('Processo do Wizard')
    plog.log_escreve('')
    #=================================================
    # Teste de Conectividade
    #=================================================
    
    con,res=w.conecta_bd(host_bd,database_bd,user_bd,password_bd)
    if res=="Erro" :
        plog.log_escreve('Erro na Conexão')
        plog.log_escreve(con)
        exit()
    else:
        plog.log_escreve('Conexão Funcionando!')
        
    #=================================================
    # Gravando fluxo de campanha no banco
    #=================================================
  
    df = w.FluxoDF()
    dfRel = w.RelacionamentoDF()
    y=0
    for i in range(len(df)):
        if df['Chave'][i] == '1' and df['Tipo_bot'][i] == 'Receptivo':
            print("Criação do Serviço inicial na tabela Service In")
        elif df['Chave'][i] == '1' and df['Tipo_bot'][i] == 'Ativo':
            print('Criar campanha na tabela Campaign')			
        if 	df['Tipo'][i] == 'Menu':
            print("Criação de Menu na tabela menu")
        if df['Tipo'][i] == 'Condicional' and df['Tipo'][i-1]=='Menu':		
            qtd = int(df['Qtd_Respostas'][i])
            print(f'Criação de {qtd} Menus Options')
            for x in range(i+1, (i+1)+qtd):
                y = y + 1
                for z in range(len(dfRel)):
                    if dfRel['chave'][z] == df['Chave'][x]:
                        print(f"{y}- Chave {df['Chave'][x]} / Sucessor {dfRel['sucessor'][z]}")
                        for w in range(i+x, (x+i)+1):
                            print(df['caminhoArq'][w])

                
				
except (Exception, psycopg2.DatabaseError) as error:
    plog.log_escreve(error)  
