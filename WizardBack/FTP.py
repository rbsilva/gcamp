from encodings import utf_8
import ftplib
from datetime import datetime
import logging
import shutil

HOSTNAME = "ftp.dlptest.com"
USERNAME = "dlpuser@dlptest.com"
PASSWORD = "eUj8GeW55SvYaswqUyDSm5v6N"

def downloadFtp():
    
    try:
        ftp = ftplib.FTP(encoding='ISO-8859-1') 

        ftp.connect(HOSTNAME)
        ftp.sendcmd("USER ")
        ftp.sendcmd("PASS ")
        print('conectou')

        ftp.cwd('Documentos')
        ftp.retrlines('LIST') 
        print('mudou de pasta')

        arquivo=datetime.now().strftime('%Y%m%d')+"_.ZIP"
        destino="C\\Dev\\"+arquivo
        try:
            ftp.retrbinary("RETR "+arquivo ,open(arquivo,'wb').write)
            ftp.close()
            logging.info('Conexao FTP fechada')
            logging.info('Download concluido '+arquivo )
            shutil.move(arquivo,destino)
            logging.info('Arquivo movido para '+destino)
            status = 'OK'
        except Exception as e:
            logging.info('Erro no Download'+e )
            status = 'Erro no Download'+e
    except Exception as erroLogin:
        logging.info('Erro Login '+erroLogin)
        status='Erro Login'+erroLogin
    return status





def uploadFTP():
    try:
        ftp_server = ftplib.FTP(HOSTNAME, USERNAME, PASSWORD) 
        ftp_server.encoding = "utf-8"
        filename = "File Name"
        with open(filename, "rb") as file: 
            
            ftp_server.storbinary(f"STOR {filename}", file) 
        ftp_server.dir() 
        ftp_server.quit()
        
    except Exception as erroLogin:
        logging.info('Erro Login '+erroLogin)
        status='Erro Login'+erroLogin
    return status    