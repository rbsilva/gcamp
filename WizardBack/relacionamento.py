#===================================================================================================== 
# Desenvolvido por Romulo Brandão Silva
#===================================================================================================== 
import pandas as pd
import os
import sys
sys.path.insert(1, './98Funcao')
import sFuncoes as f
import wFuncoes as w
import json, requests
import psycopg2
import psycopg2.extras

# cliente =  'cliente'
# fluxo = 'fluxo'
# canal = 'canal'

# elementos = pd.read_excel(f".\\00Entrada\\{cliente}_{fluxo}_{canal}.xlsx", sheet_name="Elementos")
# relacionamento = pd.read_excel(f".\\00Entrada\\{cliente}_{fluxo}_{canal}.xlsx", sheet_name="Relacionamento")

# print(elementos)


try:
    #===================================================================================================== 
    # Incializações
    #===================================================================================================== 

    dirjson  = '\..\\00Entrada'               
    dirlog   = '\..\\80Logs'
    diratual   = os.path.dirname(__file__)    
    direntrada = w.MontaDirAbaixo(diratual,'02Entrada')
    dirlog     = w.MontaDirAbaixo(diratual,'99Log')    
    arqjson    = os.path.join(direntrada,'conexaoWizard.json')      

    #===================================================================================================== 
    # Dados (Entrada)
    #=====================================================================================================         
    parametros  = f.leituraJson(arqjson,False)     
    #===================================================================================================== 
    # Banco de dados
    #===================================================================================================== 
    host_bd      = parametros['host']
    database_bd  = parametros['database']
    user_bd      = parametros['user']
    password_bd  = parametros['password']
    port_bd      = parametros['porta']
    enderco_log  = parametros['enderecoLog']
    #=================================================
    # Abertura do Log
    #=================================================
    locallog     = os.path.join(dirlog,enderco_log)
    plog         = f.Log(locallog)
    if (parametros['zeralog']):
        plog.log_arq_novo()
    plog.log_escreve('Processo do Wizard')
    plog.log_escreve('')
    #=================================================
    # Teste de Conectividade
    #=================================================
    

    con,res=w.conecta_bd(host_bd,database_bd,user_bd,password_bd)
    if res=="Erro" :
        plog.log_escreve('Erro na Conexão')
        plog.log_escreve(con)
        exit()
    else:
        plog.log_escreve('Conexão Funcionando!')
        

    #=================================================
    # Gravando configuração de campanha no banco
    #=================================================
    
    js = {"relacionamento":
[{
	"chave":"1",
	"predecessor":"0",
	"sucessor":"2"},
	
	{"chave":"2",
	"predecessor":"1",
	"sucessor":"3"},
	
	{"chave":"3",
	"predecessor":"2",
	"sucessor":"4"},
	
	{"chave":"3",
	"predecessor":"2",
	"sucessor":"5"},
	
	{"chave":"4",
	"predecessor":"3",
	"sucessor":"6"},
	
	{"chave":"6",
	"predecessor":"4",
	"sucessor":"8"},
	
	{"chave":"5",
	"predecessor":"3",
	"sucessor":"7"},
	
	{"chave":"7",
	"predecessor":"5",
	"sucessor":"9"},
	
	
]}

    def cargaRelacionamento(js):
        lista = js['relacionamento']
        for i in lista:
            sql = f"insert into relacionamento (cdmenu, cdmenupred, cdmenusuc) values ({i['chave']}, {i['predecessor']}, {i['sucessor']})"
            result =  w.inserir_bd(sql,host_bd,database_bd,user_bd,password_bd)
        return result
        
    def cargaElementos(df):                  
        for i, r in df.iterrows():    
            sql = f'''INSERT INTO public.elementos(cliente, fluxo, tipo_bot, canal, classe, tipo, chave, menu, nome, tempoespera, numrepet, tipo_interacao, qtd_respostas, caminhoarq, mensagem, msg_erro, msg_maxrepet, fimdachamada)
        VALUES ('{r['Cliente']}', '{r['Fluxo']}', '{r['Tipo_bot']}', '{r['Canal']}','{r['Classe']}', '{r['Tipo']}', '{r['Chave']}', '{r['Menu']}', '{r['Nome']}', '{r['Tempoespera']}', '{r['NumRepet']}', '{r['Interacao']}', '{r['Qtd_Respostas']}', '{r['caminhoArq']}', '{r['Mensagem']}', '{r['Msg_erro']}', '{r['Msg_maxRepet']}', '{r['FimdaChamada']}');''' 
            result = w.inserir_bd(sql,host_bd,database_bd,user_bd,password_bd)
        return result

except (Exception, psycopg2.DatabaseError) as error:
    plog.log_escreve(error)
