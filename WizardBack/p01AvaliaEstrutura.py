#===================================================================================================== 
# Desenvolvido por Romulo Brandão Silva
#===================================================================================================== 

import pandas as pd
from pandas import json_normalize
import os
import sys
sys.path.insert(1, './98Funcao')
import sFuncoes as f
import wFuncoes as w
import json, requests
import psycopg2
import psycopg2.extras

try:
    #===================================================================================================== 
    # Incializações
    #===================================================================================================== 

    dirjson  = '\..\\00Entrada'               
    dirlog   = '\..\\80Logs'
    diratual   = os.path.dirname(__file__)    
    direntrada = w.MontaDirAbaixo(diratual,'02Entrada')
    dirlog     = w.MontaDirAbaixo(diratual,'99Log')    
    arqjson    = os.path.join(direntrada,'conexaoWizard.json')      

    #===================================================================================================== 
    # Dados (Entrada)
    #=====================================================================================================         
    parametros  = f.leituraJson(arqjson,False)     
    #===================================================================================================== 
    # Banco de dados
    #===================================================================================================== 
    host_bd      = parametros['host']
    database_bd  = parametros['database']
    user_bd      = parametros['user']
    password_bd  = parametros['password']
    port_bd      = parametros['porta']
    enderco_log  = parametros['enderecoLog']
    #=================================================
    # Abertura do Log
    #=================================================
    locallog     = os.path.join(dirlog,enderco_log)
    plog         = f.Log(locallog)
    if (parametros['zeralog']):
        plog.log_arq_novo()
    plog.log_escreve('Processo do Wizard')
    plog.log_escreve('')
    #=================================================
    # Teste de Conectividade
    #=================================================    
    con,res=w.conecta_bd(host_bd,database_bd,user_bd,password_bd)
    if res=="Erro" :
        plog.log_escreve('Erro na Conexão')
        plog.log_escreve(con)
        exit()
    else:
        plog.log_escreve('Conexão Funcionando!')    
    #=================================================
    # Inicialaizações
    #=================================================
    matriz_erro=pd.DataFrame([], columns=['cdErro', 'onde', 'chave'])
    #=================================================
    # Leitura dos Json se correto Carga no Banco
    #=================================================
    elemento       = w.FluxoDF()
    relacionamento = w.RelacionamentoDF()
    #=================================================
    # 1 - Avalia Relacionamento Orfão, chave, sucessor e predecessor 
    #=================================================    
    fgvalidou_erro       = 0
    #------------------------------------
    qtd_rel              = len(relacionamento)
    qtd_rel_achou        = 0              
    #------------------------------------
    qtd_rel_sucess       = 0
    qtd_rel_achou_sucess = 0
    #------------------------------------
    qtd_rel_pred         = 0
    qtd_rel_achou_pred   = 0
    #------------------------------------
    conterro=0
    for index, rw_rel in relacionamento.iterrows():
        # Contagem de Candidatos
        fgchave      =0
        fgsucessor   =0
        fgpredecessor=0
        if rw_rel['sucessor']!='':
            qtd_rel_sucess= qtd_rel_sucess+1            
        if rw_rel['predecessor']!='':
            qtd_rel_pred  = qtd_rel_pred+1
        for ix, rw_ele in elemento.iterrows():
            # Contagem encontraddos chave
            if rw_rel['chave'] == rw_ele['Chave']:
                qtd_rel_achou = qtd_rel_achou + 1
                fgchave       =1
            # Contagem encontraddos sucessores
            if rw_rel['sucessor']!='':
                if rw_rel['sucessor'] == rw_ele['Chave']:
                    qtd_rel_achou_sucess = qtd_rel_achou_sucess + 1
                    fgsucessor   =1
            # Contagem encontraddos predecessores
            if rw_rel['predecessor']!='':
                if rw_rel['predecessor'] == rw_ele['Chave']:
                    qtd_rel_achou_pred = qtd_rel_achou_pred + 1
                    fgpredecessor=1
        # Não Achei Chave
        if fgchave==0:
            conterro=conterro+1
            matriz_erro=w.adciona_erro('9001','Chave Principal',rw_rel['chave'],matriz_erro,conterro-1)
        
        # Não Achei Sucessor
        if fgsucessor==0 and rw_rel['sucessor']!='':
            conterro=conterro+1
            matriz_erro=w.adciona_erro('9002','Chave Sucessora',rw_rel['sucessor'],matriz_erro,conterro-1)            
        
        # Não Achei Predecessor
        if fgpredecessor==0 and rw_rel['predecessor']!='':
            conterro=conterro+1
            matriz_erro=w.adciona_erro('9003','Chave Predecessora',rw_rel['predecessor'],matriz_erro,conterro-1)                        

    if qtd_rel_achou!=qtd_rel:        
        qtd_erros_rel_orfao=qtd_rel-qtd_rel_achou
        plog.log_escreve(f'Erro Relacionamento Elementos Orfãos {qtd_erros_rel_orfao}')
        fgvalidou_erro=1
        
    if qtd_rel_achou_sucess!=qtd_rel_sucess:
        qtd_erros_rel_orfao=qtd_rel_sucess-qtd_rel_achou_sucess
        plog.log_escreve(f'Erro Relacionamento Sucessores Orfãos {qtd_erros_rel_orfao}')
        fgvalidou_erro=1
    
    if qtd_rel_achou_pred!=qtd_rel_pred-1:
        qtd_erros_rel_orfao=qtd_rel_pred-qtd_rel_achou_pred
        plog.log_escreve(f'Erro Relacionamento Predecessor Orfãos {qtd_erros_rel_orfao}')
        fgvalidou_erro=1
    
    if fgvalidou_erro==1:
        exit()

    #=================================================
    # 2 - Consistencia de Preenchimento
    #   A - Menu        --> Condicional
    #   B - Condicional --> Pelo uma Resposta
    #   C - Resposta    <-- Condicional
    #   D - Anuncio     <-- Resposta
    #   E - Anuncio     <-- Entrada
    #   F - Entrada     = ter que ser única
    #   G - Entrada     <-- não pode ter predecessor
    #   H - Entrada     --> não pode ser sucessor
    #   H - Saida     --> não pode ter sucessor
    
    #=================================================
    erros = 0
    tipo_pred_ele = ""
    for i, rel in relacionamento.iterrows():
        chave = rel['chave']
        sucessor = rel['sucessor']
        pred = rel['predecessor']
        for x, ele in elemento.iterrows():
            #Armazenando o tipo da chave
            if chave == ele['Chave']:
                tipo_ele = ele['Tipo']
                next
            #Armazenando o tipo do sucessor da chave    
            if sucessor == ele['Chave']:
                tipo_suc_ele = ele['Tipo']
                next   
            #Armazenando o tipo do predecessador da chave       
            if pred == ele['Chave']:
                tipo_pred_ele = ele['Tipo'] 
                next 

        #A - Menu        --> Condicional         
        if tipo_ele == "Menu" and tipo_suc_ele != "Condicional":
            plog.log_escreve(f'Erro: O sucessor do menu deve ser uma condicional. Chave do Erro {rel["chave"]}')
            conterro=conterro+1
            matriz_erro=w.adciona_erro('9004','sucessor do menu',rel['chave'],matriz_erro,conterro-1)
            fgvalidou_erro=1
            next

        #B - Condicional --> Pelo uma Resposta         
        if tipo_ele == "Condicional" and tipo_suc_ele != "Resposta":
            plog.log_escreve(f'Erro: O sucessor da Condicional deve ser uma resposta. Chave do Erro {rel["chave"]}')
            conterro=conterro+1
            matriz_erro=w.adciona_erro('9005','sucessor da Condicional',rel['chave'],matriz_erro,conterro-1)
            fgvalidou_erro=1
            next

        #C - Resposta    <-- Condicional         
        if tipo_ele == "Resposta" and tipo_pred_ele != "Condicional":
            plog.log_escreve(f'Erro: O predecessor da Resposta deve ser uma Condicional. Chave do Erro {rel["chave"]}')
            conterro=conterro+1
            matriz_erro=w.adciona_erro('9006','predecessor da Resposta',rel['chave'],matriz_erro,conterro-1)
            fgvalidou_erro=1
            next

        #D - Anuncio     <-- Resposta    
        #E - Anuncio     <-- Entrada       
        if tipo_ele == "Anuncio" and tipo_pred_ele != "Resposta" and tipo_pred_ele != "Entrada" :
            plog.log_escreve(f'Erro: O predecessor do Anuncio deve ser uma Resposta ou uma Entrada. Chave do Erro {rel["chave"]}')
            conterro=conterro+1
            matriz_erro=w.adciona_erro('9007','predecessor do Anuncio',rel['chave'],matriz_erro,conterro-1)
            fgvalidou_erro=1
            next

        #F - Entrada     = ter que ser única
        if tipo_ele == "Entrada" and tipo_pred_ele != "":
            plog.log_escreve(f'Erro: O predecessor da Entrada não deve existir. Chave do Erro {rel["chave"]}')
            conterro=conterro+1
            matriz_erro=w.adciona_erro('9008','predecessor da Entrada',rel['chave'],matriz_erro,conterro-1)
            fgvalidou_erro=1
            next    
        #G - Entrada     --> não pode ser sucessor
        
        if tipo_suc_ele == "Entrada":
            plog.log_escreve(f'Erro: A Entrada não pode ser sucessor. Chave do Erro {rel["chave"]}')
            conterro=conterro+1
            matriz_erro=w.adciona_erro('9009','Entrada não pode ser sucessor',rel['chave'],matriz_erro,conterro-1)
            fgvalidou_erro=1
            next
        #H - Saida     --> não pode ter sucessor
        if tipo_ele == "Saida" and tipo_suc_ele != "":
            plog.log_escreve(f'Erro: A Saida não pode ter sucessor. Chave do Erro {rel["chave"]}')
            conterro=conterro+1
            matriz_erro=w.adciona_erro('9009','A Saida não pode ter sucessor',rel['chave'],matriz_erro,conterro-1)
            fgvalidou_erro=1
            next

    if fgvalidou_erro==1:
        exit()   
    plog.log_escreve('Fim de Processamento OK')

except (Exception, psycopg2.DatabaseError) as error:
    plog.log_escreve(error) 