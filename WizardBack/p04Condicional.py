#===================================================================================================== 
# Desenvolvido por Romulo Brandão Silva
# 1 - Voz
#   A - Receptivo
#   B - Ativo
# 2 - Texto
#   A - Receptivo    
#   B - Ativo
# 3 - Integração
#   A - Com Api
#   B - Com BD
#   C - Sem Integração
# 4 - Gravação em Tabelas Auxiliares
#   A - Com 
#   B - Sem
#     
#===================================================================================================== 

import pandas as pd
from pandas import json_normalize
import os
import sys
sys.path.insert(1, './98Funcao')
import sFuncoes as f
import wFuncoes as w
import json, requests
import psycopg2
import psycopg2.extras

try:
    #===================================================================================================== 
    # Incializações
    #===================================================================================================== 

    dirjson  = '\..\\00Entrada'               
    dirlog   = '\..\\80Logs'
    diratual   = os.path.dirname(__file__)    
    direntrada = w.MontaDirAbaixo(diratual,'02Entrada')
    dirlog     = w.MontaDirAbaixo(diratual,'99Log')    
    arqjson    = os.path.join(direntrada,'conexaoWizard.json')      

    #===================================================================================================== 
    # Dados (Entrada)
    #=====================================================================================================         
    parametros  = f.leituraJson(arqjson,False)     
    #===================================================================================================== 
    # Banco de dados
    #===================================================================================================== 
    host_bd      = parametros['host']
    database_bd  = parametros['database']
    user_bd      = parametros['user']
    password_bd  = parametros['password']
    port_bd      = parametros['porta']
    enderco_log  = parametros['enderecoLog']
    #=================================================
    # Abertura do Log
    #=================================================
    locallog     = os.path.join(dirlog,enderco_log)
    plog         = f.Log(locallog)
    if (parametros['zeralog']):
        plog.log_arq_novo()
    plog.log_escreve('Processo do Wizard')
    plog.log_escreve('')
    #=================================================
    # Teste de Conectividade
    #=================================================    
    con,res=w.conecta_bd(host_bd,database_bd,user_bd,password_bd)
    if res=="Erro" :
        plog.log_escreve('Erro na Conexão')
        plog.log_escreve(con)
        exit()
    else:
        plog.log_escreve('Conexão Funcionando!')    

    #==================================================
    #   Separar a Condicional
    #==================================================
    colunas_cond=['Chave',
             'Nome',
             'Qtd_Respostas'
            ]
    condicional=pd.DataFrame([],columns=colunas_cond)
    elemento       = w.FluxoDF()
    relacionamento = w.RelacionamentoDF()
    cont=0
    for i, ele in elemento.iterrows():
        cont=cont+1
        if ele['Tipo'] == 'Condicional':
            condicional.at[cont, 'Chave'] = ele['Chave']
            condicional.at[cont, 'Nome'] = ele['Nome']
            condicional.at[cont, 'Qtd_Respostas'] = ele['Qtd_Respostas']
            
    plog.log_escreve('Condicional adicionadas no DF')         
            #----------------------------------------------

    #==================================================
    #   Separar a Resposta
    #==================================================
    colunas_resp=['Chave',
             'Nome'
            ]
    resposta=pd.DataFrame([],columns=colunas_resp)
    cont=0
    for i, ele in elemento.iterrows():
        cont=cont+1
        if ele['Tipo'] == 'Resposta':
            resposta.at[cont, 'Chave'] = ele['Chave']
            resposta.at[cont, 'Nome'] = ele['Nome']
            
            
    plog.log_escreve('Respostas adicionadas no DF')         
            #----------------------------------------------

    #==================================================
    #   Separar a Anuncio
    #==================================================
    colunas_anun=['Chave',
             'Nome',
             'caminhoArq',
             'Mensagem',
	         'Msg_erro',
             'Msg_maxRepet',
             'FimdaChamada'
            ]
    anuncio=pd.DataFrame([],columns=colunas_anun)
    cont=0
    for i, ele in elemento.iterrows():
        cont=cont+1
        if ele['Tipo'] == 'Mensagem':
            anuncio.at[cont, 'Chave'] = ele['Chave']
            anuncio.at[cont, 'Nome'] = ele['Nome']
            anuncio.at[cont, 'caminhoArq'] = ele['caminhoArq']
            anuncio.at[cont, 'Mensagem'] = ele['Mensagem']
            anuncio.at[cont, 'Msg_erro'] = ele['Msg_erro']
            anuncio.at[cont, 'Msg_maxRepet'] = ele['Msg_maxRepet']
            anuncio.at[cont, 'FimdaChamada'] = ele['FimdaChamada']
            
            
    plog.log_escreve('Anuncios adicionados no DF') 
    # print(condicional)
    # print(resposta)
    print(anuncio)        
            #----------------------------------------------

except (Exception, psycopg2.DatabaseError) as error:
    plog.log_escreve(error) 