#===================================================================================================== 
# Processo de Carga de Dados
# Fábio     - 07/06/2022 - Utiliza os Dados AS IS de Entrada
# Fábio     - 28/06/2022 - Utiliza os Dados do End Point
#===================================================================================================== 
import pandas as pd
import os
import sys
sys.path.insert(1, './98Funcao')
import sFuncoes as f
import pFuncoes as p
import json, requests
import psycopg2
import psycopg2.extras
import pandas as pandas

try:    
    #===================================================================================================== 
    # Incializações
    #===================================================================================================== 
    dirjson  = '\..\\00Entrada'               
    dirlog   = '\..\\80Logs'
    diratual   = os.path.dirname(__file__)    
    direntrada = p.MontaDirAbaixo(diratual,'02Entrada')
    dirlog     = p.MontaDirAbaixo(diratual,'99Log')    
    arqjson    = os.path.join(direntrada,'conexaoGCamp.json')      

    #===================================================================================================== 
    # Dados (Entrada)
    #=====================================================================================================         
    parametros  = f.leituraJson(arqjson,False)     
    #===================================================================================================== 
    # Banco de dados
    #===================================================================================================== 
    host_bd      = parametros['host']
    database_bd  = parametros['database']
    user_bd      = parametros['user']
    password_bd  = parametros['password']
    port_bd      = parametros['porta']
    enderco_log  = parametros['enderecoLog']
    aws_host     = parametros['aws_host']    
    aws_database = parametros['aws_database']
    aws_user     = parametros['aws_user']    
    aws_password = parametros['aws_password']
    #=================================================
    # Abertura do Log
    #=================================================
    locallog     = os.path.join(dirlog,enderco_log)
    plog         = f.Log(locallog)
    if (parametros['zeralog']):
        plog.log_arq_novo()
    plog.log_escreve('')
    plog.log_escreve('Processo de Carga de Dados dos Leads')
    plog.log_escreve('')
    #=================================================
    # Teste de Conectividade
    #=================================================
    con,res=p.conecta_bd(host_bd,database_bd,user_bd,password_bd)
    if res=="Erro" :
        plog.log_escreve('Erro na Conexão')
        plog.log_escreve(con)
        exit()
    else:
        plog.log_escreve('Conexão Funcionando Banco Maquina Vendas Principal!')    

    conaws,res=p.conecta_bd(aws_host,aws_database,aws_user,aws_password)
    if res=="Erro" :
        plog.log_escreve('Erro na Conexão')
        plog.log_escreve(con)
        exit()
    else:
        plog.log_escreve('Conexão Funcionando Banco AWS1!')
    
    #=====================================================================   
    # 1 - Vou verificar qual a maquina vou fazer a requisição
    #=====================================================================   
    #sql_db       =p.cadastro_sqls('Porta_Geracao_Dados')
    #resultado,res=p.consulta_bd(sql_db,aws_host,aws_database,aws_user,aws_password)
    #if res=='Ok':
    #    porta=resultado[0][0]    
    #    if porta=='' :        
    #        plog.log_escreve('Portas Habilitadas {}'.format(porta))                                    


    #=================================================
    # Manipulação das Tabelas
    #=================================================        
    
    # Verifica a Quantidade de Campanhas com
    #   - Status Ativo 
    #   - Dentro do inicio e fim
    #   - Falat Hora 
    
    sql_db=p.cadastro_sqls('CampCont_StatusAtiva_IntervaloValido')
    resultado,res=p.consulta_bd(sql_db,host_bd,database_bd,user_bd,password_bd)
    if res=='Ok':
        qtdcampanha=resultado[0][0]    
        if qtdcampanha==1 :        
            plog.log_escreve('Qtd Campanhas Ativas dentro do Periodo {}'.format(qtdcampanha))
            # Localizando as Informações para carga
            sql_db=p.cadastro_sqls('CampLista_StatusAtiva_IntervaloValido')
            resultado,res = p.consulta_bd(sql_db,host_bd,database_bd,user_bd,password_bd)
            if res=='Ok':
                vcdcampanha     = resultado[0][0]
                vdscampanha     = resultado[0][1]
                vdStipoacesso   = resultado[0][2]
                vdsdiscopath    = resultado[0][3]
                vdsdiscofile    = resultado[0][4]        
                vnmbasehost     = resultado[0][5]
                vnmbasedatabase = resultado[0][6]
                vnmbasedtabela  = resultado[0][7]
                vnmbasedporta   = resultado[0][8]
                vnmbasedlogin   = resultado[0][9]
                vnmbasedsenha   = resultado[0][10]        
                dsarqseparador  = resultado[0][11]
                if vdStipoacesso=='File':
                    plog.log_escreve('Arquivo de Carga Disponível no Diretório {}, Nome {}'.format(vdsdiscopath,vdsdiscofile))            
                    localfile       = os.path.join(vdsdiscopath,vdsdiscofile)       
                    con,res=p.conecta_bd(host_bd,database_bd,user_bd,password_bd)                                                                 
                    dados=p.carga_pandas(localfile,dsarqseparador)
                    qtdregistros=len(dados)
                    plog.log_escreve('Quantidade de Linhas no Arquivos {}, {}'.format(vdsdiscofile,qtdregistros))                                                                
                    res=p.carga_texto(con, dados,'Lead',True)
                    if res=='Ok':
                        sql_db='''
                                select count(*) from lead;
                               '''
                        resultado,res = p.consulta_bd(sql_db,host_bd,database_bd,user_bd,password_bd)   
                        quantidade=resultado[0][0]                                          
                        plog.log_escreve('Dados Carregados {}'.format(quantidade))                        
                    else:
                        plog.log_escreve('Quantidade de Linhas no Arquivos {}, {}'.format(vdsdiscofile,qtdregistros))                        
                elif vdStipoacesso=='Banco':

                    plog.log_escreve('Arquivo de Carga Disponível no Host {}, Banco {}, Tabela {}'.format(vnmbasehost,vnmbasedatabase,vnmbasedtabela))   
                else:
                    plog.log_escreve('Tipo Acesso não Identificado')                
            else:
                plog.log_escreve('Erro na Consulta {}'.format(sql_db))                
        elif qtdcampanha==0:
            plog.log_escreve('Não existem Campanhas Ativas dentro do Período')        
        else:
            plog.log_escreve('Mais de um campanha cadastrada Ativa e Dentro do Período')        
    else:
        plog.log_escreve('Erro na Consulta {}'.format(sql_db))                
    
except (Exception, psycopg2.DatabaseError) as error:
    plog.log_escreve(error)