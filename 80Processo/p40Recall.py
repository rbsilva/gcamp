import pandas as pd
import os
import sys
sys.path.insert(1, './98Funcao')
import sFuncoes as f
import pFuncoes as p
import requests
import psycopg2
import psycopg2.extras
import json as js
from datetime import date

try:
    #===================================================================================================== 
    # Incializações
    #===================================================================================================== 
    diratual   = os.path.dirname(__file__)    
    direntrada = p.MontaDirAbaixo(diratual,'02Entrada')
    dirlog     = p.MontaDirAbaixo(diratual,'99Log')    
    arqjson    = os.path.join(direntrada,'conexaoGCamp.json')      

    #===================================================================================================== 
    # Dados (Entrada)
    #=====================================================================================================         
    parametros  = f.leituraJson(arqjson,False)     
    #===================================================================================================== 
    # Banco de dados
    #===================================================================================================== 
    host_bd      = parametros['host']
    database_bd  = parametros['database']
    user_bd      = parametros['user']
    password_bd  = parametros['password']
    port_bd      = parametros['porta']
    enderco_log  = parametros['enderecoLog']
    campoendereco= parametros['campoendereco']
    host_ing      = parametros['host_ingenium']
    database_ing  = parametros['database_ingenium']
    user_ing      = parametros['user_ingenium']
    password_ing  = parametros['password_ingenium']
    port_ing      = parametros['porta_ingenium']
    #=================================================
    # Abertura do Log
    #=================================================
    locallog     = os.path.join(dirlog,enderco_log)
    plog         = f.Log(locallog)
    if (parametros['zeralog']):
        plog.log_arq_novo()
    plog.log_escreve('Processo de Recall')
    plog.log_escreve('')
    #=================================================
    # Teste de Conectividade A5
    #=================================================
    con,res=p.conecta_bd(host_bd,database_bd,user_bd,password_bd)
    if res=="Erro" :
        plog.log_escreve('Erro na Conexão Banco A5')
        plog.log_escreve(con)
        exit()
    else:
        plog.log_escreve('Conexão Funcionando Banco A5!')

    #=================================================
    # Teste de Conectividade A5
    #=================================================
    con_ing,res_ing=p.conecta_bd(host_ing,database_ing,user_ing,password_ing)
    if res=="Erro" :
        plog.log_escreve('Erro na Conexão Banco Ingenium')
        plog.log_escreve(con)
        exit()
    else:
        plog.log_escreve('Conexão Funcionando Banco Ingenium!')

    #=================================================
    # Manipulação das Tabelas
    # Inclusão: Menu Motivo = Sim and Não chegou a DataAgendamento
    # Exclusão: Confirmação Endereço = Não - Feito
    # Exclusão: Não tem agenda
    # Exclusão: Já agendados
    #=================================================    
    # Não confirmou o endereço   
    sql_noEnd = '''select id,parameter1 as cpf, date_service as data from markings.customer_markings D right join
                    (select id_customermarkings, mms_send from markings.mms_customer_markings A right join
                    (select cdrid from ingenium.chosen_option 
                        where menuid=11 and 
                            optionid=24 and 
                            extract(day from datehour)=extract(day from now()) and
                            extract(month from datehour)=extract(month from now()) and
                            extract(year from datehour)=extract(year from now()) ) B
                            
                    on B.cdrid = A.mms_send ) C	

                    on c.id_customermarkings = d.id'''
    
    # Chegou até o menu de agendamento e não está agendado
    sql_agendamento = '''select id,parameter1 as cpf, date_service as data from markings.customer_markings D right join
                        (select id_customermarkings, mms_send from markings.mms_customer_markings A right join
                        (select cdrid from ingenium.chosen_option 
                            where menuid=12 and
                                
                                extract(day from datehour)=extract(day from now()) and
                                extract(month from datehour)=extract(month from now()) and
                                extract(year from datehour)=extract(year from now()) ) B
                                
                        on B.cdrid = A.mms_send ) C	

                        on c.id_customermarkings = d.id
                        where obs_confirmation is null
                        '''
    #Chegou ate o motivo, mas não ouviu as datas de agendamento
    sql_motivo = '''select id,parameter1 as cpf, date_service as data from markings.customer_markings D right join
                                                (select id_customermarkings, mms_send from markings.mms_customer_markings A right join
                                                (select distinct(cdrid) from ingenium.chosen_option
                            where 
                                                        menuid = 10 and
                                                        extract(day from datehour)=extract(day from now()) and
                                                        extract(month from datehour)=extract(month from now()) and
                                                        extract(year from datehour)=extract(year from now()) and 
                                                        cdrid not in
                        (
                        select cdrid from ingenium.chosen_option 
                                                    where 
                                                        menuid = 12 and
                                                        extract(day from datehour)=extract(day from now()) and
                                                        extract(month from datehour)=extract(month from now()) and
                                                        extract(year from datehour)=extract(year from now()) 

                        ) ) B
                                                        
                        on B.cdrid = A.mms_send ) C	

                        on c.id_customermarkings = d.id
                        where obs_confirmation is null
                        '''                    
    # DataFrame de Sem Endereço
    reg, res = p.consulta_bd(sql_noEnd,host_ing,database_ing,user_ing,password_ing)
    df_noEnd = pd.DataFrame(reg, columns=['id', 'parameter', 'data'])
    # DataFrame de Não agendados
    reg, res = p.consulta_bd(sql_agendamento,host_ing,database_ing,user_ing,password_ing)
    df_agendamento = pd.DataFrame(reg, columns=['id', 'parameter', 'data'])
    # DataFrame de Motivo
    reg, res = p.consulta_bd(sql_motivo,host_ing,database_ing,user_ing,password_ing)
    df_motivo = pd.DataFrame(reg, columns=['id', 'parameter', 'data'])
    # Data atual
    dataatual = date.today()
    amd = dataatual.strftime('%Y%m%d')
    # Cria Query de Insert
    for i, row in df_noEnd.iterrows():
        insert = f'''Insert into recall (cdcelula, chave_lead, motivo, tipo) 
                    values ('','{row['parameter']}', 'Não confirmou o Endereço', 'Exclusão') '''
        res = p.inserir_bd(insert,host_bd,database_bd,user_bd,password_bd)

    for i, row in df_agendamento.iterrows():
        insert = f'''Insert into recall (cdcelula, chave_lead, motivo, tipo) 
                    values ('','{row['parameter']}', 'Não escolheu agenda', 'Inclusão') '''
        res = p.inserir_bd(insert,host_bd,database_bd,user_bd,password_bd)

    for i, row in df_motivo.iterrows():
        insert = f'''Insert into recall (cdcelula, chave_lead, motivo, tipo) 
                    values ('','{row['parameter']}', 'Não chegou na agenda', 'Inclusão') '''
        res = p.inserir_bd(insert,host_bd,database_bd,user_bd,password_bd)       
    plog.log_escreve('Incluiu na tabela Recall!')
    plog.log_escreve('')

except (Exception, psycopg2.DatabaseError) as error:
    plog.log_escreve(error)

# sql_all = '''select distinct cpf from resultado_campanha where extract(day from datainicio)=extract(day from now()) and extract(month from datainicio)=extract(month from now()) and (resultado_cliente = '100' or resultado_cliente = '300' or razao= 'Número Inexistente')'''.encode('utf-8').strip()

# resultado = f.consulta_bd(sql_all)    
# df_bd= resultado
# data_atual = date.today()
# data_hoje = data_atual.strftime('%d/%m/%Y')

# json={
#   "agente": "agente_001",
#   "data": data_hoje
# }
# req = requests.post('http://3.229.194.219:9096/api/agendamento/monitor_pa',json=json)
# print(req)
# json = js.loads(req.text)

# with open('opa.txt','w') as arquivo:
#   arquivo.write(str(req.text))

# df = pd.json_normalize(json, record_path = ['content'])
# #df.to_csv('leads.csv', index=False)
# print(df)


# cont = 0
# df_erro = pd.DataFrame([], columns=['cpf'])
# for i, row in df.iterrows():
#     if row['Tarefa'] == "Agenda" and row['Status'] == "Erro":
#         chave = row['Chave'].split('|')
#         df_erro.at[cont, 'cpf'] = chave[0]
#         cont+=1

# sql=''''''
# f.inserir_bd(sql)