#===================================================================================================== 
# Processo de Marcação de Score, Tratamento de Endereço e Tratamento de Telefone
# Fábio     - 24/06/2022 - Calcula dados deriavados da celula para o APP
#===================================================================================================== 
import pandas as pd
import os
import sys
sys.path.insert(1, './98Funcao')
import sFuncoes as f
import pFuncoes as p
import json, requests
import psycopg2
import psycopg2.extras

import pandas as pandas

try:
    #===================================================================================================== 
    # Incializações
    #===================================================================================================== 
    diratual   = os.path.dirname(__file__)    
    direntrada = p.MontaDirAbaixo(diratual,'02Entrada')
    dirlog     = p.MontaDirAbaixo(diratual,'99Log')    
    arqjson    = os.path.join(direntrada,'conexaoGCamp.json')      

    #===================================================================================================== 
    # Dados (Entrada)
    #=====================================================================================================         
    parametros  = f.leituraJson(arqjson,False)     
    #===================================================================================================== 
    # Banco de dados
    #===================================================================================================== 
    host_bd      = parametros['host']
    database_bd  = parametros['database']
    user_bd      = parametros['user']
    password_bd  = parametros['password']
    port_bd      = parametros['porta']
    enderco_log  = parametros['enderecoLog']
    campoendereco= parametros['campoendereco']
    taxalighora  = parametros['taxalighora']
    #=================================================
    # Abertura do Log
    #=================================================
    locallog     = os.path.join(dirlog,enderco_log)
    plog         = f.Log(locallog)
    if (parametros['zeralog']):
        plog.log_arq_novo()
    plog.log_escreve('Processo de Carga de Dados dos Leads')
    plog.log_escreve('')
    #=================================================
    # Teste de Conectividade
    #=================================================
    con,res=p.conecta_bd(host_bd,database_bd,user_bd,password_bd)
    if res=="Erro" :
        plog.log_escreve('Erro na Conexão')
        plog.log_escreve(con)
        exit()
    else:
        plog.log_escreve('Conexão Funcionando!')
    #==========================================================================
    # Controle
    #==========================================================================        
    sql_db =p.cadastro_sqls('ContCont_StatusAtiva_IntervaloValido')
    con,res=p.conecta_bd(host_bd,database_bd,user_bd,password_bd)   
    dados  = pd.read_sql_query(sql_db, con)
    #colunas=[''
    #
    #            cdcampanha
    #            cdcelula
    #            cstatus
    #            dhinsert
    # Controle Vazio
    if dados['contagem'][0]==0:         
        plog.log_escreve('Controle sem Dados')
        sql_db =p.cadastro_sqls('CampCont_StatusAtiva_IntervaloValido')
        dados  = pd.read_sql_query(sql_db, con)
        if dados['contagem'][0]==0:
            plog.log_escreve('Nenhuma Campanha a ser Carregada')
        else:
            plog.log_escreve('Existem {} Campanha para carregar no controle'.format(len(dados)))
            sql_db =p.cadastro_sqls('CelulaCont_StatusAtiva_IntervaloValido')
            dados  = pd.read_sql_query(sql_db, con)
            if dados['contagem'][0]==0:         
                plog.log_escreve('Nenhuma Celula a ser Carregada')
            else:
                sql_db =p.cadastro_sqls('CelulaLista_StatusAtiva_IntervaloValido')
                dados  = pd.read_sql_query(sql_db, con)
                # inclussão de Dados
                for index, linha in dados.iterrows():      
                    plog.log_escreve('Campanha {} celula {} {}'.format(dados.at[index,'cdcampanha'],
                                                                    dados.at[index,'cdcelula'],
                                                                    dados.at[index,'dscelula']))                

    else:
        plog.log_escreve('Controle com Dados')
        
except (Exception, psycopg2.DatabaseError) as error:
    plog.log_escreve(error)