#===================================================================================================== 
# Processo de Carga de Dados
# Fábio     - 07/06/2022 - Utiliza os Dados AS IS de Entrada
# Fábio     - 28/06/2022 - Utiliza os Dados do End Point
#===================================================================================================== 
import pandas as pd
import os
import sys

from zmq import NULL
sys.path.insert(1, './98Funcao')
import sFuncoes as f
import pFuncoes as p
import json, requests
import psycopg2
import psycopg2.extras
import pandas as pandas

try:    
    #===================================================================================================== 
    # Incializações
    #===================================================================================================== 
    dirjson         = '\..\\00Entrada'               
    dirlog          = '\..\\80Logs'
    diratual        = os.path.dirname(__file__)    
    direntrada      = p.MontaDirAbaixo(diratual,'02Entrada')
    dirlog          = p.MontaDirAbaixo(diratual,'99Log')    
    arqjson         = os.path.join(direntrada  ,'conexaoGCamp.json')              
    #===================================================================================================== 
    # Dados (Entrada)
    #=====================================================================================================         
    parametros  = f.leituraJson(arqjson,False)     
    #===================================================================================================== 
    # Banco de dados
    #===================================================================================================== 
    host_bd      = parametros['host']
    database_bd  = parametros['database']
    user_bd      = parametros['user']
    password_bd  = parametros['password']
    port_bd      = parametros['porta']
    enderco_log  = parametros['enderecoLog']
    aws_host     = parametros['aws_host']    
    aws_database = parametros['aws_database']
    aws_user     = parametros['aws_user']    
    aws_password = parametros['aws_password']
    #=================================================
    # Abertura do Log
    #=================================================
    locallog     = os.path.join(dirlog,enderco_log)
    plog         = f.Log(locallog)
    if (parametros['zeralog']):
        plog.log_arq_novo()
    plog.log_escreve('')
    plog.log_escreve('Processo de Carga de Dados dos Leads')
    plog.log_escreve('')
    #=================================================
    # Teste de Conectividade
    #=================================================
    con,res=p.conecta_bd(host_bd,database_bd,user_bd,password_bd)
    if res=="Erro" :
        plog.log_escreve('Erro na Conexão')
        plog.log_escreve(con)
        exit()
    else:
        plog.log_escreve('Conexão Funcionando Banco Maquina Vendas Principal!')    
    
    sql_db=p.cadastro_sqls('CampCont_StatusAtiva_IntervaloValido')
    resultado,res=p.consulta_bd(sql_db,host_bd,database_bd,user_bd,password_bd)
    if res=='Ok':
        qtdcampanha=resultado[0][0]    
        if qtdcampanha==1 :        
            plog.log_escreve('Qtd Campanhas Ativas dentro do Periodo {}'.format(qtdcampanha))
            # Localizando as Informações para carga
            sql_db=p.cadastro_sqls('CampLista_StatusAtiva_IntervaloValido')
            resultado,res = p.consulta_bd(sql_db,host_bd,database_bd,user_bd,password_bd)
            if res=='Ok':
                vcdcampanha     = resultado[0][0]
                vdscampanha     = resultado[0][1]
                vdStipoacesso   = resultado[0][2]
                vdsdiscopath    = resultado[0][3]
                vdsdiscofile    = resultado[0][4]        
                vnmbasehost     = resultado[0][5]
                vnmbasedatabase = resultado[0][6]
                vnmbasedtabela  = resultado[0][7]
                vnmbasedporta   = resultado[0][8]
                vnmbasedlogin   = resultado[0][9]
                vnmbasedsenha   = resultado[0][10]        
                dsarqseparador  = resultado[0][11]
                if vdStipoacesso=='File':
                    plog.log_escreve('Arquivo de Carga Disponível no Diretório {}, Nome {}'.format(vdsdiscopath,vdsdiscofile))            
                    localfile       = os.path.join(vdsdiscopath,vdsdiscofile)       
                    con,res=p.conecta_bd(host_bd,database_bd,user_bd,password_bd)                                                                 
                    dados=p.carga_pandas(localfile,dsarqseparador)
                    qtdregistros=len(dados)
                    plog.log_escreve('Quantidade de Linhas no Arquivos {}, {}'.format(vdsdiscofile,qtdregistros))                                                                
                    res=p.carga_texto(con, dados,'Lead',True)
                    if res=='Ok':
                        sql_db='''
                                select count(*) from lead;
                               '''
                        resultado,res = p.consulta_bd(sql_db,host_bd,database_bd,user_bd,password_bd)   
                        quantidade=resultado[0][0]                                          
                        plog.log_escreve('Dados Carregados {}'.format(quantidade))                        
                    else:
                        plog.log_escreve('Quantidade de Linhas no Arquivos {}, {}'.format(vdsdiscofile,qtdregistros))                        
                elif vdStipoacesso=='Banco':
                    plog.log_escreve('Arquivo de Carga Disponível no Host {}, Banco {}, Tabela {}'.format(vnmbasehost,vnmbasedatabase,vnmbasedtabela))   
                elif vdStipoacesso=='Json':
                    #--------------------------------------------------
                    # Programa Temporario do Dorival
                    #--------------------------------------------------                    
                    conexaoMaquinaVendas    = psycopg2.connect(host     =host_bd, 
                                                               database =database_bd,
                                                               user     =user_bd, 
                                                               password =password_bd)
                    cur = conexaoMaquinaVendas.cursor()                    
                    # sql = "select porta from endpoint where habilitado=true"        
                    # cur.execute(sql)
                    # rowE = cur.fetchone()
                    dados       = json.dumps({"agente": "agente_001","arquivo": "","system": "siebel6"})
                    headers     = {'Content-Type': 'application/json'}
                    #endpoint    = "http://3.229.194.219:"+''.join(rowE)+"/api/agendamento/retorno_extracao_leads"
                    #response    = requests.request("POST", endpoint, headers=headers, data=dados)
                    endpoint    = "http://3.229.194.219:9096/api/agendamento/retorno_extracao_leads"
                    response    = requests.request("POST", endpoint, headers=headers, data=dados)
                    resultado   = json.loads(response.content)['content']
                    erro        = resultado['result']
                    if erro=='OK':
                        plog.log_escreve('Conexão JSON OK')                        
                        leads_mailing = resultado['leads_mailing']                        
                        for i in range(len(leads_mailing)):
                            if leads_mailing[i]['COD_TIPO_DOCUMENTO']			is not None:
                                pass
                            else:
                                leads_mailing[i]['COD_TIPO_DOCUMENTO']	 = ''

                            if leads_mailing[i]['COD_CAMPANHA']					is not None:
                                pass
                            else:
                                leads_mailing[i]['COD_CAMPANHA']		 = ''								
                            if leads_mailing[i]['COD_CPF_CGC']					is not None:
                                pass
                            else:
                                leads_mailing[i]['COD_CPF_CGC']			 = ''		
                            if leads_mailing[i]['NU_CONTRATO']					is not None:
                                pass
                            else:
                             leads_mailing[i]['NU_CONTRATO']			 = ''  
                            if leads_mailing[i]['NRC_CLI']						is not None:
                                pass
                            else:
                             leads_mailing[i]['NRC_CLI']				 = ''
                            if leads_mailing[i]['COD_FILIAL']					is not None:
                                pass
                            else:
                             leads_mailing[i]['COD_FILIAL']			 = ''
                            if leads_mailing[i]['COD_CLI']						is not None:
                                pass
                            else:
                             leads_mailing[i]['COD_CLI']				 = ''
                            if leads_mailing[i]['COD_RESIDENCIA_CLI']			is not None:
                                pass
                            else:
                             leads_mailing[i]['COD_RESIDENCIA_CLI']	 = ''
                            if leads_mailing[i]['NOME_CLI']						is not None:
                                pass
                            else:
                             leads_mailing[i]['NOME_CLI']			 = ''
                            if leads_mailing[i]['FONE_1']						is not None:
                                pass
                            else:
                             leads_mailing[i]['FONE_1']				 = ''
                            if leads_mailing[i]['FONE_2']						is not None:
                                pass
                            else:
                             leads_mailing[i]['FONE_2']				 = ''
                            if leads_mailing[i]['FONE_3']						is not None:
                                pass
                            else:
                             leads_mailing[i]['FONE_3']				 = ''
                            if leads_mailing[i]['FONE_4']						is not None:
                                pass
                            else:
                             leads_mailing[i]['FONE_4']				 = ''
                            if leads_mailing[i]['EMAIL_CLI']					is not None:
                                pass
                            else:
                             leads_mailing[i]['EMAIL_CLI']			 = ''
                            if leads_mailing[i]['TIPO_LOGRADOURO']				is not None:
                                pass
                            else:
                             leads_mailing[i]['TIPO_LOGRADOURO']		 = ''
                            if leads_mailing[i]['LOGRADOURO']					is not None:
                                pass
                            else:
                             leads_mailing[i]['LOGRADOURO']			 = ''
                            if leads_mailing[i]['NUMERO']						is not None:
                                pass
                            else:
                             leads_mailing[i]['NUMERO']				 = ''
                            if leads_mailing[i]['COMPLEMENTO']					is not None:
                                pass
                            else:
                             leads_mailing[i]['COMPLEMENTO']			 = ''
                            if leads_mailing[i]['BAIRRO']						is not None:
                                pass
                            else:
                             leads_mailing[i]['BAIRRO']				 = ''
                            if leads_mailing[i]['CIDADE']						is not None:
                                pass
                            else:
                             leads_mailing[i]['CIDADE']				 = ''
                            if leads_mailing[i]['COD_LOCALIDADE']				is not None:
                                pass
                            else:
                             leads_mailing[i]['COD_LOCALIDADE']		 = ''
                            if leads_mailing[i]['CEP']							is not None:
                                pass
                            else:
                             leads_mailing[i]['CEP']					 = ''
                            if leads_mailing[i]['UF']							is not None:
                                pass
                            else:
                             leads_mailing[i]['UF']					 = ''
                            if leads_mailing[i]['CANAL_VENDA']					is not None:
                                pass
                            else:
                             leads_mailing[i]['CANAL_VENDA']			 = ''
                            if leads_mailing[i]['PDV_BOV']						is not None:
                                pass
                            else:
                             leads_mailing[i]['PDV_BOV']				 = ''
                            if leads_mailing[i]['COD_SEGMENTO']				    is not None:
                                pass
                            else:
                             leads_mailing[i]['COD_SEGMENTO']		 = ''
                            if leads_mailing[i]['DESC_SEGMENTO']				is not None:
                                pass
                            else:
                             leads_mailing[i]['DESC_SEGMENTO']		 = ''
                            if leads_mailing[i]['COD_EMPRESA_CONTATO']			is not None:
                                pass
                            else:
                             leads_mailing[i]['COD_EMPRESA_CONTATO']	 = ''
                            if leads_mailing[i]['CAMPOX1']						is not None:
                                pass
                            else:
                             leads_mailing[i]['CAMPOX1']				 = ''
                            if leads_mailing[i]['CAMPOX2']						is not None:
                                pass
                            else:
                             leads_mailing[i]['CAMPOX2']				 = ''
                            if leads_mailing[i]['CAMPOX3']						is not None:
                                pass
                            else:
                             leads_mailing[i]['CAMPOX3']				 = ''
                            if leads_mailing[i]['CAMPOX4']						is not None:
                                pass
                            else:
                             leads_mailing[i]['CAMPOX4']				 = ''
                            if leads_mailing[i]['CAMPOX5']						is not None:
                                pass
                            else:
                             leads_mailing[i]['CAMPOX5']				 = ''
                            if leads_mailing[i]['CAMPOX6']						is not None:
                                pass
                            else:
                             leads_mailing[i]['CAMPOX6']				 = ''
                            if leads_mailing[i]['CAMPOX7']						is not None:
                                pass
                            else:
                             leads_mailing[i]['CAMPOX7']				 = ''
                            if leads_mailing[i]['CAMPOX8']						is not None:
                                pass
                            else:
                             leads_mailing[i]['CAMPOX8']				 = ''
                            if leads_mailing[i]['CAMPOX9']						is not None:
                                pass
                            else:
                             leads_mailing[i]['CAMPOX9']				 = ''
                            if leads_mailing[i]['CAMPOX10']						is not None:
                                pass
                            else:
                             leads_mailing[i]['CAMPOX10']			 = ''
                            if leads_mailing[i]['CAMPOX11']						is not None:
                                pass
                            else:
                             leads_mailing[i]['CAMPOX11']			 = ''
                            if leads_mailing[i]['CAMPOX12']						is not None:
                                pass
                            else:
                             leads_mailing[i]['CAMPOX12']			 = ''
                            if leads_mailing[i]['CAMPOX13']						is not None:
                                pass
                            else:
                             leads_mailing[i]['CAMPOX13']			 = ''
                            if leads_mailing[i]['CAMPOX14']						is not None:
                                pass
                            else:
                             leads_mailing[i]['CAMPOX14']			 = ''
                            if leads_mailing[i]['CAMPOX15']						is not None:
                                pass
                            else:
                             leads_mailing[i]['CAMPOX15']			 = ''
                            if leads_mailing[i]['CAMPOX16']						is not None:
                                pass
                            else:
                             leads_mailing[i]['CAMPOX16']			 = ''
                            if leads_mailing[i]['CAMPOX17']						is not None:
                                pass
                            else:
                             leads_mailing[i]['CAMPOX17']			 = ''
                            if leads_mailing[i]['CAMPOX18']						is not None:
                                pass
                            else:
                             leads_mailing[i]['CAMPOX18']			 = ''
                            if leads_mailing[i]['CAMPOX19']						is not None:
                                pass
                            else:
                             leads_mailing[i]['CAMPOX19']			 = ''
                            if leads_mailing[i]['CAMPOX20']						is not None:
                                pass
                            else:
                             leads_mailing[i]['CAMPOX20']			 = ''
                            if leads_mailing[i]['CAMPOX21']						is not None:
                                pass
                            else:
                             leads_mailing[i]['CAMPOX21']			 = ''

                        arquivo_Json    = os.path.join(direntrada,'ent_json.json')  
                        #Começa a Mágica ou a Tragédia                    
                        with open(arquivo_Json, 'w', encoding='utf-8') as t:
                            json.dump(resultado, t, ensure_ascii=False, indent=4)
                        plog.log_escreve('Arquivo do Json gerado em disco: '+arquivo_Json)
                        cur = conexaoMaquinaVendas.cursor()
                        z=0
                        cont_mail=0
                        for [COD_CAMPANHA,COD_TIPO_DOCUMENTO,COD_CPF_CGC,NU_CONTRATO,NRC_CLI,COD_FILIAL,COD_CLI,COD_RESIDENCIA_CLI,NOME_CLI,FONE_1,FONE_2,FONE_3,FONE_4,
                        EMAIL_CLI,TIPO_LOGRADOURO,LOGRADOURO,NUMERO,COMPLEMENTO,BAIRRO,CIDADE,COD_LOCALIDADE,CEP,UF,CANAL_VENDA,PDV_BOV,COD_SEGMENTO,DESC_SEGMENTO,
                        COD_EMPRESA_CONTATO,CAMPOX1,CAMPOX2,CAMPOX3,CAMPOX4,CAMPOX5,CAMPOX6,CAMPOX7,CAMPOX8,CAMPOX9,CAMPOX10,CAMPOX11,CAMPOX12,CAMPOX13,CAMPOX14,
                        CAMPOX15,CAMPOX16,CAMPOX17,CAMPOX18,CAMPOX19,CAMPOX20,CAMPOX21] in leads_mailing: 
                            sql = "insert into lead (COD_CAMPANHA,COD_TIPO_DOCUMENTO,COD_CPF_CGC,NU_CONTRATO,NRC_CLI,COD_FILIAL,COD_CLI,COD_RESIDENCIA_CLI,NOME_CLI,FONE_1,FONE_2,FONE_3,FONE_4,EMAIL_CLI,TIPO_LOGRADOURO,LOGRADOURO,NUMERO,COMPLEMENTO,BAIRRO,CIDADE,COD_LOCALIDADE,CEP,UF,CANAL_VENDA,PDV_BOV,COD_SEGMENTO,DESC_SEGMENTO,COD_EMPRESA_CONTATO,CAMPOX1,CAMPOX2,CAMPOX3,CAMPOX4,CAMPOX5,CAMPOX6,CAMPOX7,CAMPOX8,CAMPOX9,CAMPOX10,CAMPOX11,CAMPOX12,CAMPOX13,CAMPOX14,CAMPOX15,CAMPOX16,CAMPOX17,CAMPOX18,CAMPOX19,CAMPOX20,CAMPOX21) values (%s, %s, %s, %s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"                                
                            cur.execute(sql, [leads_mailing[cont_mail]['COD_CAMPANHA'],leads_mailing[cont_mail]['COD_TIPO_DOCUMENTO'],leads_mailing[cont_mail]['COD_CPF_CGC'],leads_mailing[cont_mail]['NU_CONTRATO'],leads_mailing[cont_mail]['NRC_CLI'],leads_mailing[cont_mail]['COD_FILIAL'],leads_mailing[cont_mail]['COD_CLI'],leads_mailing[cont_mail]['COD_RESIDENCIA_CLI'],leads_mailing[cont_mail]['NOME_CLI'],leads_mailing[cont_mail]['FONE_1'],leads_mailing[cont_mail]['FONE_2'],leads_mailing[cont_mail]['FONE_3'],leads_mailing[cont_mail]['FONE_4'],leads_mailing[cont_mail]['EMAIL_CLI'],leads_mailing[cont_mail]['TIPO_LOGRADOURO'],leads_mailing[cont_mail]['LOGRADOURO'],leads_mailing[cont_mail]['NUMERO'],leads_mailing[cont_mail]['COMPLEMENTO'],leads_mailing[cont_mail]['BAIRRO'],leads_mailing[cont_mail]['CIDADE'],leads_mailing[cont_mail]['COD_LOCALIDADE'],leads_mailing[cont_mail]['CEP'],leads_mailing[cont_mail]['UF'],leads_mailing[cont_mail]['CANAL_VENDA'],leads_mailing[cont_mail]['PDV_BOV'],leads_mailing[cont_mail]['COD_SEGMENTO'],leads_mailing[cont_mail]['DESC_SEGMENTO'],leads_mailing[cont_mail]['COD_EMPRESA_CONTATO'],leads_mailing[cont_mail]['CAMPOX1'],leads_mailing[cont_mail]['CAMPOX2'],leads_mailing[cont_mail]['CAMPOX3'],leads_mailing[cont_mail]['CAMPOX4'],leads_mailing[cont_mail]['CAMPOX5'],leads_mailing[cont_mail]['CAMPOX6'],leads_mailing[cont_mail]['CAMPOX7'],leads_mailing[cont_mail]['CAMPOX8'],leads_mailing[cont_mail]['CAMPOX9'],leads_mailing[cont_mail]['CAMPOX10'],leads_mailing[cont_mail]['CAMPOX11'],leads_mailing[cont_mail]['CAMPOX12'],leads_mailing[cont_mail]['CAMPOX13'],leads_mailing[cont_mail]['CAMPOX14'],leads_mailing[cont_mail]['CAMPOX15'],leads_mailing[cont_mail]['CAMPOX16'],leads_mailing[cont_mail]['CAMPOX17'],leads_mailing[cont_mail]['CAMPOX18'],leads_mailing[cont_mail]['CAMPOX19'],leads_mailing[cont_mail]['CAMPOX20'],leads_mailing[cont_mail]['CAMPOX21']])
                            conexaoMaquinaVendas.commit()
                            z=z+1
                            cont_mail=cont_mail+1
                        plog.log_escreve('Fim - Gravando Banco em MaquinaVendas Mailing: '+str(z))                        
                        #row = cur.fetchone()
                        cur.close()                                                
                    else:
                        plog.log_escreve('Conexão JSON Não OK')                                                        
        else:
            plog.log_escreve('Mais de um campanha cadastrada Ativa e Dentro do Período')        
    else:
        plog.log_escreve('Erro na Consulta {}'.format(sql_db))                    
except (Exception, psycopg2.DatabaseError) as error:
    plog.log_escreve(error)