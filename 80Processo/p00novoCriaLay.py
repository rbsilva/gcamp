#===================================================================================================== 
# Processo de Criação Básica do Ambiente de Dados do Gerenciador de Campanha
# Romulo    - 01/06/2022 - Construção do Módulo Básico
# Fábio     - 02/06/2022 - Estruturação do Processo
#===================================================================================================== 
import pandas as pd
import sys
sys.path.insert(1, './98Funcao')
import sFuncoes as f
import pFuncoes as p
import json, requests
import psycopg2
import psycopg2.extras
import os

try:
    #===================================================================================================== 
    # Incializações
    #===================================================================================================== 
    diratual   = os.path.dirname(__file__)    
    direntrada = p.MontaDirAbaixo(diratual,'02Entrada')
    dirlog     = p.MontaDirAbaixo(diratual,'99Log')    
    arqjson    = os.path.join(direntrada,'conexaoGCamp.json')      

    #===================================================================================================== 
    # Dados (Entrada)
    #=====================================================================================================         
    parametros  = f.leituraJson(arqjson,False)     
    #===================================================================================================== 
    # Banco de dados
    #===================================================================================================== 
    host_bd      = parametros['host']
    database_bd  = parametros['database']
    user_bd      = parametros['user']
    password_bd  = parametros['password']
    port_bd      = parametros['porta']
    enderco_log  = parametros['enderecoLog']

    #=================================================
    # Abertura do Log
    #=================================================
    locallog     = os.path.join(dirlog,enderco_log)
    plog         = f.Log(locallog)
    if (parametros['zeralog']):
        plog.log_arq_novo()
    plog.log_escreve('Iniciando estrutura do banco de dados do Gcamp')
    plog.log_escreve('')
    #=================================================
    # Teste de Conectividade
    #=================================================
    con,res=p.conecta_bd(host_bd,database_bd,user_bd,password_bd)
    if res=="Erro" :
        plog.log_escreve('Erro na Conexão')
        plog.log_escreve(con)
        exit()
    else:
        plog.log_escreve('Conexão Funcionando!')

##################################    CAMPANHA     ##################################################

    tabela = "Campanha"
    sql_db = '''
    DROP TABLE IF EXISTS public.campanha;

CREATE TABLE IF NOT EXISTS public.campanha
(
    cdcampanha integer,
    dscampanha character varying(100) COLLATE pg_catalog."default",
    dtinicio character varying(10) COLLATE pg_catalog."default",
    dtfim character varying(10) COLLATE pg_catalog."default",
    hrinicio character varying(10) COLLATE pg_catalog."default",
    hrfim character varying(10) COLLATE pg_catalog."default",
    cdtipoacesso integer,
    dsdiscopath character varying(200) COLLATE pg_catalog."default",
    dsdiscofile character varying(200) COLLATE pg_catalog."default",
    nmbasehost character varying(100) COLLATE pg_catalog."default",
    nmbasedatabase character varying(100) COLLATE pg_catalog."default",
    nmbasedtabela character varying(100) COLLATE pg_catalog."default",
    nmbasedporta character varying(100) COLLATE pg_catalog."default",
    nmbasedlogin character varying(100) COLLATE pg_catalog."default",
    nmbasedsenha character varying(100) COLLATE pg_catalog."default",
    qtcelulas integer,
    qtleads integer,
    qtexecutados integer,
    cdtipocampanha integer,
    cdstatus integer,
    dsarqseparador character varying(1) COLLATE pg_catalog."default"
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.campanha
    OWNER to postgres;
    
    
    '''
    res=p.inserir_bd(sql_db,host_bd,database_bd,user_bd,password_bd)
    if res=="Erro":
        plog.log_escreve(f'Não foi possivel criar a tabela {tabela}')
    else:
        plog.log_escreve(f'Tabela {tabela} criada com sucesso!')

##################################    CANAL     ##################################################
    tabela = "Canal"
    sql_db = '''
        DROP TABLE IF EXISTS public.canal;


CREATE SEQUENCE canal_cdcanal_seq;



CREATE TABLE IF NOT EXISTS public.canal
(
    cdcanal integer NOT NULL DEFAULT nextval('canal_cdcanal_seq'),
    dscanal character varying(30) COLLATE pg_catalog."default"
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.canal
    OWNER to postgres;

ALTER SEQUENCE canal_cdcanal_seq
OWNED BY canal.cdcanal;
        
        
        '''
    res=p.inserir_bd(sql_db,host_bd,database_bd,user_bd,password_bd)
    
    if res=="Erro":
        plog.log_escreve(f'Não foi possivel criar a tabela {tabela}')
    else:
        plog.log_escreve(f'Tabela {tabela} criada com sucesso!')
##################################    CELULA     ##################################################
    tabela = "Celula"
    sql_db = '''
        DROP TABLE IF EXISTS public.celula;

CREATE SEQUENCE celula_cdcelula_seq;

CREATE TABLE IF NOT EXISTS public.celula
(
    cdcampanha integer,
    cdcelula integer NOT NULL DEFAULT nextval('celula_cdcelula_seq'),
    dscelula character varying(100) COLLATE pg_catalog."default",
    dtinicio character varying(10) COLLATE pg_catalog."default",
    dtfim character varying(10) COLLATE pg_catalog."default",
    hrinicio character varying(10) COLLATE pg_catalog."default",
    hrfim character varying(10) COLLATE pg_catalog."default",
    qtexecutados integer,
    qtleads integer,
    qtdiscagens integer,
    cdestrategiadiscagem integer,
    cdtipocelula integer,
    cdcanal integer,
    cdstatus integer
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.celula
    OWNER to postgres;
	
ALTER SEQUENCE celula_cdcelula_seq
OWNED BY celula.cdcelula;	
	
        
        
        '''
    res=p.inserir_bd(sql_db,host_bd,database_bd,user_bd,password_bd)
    if res=="Erro":
        plog.log_escreve(f'Não foi possivel criar a tabela {tabela}')
    else:
        plog.log_escreve(f'Tabela {tabela} criada com sucesso!')

##################################    Estratégia Discagem     ##################################################
    tabela = "Estratégia Discagem"
    sql_db = '''
        DROP TABLE IF EXISTS public.estrategiadiscagem;

CREATE SEQUENCE estrategiadiscagem_cdestrategiadiscagem_seq;

CREATE TABLE IF NOT EXISTS public.estrategiadiscagem
(
    cdestrategiadiscagem integer NOT NULL DEFAULT nextval('estrategiadiscagem_cdestrategiadiscagem_seq'),
    dsmovimento character varying(10) COLLATE pg_catalog."default",
    qtligacoestel01 integer,
    qtligacoestel02 integer,
    qtligacoestel03 integer,
    qtligacoestel04 integer,
    qtligacoestel05 integer,
    qtligacoestel06 integer
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.estrategiadiscagem
    OWNER to postgres;

ALTER SEQUENCE estrategiadiscagem_cdestrategiadiscagem_seq
OWNED BY estrategiadiscagem.cdestrategiadiscagem;
        '''
    res=p.inserir_bd(sql_db,host_bd,database_bd,user_bd,password_bd)
    if res=="Erro":
        plog.log_escreve(f'Não foi possivel criar a tabela {tabela}')
    else:
        plog.log_escreve(f'Tabela {tabela} criada com sucesso!')
##################################    Projeto     ##################################################
    tabela = "Projeto"
    sql_db = '''
        DROP TABLE IF EXISTS public.projeto;
CREATE SEQUENCE projeto_cdprojeto_seq;
CREATE TABLE IF NOT EXISTS public.projeto
(
    cdprojeto integer  NOT NULL DEFAULT nextval('projeto_cdprojeto_seq'),
    dsprojeto character varying(100) COLLATE pg_catalog."default",
    nmconexaoa5d character varying(100) COLLATE pg_catalog."default",
    nma5dhost character varying(100) COLLATE pg_catalog."default",
    nma5ddatabase character varying(100) COLLATE pg_catalog."default",
    nma5dtabela character varying(100) COLLATE pg_catalog."default",
    nma5dporta character varying(100) COLLATE pg_catalog."default",
    nma5dlogin character varying(100) COLLATE pg_catalog."default",
    nma5dsenha character varying(100) COLLATE pg_catalog."default",
    cda5dcampanha integer,
    dsa5dselect character varying(200) COLLATE pg_catalog."default"
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.projeto
    OWNER to postgres;

ALTER SEQUENCE projeto_cdprojeto_seq
OWNED BY projeto.cdprojeto;
        '''
    res=p.inserir_bd(sql_db,host_bd,database_bd,user_bd,password_bd)
    if res=="Erro":
        plog.log_escreve(f'Não foi possivel criar a tabela {tabela}')
    else:
        plog.log_escreve(f'Tabela {tabela} criada com sucesso!')


    ##################################    Relacionamento Celulas     ##################################################
    tabela = "Relacionamento Celulas"
    sql_db = '''
        
DROP TABLE IF EXISTS public.relacionamentocelulas;

CREATE SEQUENCE relacionamentocelulas_cdrelacionamento_seq;

CREATE TABLE IF NOT EXISTS public.relacionamentocelulas
(
	cdrelacionamento integer NOT NULL DEFAULT nextval('relacionamentocelulas_cdrelacionamento_seq'),
    cdcampanha integer,
    cdcelula integer,
    cdcelulapredecessora integer,
    dsquery character varying(10) COLLATE pg_catalog."default",
    cdfiltro character varying(10) COLLATE pg_catalog."default",
    cdprioridade integer
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.relacionamentocelulas
    OWNER to postgres;
	
ALTER SEQUENCE relacionamentocelulas_cdrelacionamento_seq
OWNED BY relacionamentocelulas.cdrelacionamento;
        '''
    res=p.inserir_bd(sql_db,host_bd,database_bd,user_bd,password_bd)

    if res=="Erro":
        plog.log_escreve(f'Não foi possivel criar a tabela {tabela}')
    else:
        plog.log_escreve(f'Tabela {tabela} criada com sucesso!')

##################################    Status     ##################################################
    tabela = "Status"
    sql_db = '''
        
DROP TABLE IF EXISTS public.status;

CREATE SEQUENCE status_cdstatus_seq;

CREATE TABLE IF NOT EXISTS public.status
(
    cdstatus integer  NOT NULL DEFAULT nextval('status_cdstatus_seq'),
    dsstatus character varying(30) COLLATE pg_catalog."default"
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.status
    OWNER to postgres;
	
ALTER SEQUENCE status_cdstatus_seq
OWNED BY status.cdstatus;	
        '''
    res=p.inserir_bd(sql_db,host_bd,database_bd,user_bd,password_bd)
    if res=="Erro":
        plog.log_escreve(f'Não foi possivel criar a tabela {tabela}')
    else:
        plog.log_escreve(f'Tabela {tabela} criada com sucesso!')


##################################    Tipo Acesso     ##################################################
    tabela = "Tipo Acesso"
    sql_db = '''
        
DROP TABLE IF EXISTS public.tipoacesso;
CREATE SEQUENCE tipoacesso_cdtipoacesso_seq;
CREATE TABLE IF NOT EXISTS public.tipoacesso
(
    cdtipoacesso integer  NOT NULL DEFAULT nextval('tipoacesso_cdtipoacesso_seq'),
    dstipoacesso character varying(30) COLLATE pg_catalog."default"
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.tipoacesso
    OWNER to postgres;
	
ALTER SEQUENCE tipoacesso_cdtipoacesso_seq
OWNED BY tipoacesso.cdtipoacesso;		
        '''
    res=p.inserir_bd(sql_db,host_bd,database_bd,user_bd,password_bd)

    if res=="Erro":
        plog.log_escreve(f'Não foi possivel criar a tabela {tabela}')
    else:
        plog.log_escreve(f'Tabela {tabela} criada com sucesso!')
##################################    Tipo Celula     ##################################################
    tabela = "Tipo Celula"
    sql_db = '''
        
DROP TABLE IF EXISTS public.tipocelula;
CREATE SEQUENCE tipocelula_cdtipocelula_seq;
CREATE TABLE IF NOT EXISTS public.tipocelula
(
    cdtipocelula integer,
    dstipocelula character varying(30) COLLATE pg_catalog."default"
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.tipocelula
    OWNER to postgres;
	
ALTER SEQUENCE tipocelula_cdtipocelula_seq
OWNED BY tipocelula.cdtipocelula;			
        '''
    res=p.inserir_bd(sql_db,host_bd,database_bd,user_bd,password_bd)
    if res=="Erro":
        plog.log_escreve(f'Não foi possivel criar a tabela {tabela}')
    else:
        plog.log_escreve(f'Tabela {tabela} criada com sucesso!')

##################################    Controle     ##################################################
    tabela = "Controle"
    sql_db = '''
        
DROP TABLE IF EXISTS public.controle;

CREATE TABLE IF NOT EXISTS public.controle
(
    cdcontrole integer,
    cdcampanha integer,
    cdcelula integer,
    dtdataprocessamento character varying(8) COLLATE pg_catalog."default",
    dsstatus character varying(20) COLLATE pg_catalog."default"
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.controle
    OWNER to postgres;			
        '''
    res=p.inserir_bd(sql_db,host_bd,database_bd,user_bd,password_bd)
    if res=="Erro":
        plog.log_escreve(f'Não foi possivel criar a tabela {tabela}')
    else:
        plog.log_escreve(f'Tabela {tabela} criada com sucesso!')


##################################    Leads     ##################################################
    tabela = "Leads"
    sql_db = '''
        
DROP TABLE IF EXISTS public.lead;

CREATE TABLE IF NOT EXISTS public.lead
(
    cod_campanha character varying(30) COLLATE pg_catalog."default",
    cod_tipo_documento character varying(1) COLLATE pg_catalog."default",
    cod_cpf_cgc character varying(14) COLLATE pg_catalog."default",
    nu_contrato character varying(15) COLLATE pg_catalog."default",
    nrc_cli character varying(10) COLLATE pg_catalog."default",
    cod_filial character varying(2) COLLATE pg_catalog."default",
    cod_cli character varying(10) COLLATE pg_catalog."default",
    cod_residencia_cli character varying(10) COLLATE pg_catalog."default",
    nome_cli character varying(80) COLLATE pg_catalog."default",
    fone_1 character varying(15) COLLATE pg_catalog."default",
    fone_2 character varying(15) COLLATE pg_catalog."default",
    fone_3 character varying(15) COLLATE pg_catalog."default",
    fone_4 character varying(15) COLLATE pg_catalog."default",
    email_cli character varying(50) COLLATE pg_catalog."default",
    tipo_logradouro character varying(10) COLLATE pg_catalog."default",
    logradouro character varying(100) COLLATE pg_catalog."default",
    numero character varying(10) COLLATE pg_catalog."default",
    complemento character varying(100) COLLATE pg_catalog."default",
    bairro character varying(50) COLLATE pg_catalog."default",
    cidade character varying(50) COLLATE pg_catalog."default",
    cod_localidade character varying(10) COLLATE pg_catalog."default",
    cep character varying(10) COLLATE pg_catalog."default",
    uf character varying(2) COLLATE pg_catalog."default",
    canal_venda character varying(10) COLLATE pg_catalog."default",
    pdv_bov character varying(10) COLLATE pg_catalog."default",
    cod_segmento character varying(10) COLLATE pg_catalog."default",
    desc_segmento character varying(10) COLLATE pg_catalog."default",
    cod_empresa_contato character varying(10) COLLATE pg_catalog."default",
    campox1 character varying(15) COLLATE pg_catalog."default",
    campox2 character varying(10) COLLATE pg_catalog."default",
    campox3 character varying(8) COLLATE pg_catalog."default",
    campox4 character varying(10) COLLATE pg_catalog."default",
    campox5 character varying(8) COLLATE pg_catalog."default",
    campox6 character varying(15) COLLATE pg_catalog."default",
    campox7 character varying(10) COLLATE pg_catalog."default",
    campox8 character varying(10) COLLATE pg_catalog."default",
    campox9 character varying(4) COLLATE pg_catalog."default",
    campox10 character varying(10) COLLATE pg_catalog."default",
    campox11 character varying(100) COLLATE pg_catalog."default",
    campox12 character varying(5) COLLATE pg_catalog."default",
    campox13 character varying(5) COLLATE pg_catalog."default",
    campox14 character varying(5) COLLATE pg_catalog."default",
    campox15 character varying(1) COLLATE pg_catalog."default",
    campox16 character varying(50) COLLATE pg_catalog."default",
    campox17 character varying(10) COLLATE pg_catalog."default",
    campox18 character varying(10) COLLATE pg_catalog."default",
    campox19 character varying(2) COLLATE pg_catalog."default",
    campox20 character varying(8) COLLATE pg_catalog."default",
    campox21 character varying(400) COLLATE pg_catalog."default",
    endtrat character varying(400) COLLATE pg_catalog."default",
    cdlead character varying(100) COLLATE pg_catalog."default",
    cdgrupo01 character varying(10) COLLATE pg_catalog."default",
    cdgrupo02 character varying(10) COLLATE pg_catalog."default",
    cdgrupo03 character varying(10) COLLATE pg_catalog."default",
    cdgrupo04 character varying(10) COLLATE pg_catalog."default",
    cdgrupo05 character varying(10) COLLATE pg_catalog."default",
    cdgrupo06 character varying(10) COLLATE pg_catalog."default",
    cdgrupo07 character varying(10) COLLATE pg_catalog."default",
    cdgrupo08 character varying(10) COLLATE pg_catalog."default",
    cdgrupo09 character varying(10) COLLATE pg_catalog."default",
    cdgrupo10 character varying(10) COLLATE pg_catalog."default",
    cdscore01 character varying(10) COLLATE pg_catalog."default",
    cdscore02 character varying(10) COLLATE pg_catalog."default",
    cdscore03 character varying(10) COLLATE pg_catalog."default"
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.lead
    OWNER to postgres;			
        '''
    res=p.inserir_bd(sql_db,host_bd,database_bd,user_bd,password_bd)

    if res=="Erro":
        plog.log_escreve(f'Não foi possivel criar a tabela {tabela}')
    else:
        plog.log_escreve(f'Tabela {tabela} criada com sucesso!')

##################################    StatusExecucao     ##################################################
    tabela = "StatusExecucao"
    sql_db = '''
        
DROP TABLE IF EXISTS public.statusexecucao;

CREATE TABLE IF NOT EXISTS public.statusexecucao
(
    cdstatus integer,
    dsstatus character varying(100) COLLATE pg_catalog."default"
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.statusexecucao
    OWNER to postgres;			
        '''
    res=p.inserir_bd(sql_db,host_bd,database_bd,user_bd,password_bd)
    if res=="Erro":
        plog.log_escreve(f'Não foi possivel criar a tabela {tabela}')
    else:
        plog.log_escreve(f'Tabela {tabela} criada com sucesso!')


##################################    Controle     ##################################################
    tabela = "Controle"
    sql_db = '''
        
DROP TABLE IF EXISTS public.controle;

CREATE TABLE IF NOT EXISTS public.controle
(
    cdcampanha integer,
    cdcelula integer,
    qtregexecutar character varying(100) COLLATE pg_catalog."default",
    qtregexecutado character varying(100) COLLATE pg_catalog."default",
    cdstatus character varying(100) COLLATE pg_catalog."default"
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.controle
    OWNER to postgres;			
        '''
    res=p.inserir_bd(sql_db,host_bd,database_bd,user_bd,password_bd)
    if res=="Erro":
        plog.log_escreve(f'Não foi possivel criar a tabela {tabela}')
    else:
        plog.log_escreve(f'Tabela {tabela} criada com sucesso!')


##################################    Memoria de Contato     ##################################################
    tabela = "Memoria de Contato"
    sql_db = '''
        
DROP TABLE IF EXISTS public.memoriacontato;

CREATE TABLE IF NOT EXISTS public.memoriacontato
(
    cdcampanha integer,
    dsmenuentrada character varying(100) COLLATE pg_catalog."default",
    dsnomeaudio character varying(100) COLLATE pg_catalog."default",
    dspastaaudio character varying(100) COLLATE pg_catalog."default",
    dscomando character varying(100) COLLATE pg_catalog."default"
    
    
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.memoriacontato
    OWNER to postgres;			
        '''
    res=p.inserir_bd(sql_db,host_bd,database_bd,user_bd,password_bd)
    if res=="Erro":
        plog.log_escreve(f'Não foi possivel criar a tabela {tabela}')
    else:
        plog.log_escreve(f'Tabela {tabela} criada com sucesso!')


##################################    MenuEntradaCampanha     ##################################################
    tabela = "MenuEntradaCampanha"
    sql_db = '''
        
DROP TABLE IF EXISTS public.MenuEntradaCampanha;

CREATE TABLE IF NOT EXISTS public.MenuEntradaCampanha
(
    cdcampanha integer,
    cdcelula integer,
    cdlead integer
    
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.MenuEntradaCampanha
    OWNER to postgres;			
        '''
    res=p.inserir_bd(sql_db,host_bd,database_bd,user_bd,password_bd)
    if res=="Erro":
        plog.log_escreve(f'Não foi possivel criar a tabela {tabela}')
    else:
        plog.log_escreve(f'Tabela {tabela} criada com sucesso!')


##################################    Memoria de Contato     ##################################################
    tabela = "Memoria de Contato"
    sql_db = '''
        
DROP TABLE IF EXISTS public.memoriacontato;

CREATE TABLE IF NOT EXISTS public.memoriacontato
(
    cdcampanha integer,
    dsmenuentrada character varying(100) COLLATE pg_catalog."default",
    dsnomeaudio character varying(100) COLLATE pg_catalog."default",
    dspastaaudio character varying(100) COLLATE pg_catalog."default",
    dscomando character varying(100) COLLATE pg_catalog."default"
    
    
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.memoriacontato
    OWNER to postgres;			
        '''
    res=p.inserir_bd(sql_db,host_bd,database_bd,user_bd,password_bd)
    if res=="Erro":
        plog.log_escreve(f'Não foi possivel criar a tabela {tabela}')
    else:
        plog.log_escreve(f'Tabela {tabela} criada com sucesso!')


##################################   CadastroAudioEntrada     ##################################################
    tabela = "CadastroAudioEntrada"
    sql_db = '''
        
DROP TABLE IF EXISTS public.CadastroAudioEntrada;

CREATE TABLE IF NOT EXISTS public.CadastroAudioEntrada
(
    cdcampanha integer,
    dsmenuentrada character varying(100) COLLATE pg_catalog."default",
    dsnomeaudio character varying(100) COLLATE pg_catalog."default",
    dscaminhoaudio character varying(100) COLLATE pg_catalog."default"
    
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.CadastroAudioEntrada
    OWNER to postgres;			
        '''
    res=p.inserir_bd(sql_db,host_bd,database_bd,user_bd,password_bd)
    if res=="Erro":
        plog.log_escreve(f'Não foi possivel criar a tabela {tabela}')
    else:
        plog.log_escreve(f'Tabela {tabela} criada com sucesso!')




except (Exception, psycopg2.DatabaseError) as error:
    print(error)    