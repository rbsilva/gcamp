# Info
# p11Discador - Grava Dados para Discador Ingenium e Alimenta Banco para BI
# Data: 30/09/2021
# Autor: Dorival
# Versão: 5.0

import sFuncoes
import os
dir=os.path.dirname(__file__)
from datetime import datetime
import pandas as pd
import csv
import xlrd 
import json, requests
import psycopg2

#=================================================
# Variáveis
#=================================================
arquivo_mailing     = os.path.join(dir,'50Leads\\mailing_')+sFuncoes.hoje_anomedia()+'.csv'
arquivo_Json        = os.path.join(dir,'50Leads\\json_')+sFuncoes.hoje_anomedia()+'.json'

#=================================================
# Leitura do Arquivo de parametros
#=================================================
parametros          = sFuncoes.leituraJson(os.path.join(dir,'00Entrada\\conexaoMaquinaVendas.json'),False)

#=================================================
# Abertura do Log
#=================================================
plog           = sFuncoes.Log(os.path.join(dir,parametros['enderecoLog']))
if (parametros['zeralog']):
    plog.log_arq_novo()
plog.log_escreve('Inicio Le Retorno Extracao Leads, Carga nos Bancos')
plog.log_escreve('')


#=================================================
# Cria Conexao Banco de Dados
#=================================================
try:
    conexaoMaquinaVendas    = psycopg2.connect(host='localhost', database='MaquinaVendas',
                                        user='postgres', password='bi')
except Exception as f:
    r=f.args[0]
    plog.log_escreve('MaquinaVendas Conexão - Erro: '+r)    

#=======================================================================================================================================================================
# Leitura Retorno Extracao Leads, Arruma os dados de endereço, grava nos bancos e gera outros arquivos locais no disco
#=======================================================================================================================================================================
plog.log_escreve('Acionando EndPoint ')
#Aciona API e Ler do json
try:
    #Busca EndPoints Habilitados para consulta
    cur = conexaoMaquinaVendas.cursor()
    sql = "select nome from endpoint where habilitado=true"        
    cur.execute(sql)
    rowE = cur.fetchone()
    while rowE is not None: 
        dados       = json.dumps({"agente": "agente_001","arquivo": "","system": "siebel6"})
        headers     = {'Content-Type': 'application/json'}
        endpoint    = "http://3.229.194.219:"+''.join(rowE)+"/api/agendamento/retorno_extracao_leads"
        response    = requests.request("POST", endpoint, headers=headers, data=dados)
        resultado   =json.loads(response.content)['content']
        erro        = resultado['result']
        if erro=='OK': #OK = Nao tem Erro
            if resultado['result']=='OK':
                leads_mailing   = resultado['leads_mailing']
                controleCarga=True
                #Começa a Mágica ou a Tragédia
                if resultado['result']=='OK' and controleCarga==True:
                    #Grava em disco arquivo json
                    with open(arquivo_Json, 'w', encoding='utf-8') as t:
                        json.dump(resultado, t, ensure_ascii=False, indent=4)
                    plog.log_escreve('Arquivo do Json gerado em disco: '+arquivo_Json)

                    #Grava Mailing
                    try:
                        cur = conexaoMaquinaVendas.cursor()
                        z=0
                        cont_mail=0
                        for [COD_CAMPANHA,COD_TIPO_DOCUMENTO,COD_CPF_CGC,NU_CONTRATO,NRC_CLI,COD_FILIAL,COD_CLI,COD_RESIDENCIA_CLI,NOME_CLI,FONE_1,FONE_2,FONE_3,FONE_4,
                        EMAIL_CLI,TIPO_LOGRADOURO,LOGRADOURO,NUMERO,COMPLEMENTO,BAIRRO,CIDADE,COD_LOCALIDADE,CEP,UF,CANAL_VENDA,PDV_BOV,COD_SEGMENTO,DESC_SEGMENTO,
                        COD_EMPRESA_CONTATO,CAMPOX1,CAMPOX2,CAMPOX3,CAMPOX4,CAMPOX5,CAMPOX6,CAMPOX7,CAMPOX8,CAMPOX9,CAMPOX10,CAMPOX11,CAMPOX12,CAMPOX13,CAMPOX14,
                        CAMPOX15,CAMPOX16,CAMPOX17,CAMPOX18,CAMPOX19,CAMPOX20,CAMPOX21] in leads_mailing: 
                            sql = "insert into lead (COD_CAMPANHA,COD_TIPO_DOCUMENTO,COD_CPF_CGC,NU_CONTRATO,NRC_CLI,COD_FILIAL,COD_CLI,COD_RESIDENCIA_CLI,NOME_CLI,FONE_1,FONE_2,FONE_3,FONE_4,EMAIL_CLI,TIPO_LOGRADOURO,LOGRADOURO,NUMERO,COMPLEMENTO,BAIRRO,CIDADE,COD_LOCALIDADE,CEP,UF,CANAL_VENDA,PDV_BOV,COD_SEGMENTO,DESC_SEGMENTO,COD_EMPRESA_CONTATO,CAMPOX1,CAMPOX2,CAMPOX3,CAMPOX4,CAMPOX5,CAMPOX6,CAMPOX7,CAMPOX8,CAMPOX9,CAMPOX10,CAMPOX11,CAMPOX12,CAMPOX13,CAMPOX14,CAMPOX15,CAMPOX16,CAMPOX17,CAMPOX18,CAMPOX19,CAMPOX20,CAMPOX21) values (%s, %s, %s, %s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"        
                            cur.execute(sql, [leads_mailing[cont_mail]['COD_CAMPANHA'],leads_mailing[cont_mail]['COD_TIPO_DOCUMENTO'],leads_mailing[cont_mail]['COD_CPF_CGC'],leads_mailing[cont_mail]['NU_CONTRATO'],leads_mailing[cont_mail]['NRC_CLI'],leads_mailing[cont_mail]['COD_FILIAL'],leads_mailing[cont_mail]['COD_CLI'],leads_mailing[cont_mail]['COD_RESIDENCIA_CLI'],leads_mailing[cont_mail]['NOME_CLI'],leads_mailing[cont_mail]['FONE_1'],leads_mailing[cont_mail]['FONE_2'],leads_mailing[cont_mail]['FONE_3'],leads_mailing[cont_mail]['FONE_4'],leads_mailing[cont_mail]['EMAIL_CLI'],leads_mailing[cont_mail]['TIPO_LOGRADOURO'],leads_mailing[cont_mail]['LOGRADOURO'],leads_mailing[cont_mail]['NUMERO'],leads_mailing[cont_mail]['COMPLEMENTO'],leads_mailing[cont_mail]['BAIRRO'],leads_mailing[cont_mail]['CIDADE'],leads_mailing[cont_mail]['COD_LOCALIDADE'],leads_mailing[cont_mail]['CEP'],leads_mailing[cont_mail]['UF'],leads_mailing[cont_mail]['CANAL_VENDA'],leads_mailing[cont_mail]['PDV_BOV'],leads_mailing[cont_mail]['COD_SEGMENTO'],leads_mailing[cont_mail]['DESC_SEGMENTO'],leads_mailing[cont_mail]['COD_EMPRESA_CONTATO'],leads_mailing[cont_mail]['CAMPOX1'],leads_mailing[cont_mail]['CAMPOX2'],leads_mailing[cont_mail]['CAMPOX3'],leads_mailing[cont_mail]['CAMPOX4'],leads_mailing[cont_mail]['CAMPOX5'],leads_mailing[cont_mail]['CAMPOX6'],leads_mailing[cont_mail]['CAMPOX7'],leads_mailing[cont_mail]['CAMPOX8'],leads_mailing[cont_mail]['CAMPOX9'],leads_mailing[cont_mail]['CAMPOX10'],leads_mailing[cont_mail]['CAMPOX11'],leads_mailing[cont_mail]['CAMPOX12'],leads_mailing[cont_mail]['CAMPOX13'],leads_mailing[cont_mail]['CAMPOX14'],leads_mailing[cont_mail]['CAMPOX15'],leads_mailing[cont_mail]['CAMPOX16'],leads_mailing[cont_mail]['CAMPOX17'],leads_mailing[cont_mail]['CAMPOX18'],leads_mailing[cont_mail]['CAMPOX19'],leads_mailing[cont_mail]['CAMPOX20'],leads_mailing[cont_mail]['CAMPOX21']])
                            z=z+1
                            cont_mail=cont_mail+1
                        plog.log_escreve('Fim - Gravando Banco em MaquinaVendas Mailing: '+str(z))
                        conexaoMaquinaVendas.commit()
                    except Exception as e:
                        resultado=e.args[0]
                        plog.log_escreve('MaquinaVendas Mailign - Err: '+resultado)    
                        controleCarga=False
                else:
                    plog.log_escreve('Erro de acesso ao EndPoint')    
                plog.log_escreve('Processo Encerrado - Gerou CSV Automacao, CSV Mailing, CSV Revisao do Novo Endereco, Gravou na Tabela Leads da Ingenium, Tabela Mailing e Tabela Automacao da Maquina de Vendas - EndPoint'.format(rowE))    
        row = cur.fetchone()
    cur.close()
except Exception as e:
    resultado=e.args[0]
    plog.log_escreve('Erro - Err: ' +resultado)    
    plog.log_escreve('Erro : '      +erro)    
    controleCarga=False

