#===================================================================================================== 
# Processo de Marcação de Score, Tratamento de Endereço e Tratamento de Telefone
# Fábio     - 07/06/2022 - Inicio do Processo
# 1 - Tratemento de Endereço
#   A - Tratar Casa com Letras Depois
#   B - Tratar 

#===================================================================================================== 
import pandas as pd
import os
import sys
sys.path.insert(1, './98Funcao')
import sFuncoes as f
import pFuncoes as p
import json, requests
import psycopg2
import psycopg2.extras

import pandas as pandas

try:
    #===================================================================================================== 
    # Incializações
    #===================================================================================================== 
    diratual   = os.path.dirname(__file__)    
    direntrada = p.MontaDirAbaixo(diratual,'02Entrada')
    dirlog     = p.MontaDirAbaixo(diratual,'99Log')    
    arqjson    = os.path.join(direntrada,'conexaoGCamp.json')      

    #===================================================================================================== 
    # Dados (Entrada)
    #=====================================================================================================         
    parametros  = f.leituraJson(arqjson,False)     
    #===================================================================================================== 
    # Banco de dados
    #===================================================================================================== 
    host_bd      = parametros['host']
    database_bd  = parametros['database']
    user_bd      = parametros['user']
    password_bd  = parametros['password']
    port_bd      = parametros['porta']
    enderco_log  = parametros['enderecoLog']
    campoendereco= parametros['campoendereco']
    #=================================================
    # Abertura do Log
    #=================================================
    locallog     = os.path.join(dirlog,enderco_log)
    plog         = f.Log(locallog)
    if (parametros['zeralog']):
        plog.log_arq_novo()
    plog.log_escreve('Processo de Carga de Dados dos Leads')
    plog.log_escreve('')
    #=================================================
    # Teste de Conectividade
    #=================================================
    con,res=p.conecta_bd(host_bd,database_bd,user_bd,password_bd)
    if res=="Erro" :
        plog.log_escreve('Erro na Conexão')
        plog.log_escreve(con)
        exit()
    else:
        plog.log_escreve('Conexão Funcionando!')
    #=================================================
    # Manipulação das Tabelas
    #=================================================        
    # Conecta no Banco de Dados GCAMP - Verifica Volume da Tabela Leada
    sql_db='''
            select * from Lead
           '''
    con,res=p.conecta_bd(host_bd,database_bd,user_bd,password_bd)
    if res=='Ok':    
        sql_db         = "select * from lead;"
        dados          = pd.read_sql_query(sql_db, con)
        colunas        = dados.columns
        # Monta Layout Extra
        dados['endtrat']  =''
        dados['cdlead']   =''
        dados['cdscore01']=''
        dados['cdscore02']=''
        dados['cdscore03']=''

        dados_tratados = pd.DataFrame([],columns=colunas)
        registro       =0
        caractespeciais=0
        estado         =0
        abreviacoes    =0
        qtdteldig      =0
        qtdtel         =0
        freqtel        =[0,
                         0,
                         0,
                         0,
                         0,
                         0,
                         0,
                         0,
                         0,
                         0]
        imprimi_a_cada =100
        
        regori=0
        regalt=0
        leadalt=0

        listaabrev=f.TrataEnd_CriaAbrev()
        plog.log_escreve('Quantidade de Registros {}'.format(len(dados)))
        plog.log_escreve('---------------------------------------------------------')                
        for index, linhas in dados.iterrows():                 
            # Inicializa Algumas variáveis
            regalt=0
            fgtelfixo=0
            fgtelfixosel=0
            #------------------------------------------------------------
            dados.at[index,'campox13']=dados.at[index,'campox13'].replace('.0','')
            #------------------------------------------------------------
            # Montagem da Chave de Lead
            #------------------------------------------------------------                        
            cpftrat  ='000000000000000'+dados.at[index,'cod_cpf_cgc']
            anomesdia=dados.at[index,'campox17']
            ordem_os =dados.at[index,'campox1']
            ano      =anomesdia[-4:]
            dia      =anomesdia[:2]
            mes      =anomesdia[3:5]
            dados.at[index,'cdlead']=cpftrat[-15:]+'_'+ano+mes+dia+'_'+ordem_os
            #------------------------------------------------------------
            # Tratamento Telefone\Movimenta
            #------------------------------------------------------------                        
            dados.at[index,'fone_1']=dados.at[index,'fone_1'].replace('.0','')
            dados.at[index,'fone_2']=dados.at[index,'fone_2'].replace('.0','')
            dados.at[index,'fone_3']=dados.at[index,'fone_3'].replace('.0','')
            dados.at[index,'fone_4']=dados.at[index,'fone_4'].replace('.0','')        
            cpf=dados.at[index,'cod_cpf_cgc']                         
            telefones=[dados.at[index,'fone_1'],
                       dados.at[index,'fone_2'],
                       dados.at[index,'fone_3'],
                       dados.at[index,'fone_4']]
            telefones,res=p.TrataTelefones(telefones)                        
            if res=='Ok':
                regalt=1                       
                telefones,res=p.MovimentaTel(telefones)
            dados.at[index,'fone_1']=telefones[0]
            dados.at[index,'fone_2']=telefones[1]
            dados.at[index,'fone_3']=telefones[2]
            dados.at[index,'fone_4']=telefones[3]      
            #------------------------------------------------------------
            # Score Montage
            #------------------------------------------------------------


            #------------------------------------------------------------
            # Tratamento de Endereço       
            #------------------------------------------------------------                        
            endereco     =dados.at[registro,(campoendereco)]
            estrutura,res=p.TrataEndereco(endereco,listaabrev)
            if registro % imprimi_a_cada==0:
                plog.log_escreve('Quantidade de Registros Processados {}'.format(registro))                                
            registro=registro+1
            #------------------------------------------------------------
            # Registra no Pandas
            #------------------------------------------------------------
            endertrat=estrutura['tipo']+' '+estrutura['logr']+' '+estrutura['num']+' '+estrutura['compl']+' '+estrutura['bairro']+' '+estrutura['cep']+' '+estrutura['cidade']+' '+estrutura['estado']
            dados.at[index,'endtrat']=endertrat.replace(' ','%20')
            if regalt==1:
                leadalt=leadalt+1                
        #----------------------------------------------------------------
        # Gravar  no Banco as alterações
        #----------------------------------------------------------------
        res=p.carga_texto(con, dados,'Lead',True)
        if res=='Ok':
            sql_db='''
                    select count(*) from lead;
                   '''
        else:
            plog.log_escreve('Erro no Processo de Gravação {}'.format(res))
        resultado,res = p.consulta_bd(sql_db,host_bd,database_bd,user_bd,password_bd)   
        quantidade=resultado[0][0]  
        plog.log_escreve('---------------------------------------------------------')                
        plog.log_escreve('Dados Corrigidos e Carregados {}'.format(quantidade))                        
        plog.log_escreve('---------------------------------------------------------')        
        sql_db=''' 
                    select max(A.contagem) 
                    from (select cdlead,count(*) as contagem from lead group by cdlead) A
               '''
        resultado,res=p.consulta_bd(sql_db,host_bd,database_bd,user_bd,password_bd)   
        quantidade=resultado[0][0]
        if res=='Ok':
            if quantidade==1:
                plog.log_escreve('Chave do Lead OK')
            else:
                plog.log_escreve('Chave do Lead com Duplicidade')
        else:
            plog.log_escreve('Erro no SQl')
        plog.log_escreve('Quantidade de Registros Registros|Modificados {}|{}'.format(registro,leadalt))        
    else:        
        plog.log_escreve('Erro no Acesso ao Banco {}'.format(res))
        
except (Exception, psycopg2.DatabaseError) as error:
    plog.log_escreve(error)