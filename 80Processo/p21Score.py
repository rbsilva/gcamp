#===================================================================================================== 
# Processo de Marcação de Score
#===================================================================================================== 
import pandas as pd
import os
import sys
sys.path.insert(1, './98Funcao')
import sFuncoes as f
import pFuncoes as p
import json, requests
import psycopg2
import psycopg2.extras

import pandas as pandas

try:
    #===================================================================================================== 
    # Incializações
    #===================================================================================================== 
    diratual   = os.path.dirname(__file__)    
    direntrada = p.MontaDirAbaixo(diratual,'02Entrada')
    dirlog     = p.MontaDirAbaixo(diratual,'99Log')    
    arqjson    = os.path.join(direntrada,'conexaoGCamp.json')      

    #===================================================================================================== 
    # Dados (Entrada)
    #=====================================================================================================         
    parametros  = f.leituraJson(arqjson,False)     
    #===================================================================================================== 
    # Banco de dados
    #===================================================================================================== 
    host_bd      = parametros['host']
    database_bd  = parametros['database']
    user_bd      = parametros['user']
    password_bd  = parametros['password']
    port_bd      = parametros['porta']
    enderco_log  = parametros['enderecoLog']
    campoendereco= parametros['campoendereco']
    #=================================================
    # Abertura do Log
    #=================================================
    locallog     = os.path.join(dirlog,enderco_log)
    plog         = f.Log(locallog)
    if (parametros['zeralog']):
        plog.log_arq_novo()
    plog.log_escreve('Processo de Carga de Dados dos Leads')
    plog.log_escreve('')
    #=================================================
    # Teste de Conectividade
    #=================================================
    con,res=p.conecta_bd(host_bd,database_bd,user_bd,password_bd)
    if res=="Erro" :
        plog.log_escreve('Erro na Conexão')
        plog.log_escreve(con)
        exit()
    else:
        plog.log_escreve('Conexão Funcionando!')
    #=================================================
    # Manipulação das Tabelas
    #=================================================        
    # Conecta no Banco de Dados GCAMP - Verifica Volume da Tabela Leada
    sql_db='''
            select * from Lead
           '''
    con,res=p.conecta_bd(host_bd,database_bd,user_bd,password_bd)
    if res=='Ok':
        dados          = pd.read_sql_query(sql_db, con)        
        totreg=0
        # Classes do Score
        classes=[[100.0, 0, 0.262],
                 [79.70, 0, 0.223],
                 [76.74, 0, 0.218],
                 [75.43, 0, 0.215],
                 [72.24, 0, 0.209],
                 [72.24, 0, 0.209],
                 [56.08, 0, 0.179],
                 [55.93, 0, 0.178],
                 [53.46, 0, 0.174],
                 [42.65, 0, 0.153],
                 [37.94, 0, 0,144],
                 [37.33, 0, 0.143],
                 [36.13, 0, 0.141],
                 [34.26, 0, 0.137],
                 [33.88, 0, 0.137],
                 [33.85, 0, 0.137],
                 [28.33, 0, 0.126],
                 [27.91, 0, 0.125],
                 [22.77, 0, 0.116],
                 [21.47, 0, 0.113],
                 [20.03, 0, 0.111],
                 [17.66, 0, 0.106],
                 [16.45, 0, 0.104],
                 [15.00, 0, 0.101],
                 [11.60, 0, 0.095],
                 [10.50, 0, 0.093],
                 [7.500, 0, 0.087],
                 [6.840, 0, 0.086],
                 [1.930, 0, 0.076],
                 [0.000, 0, 0.073]
                ]   
        # DADOS_CRV.BD_DiasContrato               
        qtDiasEsteiraVirgem  =0
        qtDiasEsteiraNovo    =0
        qtDiasEsteiraAtrasado=0
        qtDiasEsteiraVelho   =0        
        #----------------------------------------------------------------------
        # DADOS_CRV.Esteira
        qtEsteira1=0
        qtEsteira2=0
        qtEsteira3=0
        qtEsteiraN=0                        
        #----------------------------------------------------------------------                
        # DADOS_CRV.Regiao
        regiaoSUD=0
        matSUD=['MG',
                'ES',
                'RJ',
                'SP'
               ]
        regiaoSUL=0
        matSUL=['PR',
                'SC',
                'RS'
               ]
        regiaoNDR=0
        matNDR=['MA',
                'PI',
                'CE',
                'RN',
                'PB',
                'PE',                
                'AL',
                'SE',
                'BA'                
               ]
        regiaoCNO=0   
        matCNO=['MS',
                'MT',
                'GO',
                'DF'              
               ]             
        regiaoNO=0
        matNO =['RO',
                'AC',
                'AM',
                'RR',
                'PA',
                'AP',                
                'TO'               
               ]             
        regiaoX=0

        # DADOS_CRV.QtdTelefones
        qtdtel1=0
        qtdtel2=0
        qtdtel3=0        
        qtdtel4=0        

        # Motivo
        qtdMotivo7029=0
        qtdMotivo7016=0
        qtdMotivo7065=0
        qtdMotivo7037=0
        qtdMotivo7017=0
        qtdMotivoOutros=0
        semclasse=0

        # Data Frame com dados não classificados
        colunas=['cod_cpf_cgc']
        nao_class=pd.DataFrame([],columns=colunas)         
        for index, linhas in dados.iterrows():                 
            #-------------------------------------------------------------------
            # Modelo Agendamento/Alo
            #-------------------------------------------------------------------
            # Incialização
            # Esteira de Contrato
            fgDiasEsteiraVirgem  =0
            fgDiasEsteiraNovo    =0
            fgDiasEsteiraAtrasado=0
            fgDiasEsteiraVelho   =0
            # Esteira de Processamento
            fgEsteira1=0
            fgEsteira2=0
            fgEsteira3=0
            fgEsteiraN=0           
            # Regiao
            fgRegiaoSUD=0
            fgRegiaoSUL=0
            fgRegiaoNDR=0
            fgRegiaoCNO=0           
            fgRegiaoNO=0        
            fgRegiaoX=0                         
            # Quatidade de telefones
            fgqtdtel1=0
            fgqtdtel2=0
            fgqtdtel3=0        
            fgqtdtel4=0
            # Motivo
            motivo=''
            fgdMotivo7029=0
            fgdMotivo7016=0
            fgdMotivo7065=0
            fgdMotivo7037=0
            fgdMotivo7017=0
            # Passagens Discador
            qtdPassDiscador=0
            # Migracao Interna
            fgMigracaoInternaSim=0
            fgMigracaoInternaNao=0
            fgMigracaoInternaOut=0
            # DDD Diferente 1,2 telefones
            fgDDDDiferentes=0
                                    
            if dados.at[index,'cod_cpf_cgc']=='2022985907':
                www=0

            # Tratando Dias de Contrato            
            dias_contrato=pd.to_datetime(dados.at[index,'campox17'],format='%d/%m/%Y')-pd.to_datetime(dados.at[index,'campox2'],format='%d/%m/%Y')            
            if dias_contrato.days<=3:
                fgDiasEsteiraVirgem=1
                qtDiasEsteiraVirgem=qtDiasEsteiraVirgem+1
            elif dias_contrato.days<=7:
                fgDiasEsteiraNovo=1
                qtDiasEsteiraNovo=qtDiasEsteiraNovo+1
            elif dias_contrato.days<=14:
                fgDiasEsteiraAtrasado=1
                qtDiasEsteiraAtrasado=qtDiasEsteiraAtrasado+1
            else:
                fgDiasEsteiraVelho=1
                qtDiasEsteiraVelho=qtDiasEsteiraVelho+1
            # Tratando Esteira
            esteira=dados.at[index,'campox12']
            if esteira=='1':
                fgEsteira1=1
                qtEsteira1=qtEsteira1+1
            elif esteira=='2':
                fgEsteira2=1                
                qtEsteira2=qtEsteira2+1
            elif esteira=='3':
                fgEsteira3=1                
                qtEsteira3=qtEsteira3+1
            else:
                fgEsteiraN=1                
                qtEsteiraN=qtEsteiraN+1
            # Tratando Regiao
            uf=dados.at[index,'uf']
            if uf in matSUD:
                fgRegiaoSUD=1
                regiaoSUD  =regiaoSUD+1
            elif uf in matSUL:
                fgRegiaoSUL=1
                regiaoSUL  =regiaoSUL+1                
            elif uf in matNDR:
                fgRegiaoNDR=1
                regiaoNDR  =regiaoNDR+1                
            elif uf in matNO:
                fgRegiaoNO =1
                regiaoNO   =regiaoNO+1                                
            elif uf in matCNO:
                fgRegiaoCNO=1    
                regiaoCNO  =regiaoCNO+1                                                
            else:
                fgRegiaoX  =1
                regiaoX    =regiaoX+1
            # Tratando Numero de Telefones
            tel1=dados.at[index,'fone_1']
            tel2=dados.at[index,'fone_2']
            tel3=dados.at[index,'fone_3']
            tel4=dados.at[index,'fone_4']
            qtdtels=0
            if tel1!='NaN':
                qtdtels=qtdtels+1
            if tel2!='NaN':
                qtdtels=qtdtels+1                
            if tel3!='NaN':
                qtdtels=qtdtels+1                                
            if tel4!='NaN':
                qtdtels=qtdtels+1                                                
            if qtdtels==1:
                fgqtdtel1=1
                qtdtel1=qtdtel1+1                
            if qtdtels==2:       
                fgqtdtel2=1         
                qtdtel2=qtdtel2+1
            if qtdtels==3: 
                fgqtdtel3=1               
                qtdtel3=qtdtel3+1
            if qtdtels==4: 
                fgqtdtel4=1               
                qtdtel4=qtdtel4+1                            
            # Motivo
            motivo=dados.at[index,'campox9']
            if    motivo=='7029':
                fgdMotivo7029=1
                qtdMotivo7029=qtdMotivo7029+1
            elif  motivo=='7016':
                fgdMotivo7016=1
                qtdMotivo7016=qtdMotivo7016+1                
            elif  motivo=='7065':
                fgdMotivo7065=1
                qtdMotivo7065=qtdMotivo7065+1                
            elif  motivo=='7037':
                fgdMotivo7037=1
                qtdMotivo7037=qtdMotivo7037+1                
            elif  motivo=='7017':
                fgdMotivo7017=1
                qtdMotivo7017=qtdMotivo7017+1                
            else:
                qtdMotivoOutros=qtdMotivoOutros+1                
            # Passagens Discador
            qtdPassDiscador=int(dados.at[index,'campox13'])
            # Migração Interna
            if dados.at[index,'campox15']=='S':
                fgMigracaoInternaSim=1
            elif dados.at[index,'campox15']=='N':
                fgMigracaoInternaNao=1                
            else:
                fgMigracaoInternaOut=1                
            # DDD Diferentes
            if dados.at[index,'fone_2']!='NaN':
                if dados.at[index,'fone_1'][:2]!=dados.at[index,'fone_2'][:2]:
                    fgDDDDiferentes=1            

            # Marcação
            # Seguemento 01
                # Leads.BD_diasEsteira is Virgem         AND 
                # Leads.Regiao is CENTRO-OESTE and NORTE AND 
                # Leads.Esteira is 2 AND Leads.Motivo is 7037
            if ( fgDiasEsteiraVirgem==1              and
                (fgRegiaoCNO==1 or fgRegiaoNO==1)    and
                 fgEsteira2==1                       and
                 fgdMotivo7037==1
               ):            
               classes[0][1]=classes[0][1]+1
            # Seguemento 02
                # Leads.BD_diasEsteira is Virgem         AND 
                # Leads.Regiao is SUDESTE and SUL        AND 
                # Leads.PassagensDiscador is 0
            elif ( fgDiasEsteiraVirgem==1              and
                  (fgRegiaoSUD==1 or fgRegiaoSUL==1)   and
                   qtdPassDiscador==0 
                 ):
                 classes[1][1]=classes[1][1]+1
            # Seguemento 03 - OK
                # Leads.BD_diasEsteira is Novo           AND 
                # Leads.Motivo is 7017 and 7037
            elif ( fgDiasEsteiraNovo==1              and
                  (fgdMotivo7017==1 or fgdMotivo7037==1)
                 ):
                 classes[2][1]=classes[2][1]+1
            # Seguemento 04 
                # Leads.BD_diasEsteira is Virgem         AND 
                # Leads.Regiao is CENTRO-OESTE and NORTE AND 
                # Leads.Esteira is 2                     AND 
                # Leads.Motivo is 7017 and 7029
            elif ( fgDiasEsteiraVirgem==1              and
                  (fgRegiaoCNO==1 or fgRegiaoNO==1)   and
                   fgEsteira2==1                       and
                  (fgdMotivo7017==1 or fgdMotivo7029==1) 
                 ):
                 classes[3][1]=classes[3][1]+1            
            # Seguemento 05
                # Leads.BD_diasEsteira is Novo           AND 
                # Leads.Motivo is 7016 and 7029 and 7065 AND 
                # Leads.Esteira is 2                     AND 
                # Leads.Regiao is CENTRO-OESTE and NORTE AND 
                # Leads.MigracaoInterna is S
            elif ( fgDiasEsteiraNovo==1                 and
                  (fgdMotivo7016==1 or fgdMotivo7029==1 or fgdMotivo7065==1) and
                   fgEsteira2==1                        and                                   
                  (fgRegiaoCNO==1 or fgRegiaoNO==1)    and 
                   fgMigracaoInternaSim==1 
                 ):
                 classes[4][1]=classes[4][1]+1            

            # Seguemento 06
                # Leads.BD_diasEsteira is Virgem         AND 
                # Leads.Regiao is SUDESTE and SUL        AND 
                # Leads.PassagensDiscador is 1           AND 
                # Leads.Motivo is 7037
            elif ( fgDiasEsteiraVirgem==1               and
                  (fgRegiaoSUD==1 or fgRegiaoSUL==1)    and 
                  qtdPassDiscador==1                    and
                  fgdMotivo7037==1
                 ):
                 classes[5][1]=classes[5][1]+1                                         
            # Seguemento 07
                # Leads.BD_diasEsteira is Novo           AND 
                # Leads.Motivo is 7016 and 7029 and 7065 AND 
                # Leads.Esteira is 1 and 3               AND 
                # Leads.Regiao is CENTRO-OESTE and NORTE and SUL AND 
                # Leads.MigracaoInterna is S
            elif ( fgDiasEsteiraNovo==1                 and
                  (fgdMotivo7016==1 or fgdMotivo7029==1 or fgdMotivo7065==1) and
                  (fgEsteira1==1    or  fgEsteira3==1)  and                                   
                  (fgRegiaoCNO==1 or fgRegiaoNO==1 or fgRegiaoSUL==1)    and 
                   fgMigracaoInternaSim==1 
                 ):
                 classes[6][1]=classes[6][1]+1                        
            # Seguemento 08
                # Leads.BD_diasEsteira is Virgem         AND 
                # Leads.Regiao is SUDESTE and SUL        AND 
                # Leads.PassagensDiscador is 1           AND 
                # Leads.Motivo is 7017 and 7029          AND 
                # Leads.MigracaoInterna is N
            elif ( fgDiasEsteiraVirgem==1                and
                  (fgRegiaoSUD==1 or fgRegiaoSUL==1)     and 
                  qtdPassDiscador==1                     and
                  (fgdMotivo7017==1 or fgdMotivo7029==1) and
                  fgMigracaoInternaNao==1
                 ):
                 classes[7][1]=classes[7][1]+1                         
            # Seguemento 09
                # Leads.BD_diasEsteira is Atrasado       AND 
                # Leads.Motivo is 7017 and 7037
            elif ( fgDiasEsteiraAtrasado==1               and
                 ( fgdMotivo7017==1 or fgdMotivo7037==1 )
                 ):
                 classes[8][1]=classes[8][1]+1                         
            # Seguemento 10
                # Leads.BD_diasEsteira is Virgem         AND 
                # Leads.Regiao is CENTRO-OESTE and NORTE AND 
                # Leads.Esteira is 2                     AND 
                # Leads.Motivo is 7016 and 7065
            elif ( fgDiasEsteiraVirgem==1               and
                  (fgRegiaoCNO==1 or fgRegiaoNO==1)     and 
                  fgEsteira2==1                         and
                  (fgdMotivo7016==1 or fgdMotivo7065==1 )
                 ):
                 classes[9][1]=classes[9][1]+1                         
            # Seguemento 11
                # Leads.BD_diasEsteira is Atrasado       AND 
                # Leads.Motivo is 7016 and 7029 and 7065 AND 
                # Leads.MigracaoInterna is S
            elif ( fgDiasEsteiraAtrasado==1               and
                  (fgdMotivo7016==1 or fgdMotivo7029==1 or fgdMotivo7065==1)    and 
                  fgMigracaoInternaSim==1                  
                 ):
                 classes[10][1]=classes[10][1]+1                         
            # Seguemento 12
                # Leads.BD_diasEsteira is Novo           AND 
                # Leads.Motivo is 7016 and 7029 and 7065 AND 
                # Leads.Esteira is 2                     AND 
                # Leads.Regiao is SUDESTE and SUL        AND 
                # Leads.DDDDiferentes is 0
            elif ( fgDiasEsteiraNovo==1               and
                  (fgdMotivo7016==1 or fgdMotivo7029==1 or fgdMotivo7065==1)    and 
                  fgEsteira2==1                                                 and
                  (fgRegiaoSUD==1 or fgRegiaoSUL==1)                            and
                  fgDDDDiferentes==0
                 ):
                 classes[11][1]=classes[11][1]+1                         
            # Seguemento 13
                # Leads.BD_diasEsteira is Virgem         AND 
                # Leads.Regiao is SUDESTE and SUL        AND 
                # Leads.PassagensDiscador is 1           AND 
                # Leads.Motivo is 7017 and 7029          AND 
                # Leads.MigracaoInterna is S
            elif ( fgDiasEsteiraVirgem==1                and
                  (fgRegiaoSUD==1 or fgRegiaoSUL==1)     and 
                  qtdPassDiscador==1                     and
                  (fgdMotivo7017==1 or fgdMotivo7029==1) and
                  fgMigracaoInternaSim==1                  
                 ):
                 classes[12][1]=classes[12][1]+1             
            # Seguemento 14
                # Leads.BD_diasEsteira is Virgem         AND 
                # Leads.Regiao is NORDESTE
            elif ( fgDiasEsteiraVirgem==1               and
                   fgRegiaoNDR==1                        
                 ):
                 classes[13][1]=classes[13][1]+1                         
            # Seguemento 15
                # Leads.BD_diasEsteira is Virgem         AND 
                # Leads.Regiao is CENTRO-OESTE and NORTE AND 
                # Leads.Esteira is 1 and 3
            elif ( fgDiasEsteiraVirgem==1               and
                  (fgRegiaoCNO==1 or fgRegiaoNO==1)    and 
                  (fgEsteira1==1 or fgEsteira3==1)
                 ):
                 classes[14][1]=classes[14][1]+1                         
            # Seguemento 16
                # Leads.BD_diasEsteira is Novo           AND 
                # Leads.Motivo is 7016 and 7029 and 7065 AND 
                # Leads.Esteira is 2                     AND 
                # Leads.Regiao is CENTRO-OESTE and NORTE AND 
                # Leads.MigracaoInterna is N
            elif (fgDiasEsteiraNovo==1                                       and                  
                  (fgdMotivo7016==1 or fgdMotivo7029==1 or fgdMotivo7065==1) and
                  fgEsteira2==1                                              and   
                  (fgRegiaoCNO==1 or fgRegiaoNO==1)                         and
                  fgMigracaoInternaNao==1                                                        
                 ):
                 classes[15][1]=classes[15][1]+1                         
            # Seguemento 17
                # Leads.BD_diasEsteira is Atrasado       AND 
                # Leads.Motivo is 7016 and 7029 and 7065 AND 
                # Leads.MigracaoInterna is N             AND 
                # Leads.Esteira is 2                     AND 
                # Leads.Regiao is SUDESTE
            elif ( fgDiasEsteiraAtrasado==1                                  and
                  (fgdMotivo7016==1 or fgdMotivo7029==1 or fgdMotivo7065==1) and
                   fgMigracaoInternaNao==1                                   and                                          
                   fgEsteira2==1                                             and                  
                   fgRegiaoSUD==1 
                 ):
                 classes[16][1]=classes[16][1]+1             
            # Seguemento 18
                # Leads.BD_diasEsteira is Velho          AND 
                # Leads.MigracaoInterna is S
            elif ( fgDiasEsteiraVelho==1               and
                   fgMigracaoInternaSim==1
                 ):
                 classes[17][1]=classes[17][1]+1             
            # Seguemento 19
                # Leads.BD_diasEsteira is Novo           AND 
                # Leads.Motivo is 7016 and 7029 and 7065 AND 
                # Leads.Esteira is 1 and 3               AND 
                # Leads.Regiao is CENTRO-OESTE and NORTE and SUL AND 
                # Leads.MigracaoInterna is N
            elif (fgDiasEsteiraNovo==1                                       and
                  (fgdMotivo7016==1 or fgdMotivo7029==1 or fgdMotivo7065==1) and                  
                  (fgEsteira1==1 or fgEsteira3==1)                           and
                  (fgRegiaoCNO==1 or fgRegiaoNO==1 or fgRegiaoSUL==1)        and                   
                  fgMigracaoInternaNao==1
                 ):
                 classes[18][1]=classes[18][1]+1             
            # Seguemento 20
                # Leads.BD_diasEsteira is Virgem         AND 
                # Leads.Regiao is SUDESTE and SUL        AND 
                # Leads.PassagensDiscador is 2
            elif ( fgDiasEsteiraVirgem==1               and
                  (fgRegiaoSUD==1 or fgRegiaoSUL==1)    and 
                  qtdPassDiscador==2
                 ):
                 classes[19][1]=classes[19][1]+1             
            # Seguemento 21
                # Leads.BD_diasEsteira is Novo           AND 
                # Leads.Motivo is 7016 and 7029 and 7065 AND 
                # Leads.Esteira is 2                     AND 
                # Leads.Regiao is SUDESTE and SUL        AND 
                # Leads.DDDDiferentes is 1
            elif ( fgDiasEsteiraNovo==1                 and
                  (fgdMotivo7016==1 or fgdMotivo7029==1 or fgdMotivo7065==1) and                  
                  fgEsteira2==1                         and
                  (fgRegiaoSUD==1 or fgRegiaoSUL==1)    and                   
                  fgDDDDiferentes==1
                 ):
                 classes[20][1]=classes[20][1]+1             
            # Seguemento 22
                # Leads.BD_diasEsteira is Novo           AND 
                # Leads.Motivo is 7016 and 7029 and 7065 AND 
                # Leads.Esteira is 2                     AND 
                # Leads.Regiao is NORDESTE
            elif (fgDiasEsteiraNovo==1                                              and
                 (fgdMotivo7016==1 or fgdMotivo7029==1 or fgdMotivo7065==1)         and
                  fgEsteira2==1                                                     and
                  fgRegiaoNDR==1                                              
                 ):
                 classes[21][1]=classes[21][1]+1             
            # Seguemento 23
                # Leads.BD_diasEsteira is Atrasado       AND 
                # Leads.Motivo is 7016 and 7029 and 7065 AND 
                # Leads.MigracaoInterna is N             AND 
                # Leads.Esteira is 2                     AND 
                # Leads.Regiao is SUL
            elif (fgDiasEsteiraAtrasado==1                                          and
                 (fgdMotivo7016==1 or fgdMotivo7029==1 or fgdMotivo7065==1)         and                                                      
                  fgMigracaoInternaNao==1                                           and
                  fgEsteira2 ==1                                                    and                  
                  fgRegiaoSUL==1
                 ):
                 classes[22][1]=classes[22][1]+1             
            # Seguemento 24
                # Leads.BD_diasEsteira is Atrasado       AND 
                # Leads.Motivo is 7016 and 7029 and 7065 AND 
                # Leads.MigracaoInterna is N             AND 
                # Leads.Esteira is 2                     AND 
                # Leads.Regiao is NORTE
            elif ( fgDiasEsteiraAtrasado==1               and
                 (fgdMotivo7016==1 or fgdMotivo7029==1 or fgdMotivo7065==1)         and                                                      
                  fgMigracaoInternaNao==1                                           and
                  fgEsteira2 ==1                                                    and                  
                  fgRegiaoNO==1
                 ):
                 classes[23][1]=classes[23][1]+1             
            # Seguemento 25
                # Leads.BD_diasEsteira is Virgem         AND 
                # Leads.Regiao is SUDESTE and SUL        AND 
                # Leads.PassagensDiscador is 1           AND 
                # Leads.Motivo is 7016 and 7065
            elif ( fgDiasEsteiraVirgem==1               and
                  (fgRegiaoSUD==1 or fgRegiaoSUL==1)    and 
                  qtdPassDiscador==1                    and
                  (fgdMotivo7016==1 or fgdMotivo7065==1)
                 ):
                 classes[24][1]=classes[24][1]+1             
            # Seguemento 26
                # Leads.BD_diasEsteira is Atrasado       AND 
                # Leads.Motivo is 7016 and 7029 and 7065 AND 
                # Leads.MigracaoInterna is N             AND 
                # Leads.Esteira is 2                     AND 
                # Leads.Regiao is CENTRO-OESTE
            elif ( fgDiasEsteiraAtrasado ==1               and
                  (fgdMotivo7016==1 or fgdMotivo7029==1 or fgdMotivo7065==1)         and                                                      
                  fgMigracaoInternaNao==1                  and
                  fgEsteira2==1                            and 
                  fgRegiaoCNO==1                           
                 ):
                 classes[25][1]=classes[25][1]+1             
            # Seguemento 27
                # Leads.BD_diasEsteira is Atrasado       AND 
                # Leads.Motivo is 7016 and 7029 and 7065 AND 
                # Leads.MigracaoInterna is N             AND 
                # Leads.Esteira is 2                     AND 
                # Leads.Regiao is NORDESTE
            elif ( fgDiasEsteiraAtrasado==1               and
                  (fgdMotivo7016==1 or fgdMotivo7029==1 or fgdMotivo7065==1)         and                                                                  
                   fgMigracaoInternaNao==1                  and
                   fgEsteira2==1                            and                                      
                   fgRegiaoNDR==1                  
                 ):
                 classes[26][1]=classes[26][1]+1             
            # Seguemento 28
                # Leads.BD_diasEsteira is Novo           AND 
                # Leads.Motivo is 7016 and 7029 and 7065 AND 
                # Leads.Esteira is 1 and 3               AND 
                # Leads.Regiao is NORDESTE and SUDESTE
            elif ( fgDiasEsteiraNovo==1                 and
                  (fgdMotivo7016==1 or fgdMotivo7029==1 or fgdMotivo7065==1)         and                                                                              
                  (fgEsteira1==1 or fgEsteira3==1)      and
                  (fgRegiaoNDR==1 or fgRegiaoSUD==1) 
                 ):
                 classes[27][1]=classes[27][1]+1             
            # Seguemento 29
                # Leads.BD_diasEsteira is Velho          AND 
                # Leads.MigracaoInterna is N
            elif ( fgDiasEsteiraVelho==1               and
                   fgMigracaoInternaNao==1
                 ):
                 classes[28][1]=classes[28][1]+1                         
            # Seguemento 30
                # Leads.BD_diasEsteira is Atrasado       AND 
                # Leads.Motivo is 7016 and 7029 and 7065 AND 
                # Leads.MigracaoInterna is N             AND 
                # Leads.Esteira is 1 and 3
            elif ( fgDiasEsteiraAtrasado==1               and
                  (fgdMotivo7016==1 or fgdMotivo7029==1 or fgdMotivo7065==1)         and                                                                                                
                  fgMigracaoInternaNao==1                 and
                  (fgEsteira1==1 or fgEsteira3==1) 
                 ):
                 classes[29][1]=classes[29][1]+1             
            else:
                semclasse=semclasse+1
                nao_class.append([], ignore_index=True)   
                nao_class.at[semclasse-1,'cod_cpf_cgc']=dados.at[index,'cod_cpf_cgc']

            totreg=totreg+1
        plog.log_escreve('-----------------------------------------------------------------------------------')
        plog.log_escreve('Montagem do Modelo Agendamento\Alo')
        classificados=0
        notamedia=0        
        taxamedia=0
        for i in range(0,len(classes)):            
            contagem='00'+str(i)
            contagem=contagem[-2:]
            plog.log_escreve('Registros {}, Seguemento {} Qtd {}'.format(totreg,contagem,classes[i][1]))
            classificados=classificados+classes[i][1]        
            notamedia=notamedia+classes[i][1]*classes[i][0]
            taxamedia=taxamedia+classes[i][1]*classes[i][2]
        plog.log_escreve('-----------------------------------------------------------------------------------')            
        plog.log_escreve('Nota Média do Mailing {}'.format(notamedia/classificados))
        plog.log_escreve('Score do Mailing      {}'.format(taxamedia/classificados))        
        plog.log_escreve('-----------------------------------------------------------------------------------')            
        plog.log_escreve('Registros {}, Sem Classe {} Classificados {}'.format(totreg,semclasse,classificados))
        for index, linhas in nao_class.iterrows():  
            plog.log_escreve(' CPF {}'.format(nao_class.at[index,'cod_cpf_cgc']))            

        plog.log_escreve('-----------------------------------------------------------------------------------')
        plog.log_escreve('Dias de Esteira de Contrato Virgem  {:0.0%}, Novo  {:0.0%}, Atrasado  {:0.0%}, Velho {:0.0%}, {:0.0%}'.format(qtDiasEsteiraVirgem/totreg,
                                                                                                                     qtDiasEsteiraNovo/totreg,
                                                                                                                     qtDiasEsteiraAtrasado/totreg,
                                                                                                                     qtDiasEsteiraVelho/totreg,
                                                                                                                     (qtDiasEsteiraVirgem+
                                                                                                                     qtDiasEsteiraNovo+
                                                                                                                     qtDiasEsteiraAtrasado+
                                                                                                                     qtDiasEsteiraVelho)/totreg  
                                                                                                                     ))
        plog.log_escreve('Dias na Esteira  1  {:0.0%}, 2  {:0.0%}, 3  {:0.0%}, {:0.0%}'.format(qtEsteira1/totreg,
                                                                                               qtEsteira2/totreg,
                                                                                               qtEsteira3/totreg,                                                                                               
                                                                                               (qtEsteira1+
                                                                                                qtEsteira2+
                                                                                                qtEsteira3)/totreg
                                                                                               ))
        plog.log_escreve('Região SD {:0.0%}, SU {:0.0%}, ND {:0.0%}, NO {:0.0%}, CO {:0.0%}, {:0.0%}'.format(regiaoSUD/totreg,
                                                                                                             regiaoSUL/totreg,
                                                                                                             regiaoNDR/totreg,
                                                                                                             regiaoNO/totreg,
                                                                                                             regiaoCNO/totreg,
                                                                                                            (regiaoSUD+
                                                                                                             regiaoSUL+
                                                                                                             regiaoNDR+
                                                                                                             regiaoNO+
                                                                                                             regiaoCNO)/totreg
                                                                                                    )
                                                                                                    )
        plog.log_escreve('qtdtel1 {:0.0%}, qtdtel2 {:0.0%}, qtdtel3 {:0.0%}, qtdtel4 {:0.0%},{:0.0%} '.format(qtdtel1/totreg,
                                                                                                    qtdtel2/totreg,
                                                                                                    qtdtel3/totreg,
                                                                                                    qtdtel4/totreg,
                                                                                                    (qtdtel1+
                                                                                                     qtdtel2+
                                                                                                     qtdtel3+
                                                                                                     qtdtel4)/totreg
                                                                                                    )                                                                                                    
                                                                                                    )                                                                                                    
        plog.log_escreve('Motivos 7029 {:0.0%}, 7016 {:0.0%}, 7065 {:0.0%}, 7037 {:0.0%}, 7017 {:0.0%}, {:0.0%}'.format(qtdMotivo7029/totreg,
                                                                                                                        qtdMotivo7016/totreg,
                                                                                                                        qtdMotivo7065/totreg,
                                                                                                                        qtdMotivo7037/totreg,
                                                                                                                        qtdMotivo7017/totreg,
                                                                                                                        (qtdMotivo7029+
                                                                                                                         qtdMotivo7016+
                                                                                                                         qtdMotivo7065+
                                                                                                                         qtdMotivo7037+
                                                                                                                         qtdMotivo7017
                                                                                                                         )/totreg
                                                                                                    )
                                                                                                    )
        
except (Exception, psycopg2.DatabaseError) as error:
    plog.log_escreve(error)