# Info
# p11Discador - Grava Dados para Discador Ingenium e Alimenta Banco para BI
# Data: 30/09/2021
# Autor: Dorival
# Versão: 5.0

import sFuncoes
import os
dir=os.path.dirname(__file__)
from datetime import datetime
import pandas as pd
import csv
import xlrd 
import json, requests
import psycopg2
import unidecode

#=================================================
# Variáveis
#=================================================
origemMailing       = 'C:\\Orquestrador_Dorival\\50Leads\\20210929_1_PAV.xlsx'
arquivoFinal        = os.path.join(dir,'50Leads\\')+sFuncoes.hoje_anomedia()
localFinal          = os.path.join(dir,'50Leads\\')
controleCarga       = False
nordeste			= "CE,RN,PB,PE"
#=================================================
# Leitura do Arquivo de parametros
#=================================================
parametros          = sFuncoes.leituraJson(os.path.join(dir,'00Entrada\\conexaoMaquinaVendas.json'),False)

#=================================================
# Abertura do Log
#=================================================
plog           = sFuncoes.Log(os.path.join(dir,parametros['enderecoLog']))
if (parametros['zeralog']):
    plog.log_arq_novo()
plog.log_escreve('Inicio Le Retorno Extracao Leads, Carga nos Bancos')
plog.log_escreve('')


#=================================================
# Cria Conexao Banco de Dados
#=================================================
try:
    conexaoIngenium         = psycopg2.connect(host='18.229.4.57', database='data_ims',
                                         user='a5solutions', password='@5s0lut10ns')
    #pass
except Exception as f:
    r=f.args[0]
    plog.log_escreve('Ingenium Conexão - Erro: '+r)    

try:
    conexaoMaquinaVendas    = psycopg2.connect(host='localhost', database='MaquinaVendas',
                                        user='postgres', password='postgres')
except Exception as f:
    r=f.args[0]
    plog.log_escreve('MaquinaVendas Conexão - Erro: '+r)    

#=======================================================================================================================================================================
# Limpa Tabela Leads
#=======================================================================================================================================================================
plog.log_escreve('Limpando Tabela de Leads do dia Anterior')
cur = conexaoIngenium.cursor()
sql = "update markings.customer_markings set date_confirmation=current_date-1 where date_confirmation is null"
cur.execute(sql)
conexaoIngenium.commit()
cur.close()
plog.log_escreve('Fim da Limpeza')

#=======================================================================================================================================================================
# Leitura Retorno Extracao Leads, Arruma os dados de endereço, grava nos bancos e gera outros arquivos locais no disco
#=======================================================================================================================================================================

plog.log_escreve('Acionando EndPoint ')
#Aciona API e Ler do json
try:
    #Busca EndPoints Habilitados para consulta
    cur = conexaoMaquinaVendas.cursor()
    sql = "select porta from endpoint where habilitado=true"        
    cur.execute(sql)
    rowE = cur.fetchall()
    #while rowE is not None: #tuple
    for index, tuple in enumerate(rowE):
        endp = tuple[0]
        arquivo_mailing     = 'C:\\00Producao\\08Tahto\\Orquestrador\\90Report\\mailing_'+sFuncoes.hoje_anomedia()+'.csv'
        dados               = json.dumps({"agente": "agente_001","arquivo": "","system": "siebel6"})
        headers             = {'Content-Type': 'application/json'}
        endpoint            = "http://3.229.194.219:"+''.join(tuple[0])+"/api/agendamento/retorno_extracao_leads"
        response            = requests.request("POST", endpoint, headers=headers, data=dados)
        resultado           = json.loads(response.content)['content']
        erro                = resultado['result']
        if erro=='OK': #OK = Nao tem Erro
            if resultado['result']=='OK':
                automacao       = resultado['automacao']
                leads_mailing   = resultado['leads_mailing']
                controleCarga   = True
                if resultado['result']=='OK' and controleCarga==True:
                    leads_mailing   = resultado['leads_mailing']
                    ma              = pd.json_normalize(resultado['leads_mailing'])
                    ma.to_csv(arquivo_mailing,index=None,sep=';')
                    plog.log_escreve('Arquivo de Mailing gerado em disco: '+arquivo_mailing)
                    
                    cur = conexaoIngenium.cursor()
                    plog.log_escreve('Iniciando gravacao Tabela Leads Ingenium: ')
                    try:
                        i=0
                        j=0
                        z=0
                        nordeste			= "CE,RN,PB,PE"
                        for automa in automacao: 
                            if automa['statusEnriquecimento']=='OK':
                                leads_mailing   = resultado['leads_mailing']
                                for leads in leads_mailing: #Percorre Mailing para pegar o campo telefones e o X9
                                    if str(automa['cpf']).replace('.0','')==str(leads['COD_CPF_CGC']).replace('.0',''):# and (leads['UF']!='CE' and leads['UF']!='RN' and leads['UF']!='PB' and leads['UF']!='PE'):
                                        today=datetime.now()
                                        i=i+1
                                        endereco = sFuncoes.corrige_endereço(leads['CAMPOX21'])
                                        qtd=len(endereco)
                                        for x in range(qtd):
                                            palavra=endereco[x:x+8]
                                            try:
                                                if type(int(palavra)) is int:
                                                    if int(palavra)>10000000:
                                                        cep=endereco[x:x+8]
                                                        cep_ajustado = 'CEP '+cep[:3]+','+cep[:5][3:]+','+cep[5:]
                                                        endereco = endereco.replace(cep,cep_ajustado)
                                            except:
                                                pass
                                        sem_estado=endereco[:len(endereco)-2]
                                        uf=sFuncoes._estado_descricao_(endereco[len(endereco)-2:])
                                        endereco=sem_estado+' '+uf                           
                                        sql = "insert into a5solutions.leads  (name_client, campaignid, phone_1, phone_2, dateinsert, cpf, address,reason) values ( %s, %s, %s, %s, %s, %s, %s, %s)"        
                                        cur.execute(sql, [leads['NOME_CLI'].replace(' ','%20'),'1',str(leads['FONE_1']).replace('.0',''), str(leads['FONE_2']).replace('.0',''), '{:%Y-%m-%d %H:%M:%S}'.format(today), leads['COD_CPF_CGC'], endereco.replace(' ','%20').replace('|',','),leads['CAMPOX9']]) #-- Alteração realizada para inserir o contato do pessoal da Oi -- 28.02.2022                      
                                        break
                            else:
                                z=z+1        
                        plog.log_escreve('Fim - Gravando Banco em Ingenium Leads: '+str(i)+' Registros OK e NOK '+str(z))
                        conexaoIngenium.commit()
                    except Exception as e:
                        resultado=e.args[0]
                        plog.log_escreve('Ingenium Leads - Err: '+resultado)    
                        controleCarga=False

                    ##Grava Mailing
                    try:
                        cur = conexaoMaquinaVendas.cursor()
                        z=0
                        cont_mail=0
                        for [COD_CAMPANHA,COD_TIPO_DOCUMENTO,COD_CPF_CGC,NU_CONTRATO,NRC_CLI,COD_FILIAL,COD_CLI,COD_RESIDENCIA_CLI,NOME_CLI,FONE_1,FONE_2,FONE_3,FONE_4,
                             EMAIL_CLI,TIPO_LOGRADOURO,LOGRADOURO,NUMERO,COMPLEMENTO,BAIRRO,CIDADE,COD_LOCALIDADE,CEP,UF,CANAL_VENDA,PDV_BOV,COD_SEGMENTO,DESC_SEGMENTO,
                             COD_EMPRESA_CONTATO,CAMPOX1,CAMPOX2,CAMPOX3,CAMPOX4,CAMPOX5,CAMPOX6,CAMPOX7,CAMPOX8,CAMPOX9,CAMPOX10,CAMPOX11,CAMPOX12,CAMPOX13,CAMPOX14,
                             CAMPOX15,CAMPOX16,CAMPOX17,CAMPOX18,CAMPOX19,CAMPOX20,CAMPOX21] in leads_mailing: 
                            sql = "insert into mailing (COD_CAMPANHA,COD_TIPO_DOCUMENTO,COD_CPF_CGC,NU_CONTRATO,NRC_CLI,COD_FILIAL,COD_CLI,COD_RESIDENCIA_CLI,NOME_CLI,FONE_1,FONE_2,FONE_3,FONE_4,EMAIL_CLI,TIPO_LOGRADOURO,LOGRADOURO,NUMERO,COMPLEMENTO,BAIRRO,CIDADE,COD_LOCALIDADE,CEP,UF,CANAL_VENDA,PDV_BOV,COD_SEGMENTO,DESC_SEGMENTO,COD_EMPRESA_CONTATO,CAMPOX1,CAMPOX2,CAMPOX3,CAMPOX4,CAMPOX5,CAMPOX6,CAMPOX7,CAMPOX8,CAMPOX9,CAMPOX10,CAMPOX11,CAMPOX12,CAMPOX13,CAMPOX14,CAMPOX15,CAMPOX16,CAMPOX17,CAMPOX18,CAMPOX19,CAMPOX20,CAMPOX21,DTINSERCAO) values (%s, %s, %s, %s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"        
                            cur.execute(sql, [leads_mailing[cont_mail]['COD_CAMPANHA'],leads_mailing[cont_mail]['COD_TIPO_DOCUMENTO'],leads_mailing[cont_mail]['COD_CPF_CGC'],leads_mailing[cont_mail]['NU_CONTRATO'],leads_mailing[cont_mail]['NRC_CLI'],leads_mailing[cont_mail]['COD_FILIAL'],leads_mailing[cont_mail]['COD_CLI'],leads_mailing[cont_mail]['COD_RESIDENCIA_CLI'],leads_mailing[cont_mail]['NOME_CLI'],leads_mailing[cont_mail]['FONE_1'],leads_mailing[cont_mail]['FONE_2'],leads_mailing[cont_mail]['FONE_3'],leads_mailing[cont_mail]['FONE_4'],leads_mailing[cont_mail]['EMAIL_CLI'],leads_mailing[cont_mail]['TIPO_LOGRADOURO'],leads_mailing[cont_mail]['LOGRADOURO'],leads_mailing[cont_mail]['NUMERO'],leads_mailing[cont_mail]['COMPLEMENTO'],leads_mailing[cont_mail]['BAIRRO'],leads_mailing[cont_mail]['CIDADE'],leads_mailing[cont_mail]['COD_LOCALIDADE'],leads_mailing[cont_mail]['CEP'],leads_mailing[cont_mail]['UF'],leads_mailing[cont_mail]['CANAL_VENDA'],leads_mailing[cont_mail]['PDV_BOV'],leads_mailing[cont_mail]['COD_SEGMENTO'],leads_mailing[cont_mail]['DESC_SEGMENTO'],leads_mailing[cont_mail]['COD_EMPRESA_CONTATO'],leads_mailing[cont_mail]['CAMPOX1'],leads_mailing[cont_mail]['CAMPOX2'],leads_mailing[cont_mail]['CAMPOX3'],leads_mailing[cont_mail]['CAMPOX4'],leads_mailing[cont_mail]['CAMPOX5'],leads_mailing[cont_mail]['CAMPOX6'],leads_mailing[cont_mail]['CAMPOX7'],leads_mailing[cont_mail]['CAMPOX8'],leads_mailing[cont_mail]['CAMPOX9'],leads_mailing[cont_mail]['CAMPOX10'],leads_mailing[cont_mail]['CAMPOX11'],leads_mailing[cont_mail]['CAMPOX12'],leads_mailing[cont_mail]['CAMPOX13'],leads_mailing[cont_mail]['CAMPOX14'],leads_mailing[cont_mail]['CAMPOX15'],leads_mailing[cont_mail]['CAMPOX16'],leads_mailing[cont_mail]['CAMPOX17'],leads_mailing[cont_mail]['CAMPOX18'],leads_mailing[cont_mail]['CAMPOX19'],leads_mailing[cont_mail]['CAMPOX20'],leads_mailing[cont_mail]['CAMPOX21'],str('{:%Y%m%d}'.format(today))])
                            z=z+1
                            cont_mail=cont_mail+1
                        plog.log_escreve('Fim - Gravando Banco em MaquinaVendas Mailing: '+str(z))
                        conexaoMaquinaVendas.commit()
                    except Exception as e:
                        resultado=e.args[0]
                        plog.log_escreve('MaquinaVendas Mailign - Err: '+resultado)    
                        controleCarga=False
                else:
                    plog.log_escreve('Erro de acesso ao EndPoint')    
                plog.log_escreve('Processo Encerrado - EndPoint '+endp)    
        #rowE = cur.fetchone()
    cur.close()
except Exception as e:
    resultado=e.args[0]
    plog.log_escreve('Erro - Err: ' .format(resultado))    
    plog.log_escreve('Erro : '      .format(erro))    
    plog.log_escreve('Erro : '      .format(resultado['status']))
    controleCarga=False

