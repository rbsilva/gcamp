#====================================================================================================
# Bibliotecas
import pandas as pd
import numpy as np
import json
import openpyxl
import psycopg2
from datetime import date
import sFuncoes as f
import pFuncoes as p
import os
#===================================================================================================== 
# Funções globais
def json_reader(file):
    with open(file, 'r', encoding='utf8') as f:
        return json.load(f)

#-----------------------------------------------------------------------------------------------------
def diamesano():
    data_atual = date.today()
    data= data_atual.strftime('%d%m%Y')
    return data

#===================================================================================================== 
dirjson  = 'C:\\Clientes\\Ativos\\127_TathtoTelevendas\\GCamp\\02Processo\\00Entrada'
dirlog   = 'C:\\Clientes\\Ativos\\127_TathtoTelevendas\\GCamp\\02Processo\\80Logs'
#===================================================================================================== 
# Dados (Entrada)
#=====================================================================================================     
localjson   = os.path.join(dirjson,'conexaoGCamp.json')
parametros  = f.leituraJson(localjson,False) 
    
#===================================================================================================== 
# Banco de dados
#===================================================================================================== 
host_bd      = parametros['host']
database_bd  = parametros['database']
user_bd      = parametros['user']
password_bd  = parametros['password']
port_bd      = parametros['porta']
enderco_log  = parametros['enderecoLogCria']

#===================================================================================================== 
# Funções para o programa
#-----------------------------------------------------------------------------------------------------
def excel_reader(file):
    df = pd.read_excel(file)
    return df

def conecta_bd():
  con = psycopg2.connect(host=host_bd , 
                         database=database_bd,
                         user=user_bd, 
                         password=password_bd,
                         port=port_bd )
  return con

def inserir_bd(sql):
    con = conecta_bd()
    cur = con.cursor()
    try:
        cur.execute(sql)
        con.commit()
        cur.close()
        return 0
    except (Exception, psycopg2.DatabaseError) as error:
        print("Error: %s" % error)
        con.rollback()
        cur.close()
        return error

def truncate(table):
    con = conecta_bd()
    cur = con.cursor()
    try:
        cur.execute("TRUNCATE TABLE %s" % table)
        con.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print("Error: %s" % error)
        con.rollback()
        cur.close()
        return 1
    cur.close()

def consulta_bd(sql):
    con = conecta_bd()
    cur = con.cursor()
    cur.execute(sql)
    recset = cur.fetchall()
    registros = []
    for rec in recset:
        registros.append(rec)
    con.close()
    return registros