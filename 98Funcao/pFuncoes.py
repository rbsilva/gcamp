#====================================================================================================
# Funções Espacifícias para os Processos Z
#===================================================================================================== 
import pandas as pd
import numpy as np
import sys
sys.path.insert(1, './98Funcao')
import sFuncoes as f
import json
import psycopg2
from datetime import date
import os

from sklearn.model_selection import ShuffleSplit
#====================================================================================================
# Funções de Operação no Banco de Dados
#===================================================================================================== 

def conecta_bd(host_bd,database_bd,user_bd,password_bd):
  try:
    con = psycopg2.connect(host=host_bd , 
                           database=database_bd,
                           user=user_bd, 
                           password=password_bd)
    res="Ok"         
  except Exception as f:
    con=f.args[0]
    res="Erro"
  return con,res

def inserir_bd(sql,host_bd,database_bd,user_bd,password_bd):
    try:
        con,res = conecta_bd(host_bd,database_bd,user_bd,password_bd)
        if res=='Erro':                                
            return res        
        cur = con.cursor()
        cur.execute(sql)
        con.commit()
        res="Ok"        
    except (Exception, psycopg2.DatabaseError) as error:
        print("Error: %s" % error)
        cur.close()
        res=error.pgcode
        return res
    cur.close()
    return res

def truncate(table,host_bd,database_bd,user_bd,password_bd):
    try:
        con,res = conecta_bd(host_bd,database_bd,user_bd,password_bd)
        if res=='Erro':                                
            return res
        cur = con.cursor()        
        cur.execute("TRUNCATE TABLE %s" % table)
        con.commit()
        res="Ok"
    except (Exception, psycopg2.DatabaseError) as error:
        cur.close()        
        res=error.pgcode
    cur.close()
    return res

def apagar(table,host_bd,database_bd,user_bd,password_bd):
    try:
        con,res = conecta_bd(host_bd,database_bd,user_bd,password_bd)
        if res=='Erro':                                
            return res
        cur = con.cursor()        
        cur.execute("DROP TABLE %s" % table)
        con.commit()
        res="Ok"
    except (Exception, psycopg2.DatabaseError) as error:
        cur.close()        
        res=error.pgcode
    cur.close()
    return res

def consulta_bd(sql,host_bd,database_bd,user_bd,password_bd):
    try:
        con,res = conecta_bd(host_bd,database_bd,user_bd,password_bd)
        if res=='Ok':
            cur = con.cursor()
            cur.execute(sql)
            recset = cur.fetchall()
            registros = []
            for rec in recset:
                registros.append(rec)
            con.close()
        else:
            res='Erro Conexão'
    except (Exception, psycopg2.DatabaseError) as error:
        registros=0
        res=error.pgcode
        con.close()
    return registros,res

def carga_texto(conn, df, table,limpa):
    tuples = [tuple(x) for x in df.to_numpy()]
    cols = ','.join(list(df.columns))
    query  = "INSERT INTO %s(%s) VALUES %%s" % (table, cols)
    if limpa:
        cursor = conn.cursor()        
        cursor.execute("TRUNCATE TABLE %s" % table)
        conn.commit()
        cursor.close()
    cursor = conn.cursor()
    try:
        psycopg2.extras.execute_values(cursor, query, tuples)
        conn.commit()
        res='Ok'
    except (Exception, psycopg2.DatabaseError) as error:
        print("Error: %s" % error)
        cursor.close()    
        res=error
    cursor.close()
    return res

def MontaDirAbaixo(DirEntrada,DirAbaixo):
    posicao=0
    achou=0
    for i in range(len(DirEntrada)-1,0,-1):
        if DirEntrada[i]=='\\' and achou==0:
            posicao=i
            achou=1        
    dirraiz =DirEntrada[0:posicao]
    dirsaida=os.path.join(dirraiz,DirAbaixo)
    return dirsaida    

def TrataEnd(Endereco):
    if len(Endereco)==0:
        res='Erro01'
        resultado=''
    else:
        res='Ok'
        resultado=Endereco
    return resultado,res

def TelBanidos(telefone):
    res='Nao achou'
    telefones=['3232323232',
               '5151515151'
               ]
    if telefone in telefones:
        res='Ok'
    return res

def TrataTelefones(telefones):
    res='Erro'
    fgtelfixosel=-1        
    for seqtel in range(0,4):
        freqtel =[0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0]                
        if telefones[seqtel]!='NaN' and telefones[seqtel]!='':
            #---------------------------------------
            # Quantidade de Digitos < 10 | DDD<=10
            #---------------------------------------
            if len(telefones[seqtel])<10 or int(telefones[seqtel][2:])<=10:
                telefone[seqtel]='NaN'
                res='Ok'
                
            #---------------------------------------
            # Avalia Numero Repetido
            #---------------------------------------
            for i in range(0,len(telefones[seqtel])):
                digito=int(telefones[seqtel][i])
                freqtel[digito]=freqtel[digito]+1                                                        
            for i in range(0,9):                    
                if freqtel[i]>7:
                    qtdteldig=qtdteldig+1
                    telefone='NaN'
                    res='Ok'
            #---------------------------------------                    
            # Telefones banidos
            #---------------------------------------
            res=TelBanidos(telefones[seqtel])
            if res=='Ok':                
                telefones[seqtel]='NaN'    
                res='Ok'            
            
            # Identifica Telfone Fixo
            if telefones[seqtel]!='NaN':
                if int(telefones[seqtel][2:3])<9:
                    fgtelfixosel=seqtel
                    res='Ok'                       
            if fgtelfixosel>=0:         
                if fgtelfixosel==0:
                    telefone=telefones[0]
                    telefones[0]=telefones[1]
                    telefones[1]=telefone
                    res='Ok'
                elif fgtelfixosel==1:
                    telefone=telefones[1]
                    telefones[1]=telefones[2]
                    telefones[2]=telefone
                    res='Ok'
                elif fgtelfixosel==2:
                    telefone=telefones[2]
                    telefones[2]=telefones[3]
                    telefones[3]=telefone
                    res='Ok'

    return telefones,res

def MovimentaTel(telefones):
    res='Ok'
    est_correta=False
    while est_correta==False:
        for i in range(0,4):
            for j in range(i+1,4):
                if telefones[i]=='NaN':
                    if telefones[j]!='NaN':
                        telefones[i]=telefones[j]
                        telefones[j]='NaN'
        est_correta=True
        for i in range(1,4):
            if telefones[i]!='NaN' and telefones[i-1]=='NaN':
                est_correta=False                            

    return telefones, res

def TrataEndereco(endereco,listaabrev):
    resultado,res=f.TrataEnd_CaractEspeciais(endereco)
    if res=='Ok':
        if endereco != resultado:                
            endereco = resultado                    
    # TrataEndereço Decupando    
    estrutura    =f.TrataEnd_Decupa(endereco)                
    # Tratando Tipo Endereco            
    resultado,res=f.TrataEnd_LocalizaAbrev(listaabrev,estrutura['tipo'])
    if res=='Ok':
        estrutura['tipo']=resultado.upper()
    
    # Tratando Logradouro
    estrutura['logr']=estrutura['logr']+','

    # Tratando Numero
    if estrutura['num']!='0' and estrutura['num']!='':
        estrutura['num']=estrutura['num']+','
    else:
        estrutura['num']=''
    # Tratando Complemento
    complemento = estrutura['compl'].split(' ')  
    qtdpalcompl = 0            
    for i in range(0,len(complemento)):
        qtdpalcompl=qtdpalcompl+1
        if complemento[i].isdigit()==False and len(complemento[i])<5:
            resultado,res=f.TrataEnd_LocalizaAbrev(listaabrev,complemento[i])
            if i>0 and complemento[i-1]=='CASA':
                complemento[i]=complemento[i]
            else:
                complemento[i]=resultado.upper()
    if estrutura['compl']!='':
        estrutura['compl'] =' '.join(complemento)+','

    # BAIRRO
    estrutura['bairro']='BAIRRO '+estrutura['bairro']+','

    # CEP
    estrutura['cep']='CEP '+estrutura['cep'][:3]+','+estrutura['cep'][:5][3:]+','+estrutura['cep'][5:]+','

    # Tratamento Cidade
    estrutura['cidade']='CIDADE '+estrutura['cidade']+','
    
    # Tratamento Estado
    resultado,res=f.TrataEnd_Estado(estrutura['estado'])
    if res=='Ok':
        estrutura['estado']='ESTADO '+resultado.upper()                
        
    return estrutura, res

def carga_pandas(filename,separador):
    try:
        index=0
        for line in open(filename):    
            if index==0:
                colunas=line.split(separador)        
            index=index+1
        dados=pd.DataFrame([],columns=colunas)    
        index=0
        for linha in open(filename):
            Mlinha=linha.split(separador)
            if index>0:
                dados.append([], ignore_index=True)                
                for col in range(0,len(colunas)):
                    dados.at[index,(colunas[col])]=Mlinha[col]
            index=index+1
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)  
    return dados

def cadastro_sqls(tipo):
    if   tipo=='CampLista_StatusAtiva_IntervaloValido':
        sql_db='''                  
                select 
                    D.cdCampanha,
                    D.dsCampanha,
                    D.dsTipoAcesso,
                    D.dsDiscoPath,
                    D.dsDiscoFile,
                    D.nmBaseHost,
                    D.nmBaseDatabase,
                    D.nmBaseDTabela,
                    D.nmBaseDPorta,
                    D.nmBaseDLogin,
                    D.nmBaseDSenha,
                    D.dsarqseparador
                from 
                (
	                select 
               		    A.cdCampanha,
                    	A.dsCampanha,
                    	C.dsTipoAcesso,
                    	A.dsDiscoPath,
                    	A.dsDiscoFile,
                    	A.nmBaseHost,
                	    A.nmBaseDatabase,
                       	A.nmBaseDTabela,
                    	A.nmBaseDPorta,
                    	A.nmBaseDLogin,
                    	A.nmBaseDSenha,				
   		                to_date(A.dtInicio,'DD\MM\YYY') as dtini,
       		            to_date(A.dtFim,'DD\MM\YYY')    as dtfim,
   	    	            A.hrInicio,
   		                A.hrFim,
                        A.dsarqseparador,
	                    CURRENT_DATE,
		                CURRENT_TIME
	                from campanha A
                    left join status B
                    on A.cdStatus=B.cdStatus
                    left join tipoacesso C
                    on C.cdtipoacesso = A.cdtipoacesso
                    where B.dsStatus='Ativa'				
                ) D
                    where (CURRENT_DATE>=D.dtini and 
	                CURRENT_DATE       <=D.dtFIM);	               
                '''
    elif tipo=='CampCont_StatusAtiva_IntervaloValido':    
        sql_db= '''
                select count(C.*) as contagem from 
                (
	            select 
   		            to_date(dtInicio,'DD\MM\YYY') as dtini,
   		            to_date(dtFim,'DD\MM\YYY')    as dtfim,
   		            hrInicio,
   		            hrFim,
	                CURRENT_DATE,
		            CURRENT_TIME
	            from campanha A
                left join status B
                on A.cdStatus=B.cdStatus
                where B.dsStatus='Ativa'
                ) C
                where (CURRENT_DATE>=C.dtini and 
	            CURRENT_DATE<=C.dtFIM);	                     
                '''
    elif tipo=='ContCont_StatusAtiva_IntervaloValido':
        sql_db='''select count(*) as contagem from controle'''    

    elif tipo=='ContList_StatusAtiva_IntervaloValido':
        sql_db='''select * from controle'''  

    elif tipo=='CelulaCont_StatusAtiva_IntervaloValido':  
        sql_db='''
                        select count(C.*) as contagem from 
                (
	            select 
   		            to_date(dtInicio,'DD\MM\YYY') as dtini,
   		            to_date(dtFim,'DD\MM\YYY')    as dtfim,
   		            hrInicio,
   		            hrFim,
	                CURRENT_DATE,
		            CURRENT_TIME
	            from celula A
                left join status B
                on A.cdStatus=B.cdStatus
                where B.dsStatus='Ativa'
                ) C
                where (CURRENT_DATE>=C.dtini and 
	            CURRENT_DATE<=C.dtFIM);	           
            '''  

    elif tipo=='CelulaCont_StatusAtiva_IntervaloValido':  
        sql_db='''
                        select count(C.*) as contagem from 
                (
	            select 
   		            to_date(dtInicio,'DD\MM\YYY') as dtini,
   		            to_date(dtFim,'DD\MM\YYY')    as dtfim,
   		            hrInicio,
   		            hrFim,
	                CURRENT_DATE,
		            CURRENT_TIME
	            from celula A
                left join status B
                on A.cdStatus=B.cdStatus
                where B.dsStatus='Ativa'
                ) C
                where (CURRENT_DATE>=C.dtini and 
	            CURRENT_DATE<=C.dtFIM);	           
            '''  
    elif tipo=='CelulaLista_StatusAtiva_IntervaloValido':  
        sql_db='''
                        select C.* as contagem from 
                (
	            select 
   		            cdcampanha,
                    cdcelula,
                    dscelula,                    
                    to_date(dtInicio,'DD\MM\YYY') as dtini,
   		            to_date(dtFim,'DD\MM\YYY')    as dtfim,
   		            hrInicio,
   		            hrFim,
	                CURRENT_DATE,
		            CURRENT_TIME
	            from celula A
                left join status B
                on A.cdStatus=B.cdStatus
                where B.dsStatus='Ativa'
                ) C
                where (CURRENT_DATE>=C.dtini and 
	            CURRENT_DATE<=C.dtFIM);	           
            '''  
    elif tipo=='Porta_Geracao_Dados':  
        sql_db='''
            select porta from endpoint where habilitado=true;	           
            '''              
    else:
        sql_db='Erro'
    return sql_db