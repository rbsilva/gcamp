#====================================================================================================
# Funções Espacifícias para os Processos Z
#===================================================================================================== 
from matplotlib.font_manager import json_dump, json_load
import pandas as pd
import numpy as np
import json
import psycopg2
from datetime import date
import os
from pandas import json_normalize

from sklearn.model_selection import ShuffleSplit
#====================================================================================================
# Funções de Operação no Banco de Dados
#===================================================================================================== 

def conecta_bd(host_bd,database_bd,user_bd,password_bd):
  try:
    con = psycopg2.connect(host=host_bd , 
                           database=database_bd,
                           user=user_bd, 
                           password=password_bd)
    res="Ok"         
  except Exception as f:
    con=f.args[0]
    res="Erro"
  return con,res

def inserir_bd(sql,host_bd,database_bd,user_bd,password_bd):
    try:
        con,res = conecta_bd(host_bd,database_bd,user_bd,password_bd)
        if res=='Erro':                                
            return res        
        cur = con.cursor()
        cur.execute(sql)
        con.commit()
        res="Ok"        
    except (Exception, psycopg2.DatabaseError) as error:
        print("Error: %s" % error)
        cur.close()
        res=error.pgcode
        return res
    cur.close()
    return res

def truncate(table,host_bd,database_bd,user_bd,password_bd):
    try:
        con,res = conecta_bd(host_bd,database_bd,user_bd,password_bd)
        if res=='Erro':                                
            return res
        cur = con.cursor()        
        cur.execute("TRUNCATE TABLE %s" % table)
        con.commit()
        res="Ok"
    except (Exception, psycopg2.DatabaseError) as error:
        cur.close()        
        res=error.pgcode
    cur.close()
    return res

def apagar(table,host_bd,database_bd,user_bd,password_bd):
    try:
        con,res = conecta_bd(host_bd,database_bd,user_bd,password_bd)
        if res=='Erro':                                
            return res
        cur = con.cursor()        
        cur.execute("DROP TABLE %s" % table)
        con.commit()
        res="Ok"
    except (Exception, psycopg2.DatabaseError) as error:
        cur.close()        
        res=error.pgcode
    cur.close()
    return res

def consulta_bd(sql,host_bd,database_bd,user_bd,password_bd):
    try:
        con,res = conecta_bd(host_bd,database_bd,user_bd,password_bd)
        if res=='Ok':
            cur = con.cursor()
            cur.execute(sql)
            recset = cur.fetchall()
            registros = []
            for rec in recset:
                registros.append(rec)
            con.close()
        else:
            res='Erro Conexão'
    except (Exception, psycopg2.DatabaseError) as error:
        registros=0
        res=error.pgcode
        con.close()
    return registros,res

def carga_texto(conn, df, table,limpa):
    tuples = [tuple(x) for x in df.to_numpy()]
    cols = ','.join(list(df.columns))
    query  = "INSERT INTO %s(%s) VALUES %%s" % (table, cols)
    if limpa:
        cursor = conn.cursor()        
        cursor.execute("TRUNCATE TABLE %s" % table)
        conn.commit()
        cursor.close()
    cursor = conn.cursor()
    try:
        psycopg2.extras.execute_values(cursor, query, tuples)
        conn.commit()
        res='Ok'
    except (Exception, psycopg2.DatabaseError) as error:
        print("Error: %s" % error)
        conn.rollback()
        cursor.close()    
        res=error
    cursor.close()
    return res


def MontaDirAbaixo(DirEntrada,DirAbaixo):
    posicao=0
    achou=0
    for i in range(len(DirEntrada)-1,0,-1):
        if DirEntrada[i]=='\\' and achou==0:
            posicao=i
            achou=1        
    dirraiz =DirEntrada[0:posicao]
    dirsaida=os.path.join(dirraiz,DirAbaixo)
    return dirsaida    


def TrataEnd(Endereco):
    if len(Endereco)==0:
        res='Erro01'
        resultado=''
    else:
        res='Ok'
        resultado=Endereco
    return resultado,res


def FluxoDF():
    fluxo = '''{
"elemento":[
{
	"Cliente":"RecVoz",
	"Fluxo":"Voz",
	"Tipo_bot":"Receptivo",
	"Canal":"Voz",
	"Classe":"Inicio",
	"Tipo":"Entrada",
	"Chave":"1",
	"Menu":"Entrada",
	"Nome":"Rec_Voz_Entrada",
	"Tempoespera":"",
	"NumRepet":"",
	"Interacao":"",
	"Qtd_Respostas":"",
	"caminhoArq":"",
	"Mensagem":"",
	"Msg_erro":"",
	"Msg_maxRepet":"",
	"FimdaChamada":"",
	"Curadoria":""
},
{
	"Cliente":"RecVoz",
	"Fluxo":"Voz",
	"Tipo_bot":"Receptivo",
	"Canal":"Voz",
	"Classe":"Interacao",
	"Tipo":"Menu",
	"Chave":"2",
	"Menu":"Carteirizacao",
	"Nome":"Rec_Voz_Carteirizacao",
	"Tempoespera":"3",
	"NumRepet":"3",
	"Interacao":"ASR",
	"Qtd_Respostas":"",
	"caminhoArq":"Rec_Voz_Carteirizacao.alaw",
	"Mensagem":"",
	"Msg_erro":"",
	"Msg_maxRepet":"",
	"FimdaChamada":"",
	"Curadoria":""
}
,
{
	"Cliente":"RecVoz",
	"Fluxo":"Voz",
	"Tipo_bot":"Receptivo",
	"Canal":"Voz",
	"Classe":"Interacao",
	"Tipo":"Condicional",
	"Chave":"3",
	"Menu":"Cart_condicional",
	"Nome":"Rec_Voz_Cart_condicional",
	"Tempoespera":"",
	"NumRepet":"",
	"Interacao":"",
	"Qtd_Respostas":"2",
	"caminhoArq":"",
	"Mensagem":"",
	"Msg_erro":"",
	"Msg_maxRepet":"",
	"FimdaChamada":"",
	"Curadoria":""
}
,
{
	"Cliente":"RecVoz",
	"Fluxo":"Voz",
	"Tipo_bot":"Receptivo",
	"Canal":"Voz",
	"Classe":"Interacao",
	"Tipo":"Resposta",
	"Chave":"4",
	"Menu":"Cart_1",
	"Nome":"Rec_Voz_Cart_1",
	"Tempoespera":"",
	"NumRepet":"",
	"Interacao":"",
	"Qtd_Respostas":"",
	"caminhoArq":"",
	"Mensagem":"",
	"Msg_erro":"",
	"Msg_maxRepet":"",
	"FimdaChamada":"",
	"Curadoria":""
}
,
{
	"Cliente":"RecVoz",
	"Fluxo":"Voz",
	"Tipo_bot":"Receptivo",
	"Canal":"Voz",
	"Classe":"Interacao",
	"Tipo":"Resposta",
	"Chave":"5",
	"Menu":"Cart_2",
	"Nome":"Rec_Voz_Cart_2",
	"Tempoespera":"",
	"NumRepet":"",
	"Interacao":"",
	"Qtd_Respostas":"",
	"caminhoArq":"",
	"Mensagem":"",
	"Msg_erro":"",
	"Msg_maxRepet":"",
	"FimdaChamada":"",
	"Curadoria":""
},
{
	"Cliente":"RecVoz",
	"Fluxo":"Voz",
	"Tipo_bot":"Receptivo",
	"Canal":"Voz",
	"Classe":"Anuncio",
	"Tipo":"Mensagem",
	"Chave":"6",
	"Menu":"Cart_1_MSG",
	"Nome":"Rec_Voz_Cart_1_MSG",
	"Tempoespera":"",
	"NumRepet":"",
	"Interacao":"",
	"Qtd_Respostas":"",
	"caminhoArq":"Rec_Voz_Cart_1_MSG.alaw",
	"Mensagem":"",
	"Msg_erro":"",
	"Msg_maxRepet":"",
	"FimdaChamada":"False",
	"Curadoria":""
},


{
	"Cliente":"RecVoz",
	"Fluxo":"Voz",
	"Tipo_bot":"Receptivo",
	"Canal":"Voz",
	"Classe":"Anuncio",
	"Tipo":"Mensagem",
	"Chave":"7",
	"Menu":"Cart_2_MSG",
	"Nome":"Rec_Voz_Cart_2_MSG",
	"Tempoespera":"",
	"NumRepet":"",
	"Interacao":"",
	"Qtd_Respostas":"",
	"caminhoArq":"Rec_Voz_Cart_2_MSG.alaw",
	"Mensagem":"",
	"Msg_erro":"",
	"Msg_maxRepet":"",
	"FimdaChamada":"False",
	"Curadoria":""
},
{
	"Cliente":"RecVoz",
	"Fluxo":"Voz",
	"Tipo_bot":"Receptivo",
	"Canal":"Voz",
	"Classe":"Interacao",
	"Tipo":"Humano",
	"Chave":"8",
	"Menu":"Cart_1_Hum",
	"Nome":"Rec_Voz_Cart_1_Hum",
	"Tempoespera":"",
	"NumRepet":"",
	"Interacao":"",
	"Qtd_Respostas":"",
	"caminhoArq":"",
	"Mensagem":"",
	"Msg_erro":"",
	"Msg_maxRepet":"",
	"FimdaChamada":"",
	"Curadoria":""
},
{
	"Cliente":"RecVoz",
	"Fluxo":"Voz",
	"Tipo_bot":"Receptivo",
	"Canal":"Voz",
	"Classe":"Interacao",
	"Tipo":"Humano",
	"Chave":"9",
	"Menu":"Cart_2_Hum",
	"Nome":"Rec_Voz_Cart_2_Hum",
	"Tempoespera":"",
	"NumRepet":"",
	"Interacao":"",
	"Qtd_Respostas":"",
	"caminhoArq":"",
	"Mensagem":"",
	"Msg_erro":"",
	"Msg_maxRepet":"",
	"FimdaChamada":"",
	"Curadoria":""
}
]}'''
       
    info = json.loads(fluxo)

    df = json_normalize(info['elemento']) #Results contain the required data
    return df

def RelacionamentoDF():
    js = '''{"relacionamento":
[{
	"chave":"1","predecessor":"0","sucessor":"2"},
	
	{"chave":"2","predecessor":"1","sucessor":"3"},
	
	{"chave":"3","predecessor":"2","sucessor":"4"},
	
	{"chave":"3","predecessor":"2","sucessor":"5"},
	
	{"chave":"4","predecessor":"3","sucessor":"6"},
	
	{"chave":"6","predecessor":"4","sucessor":"8"},
	
	{"chave":"5","predecessor":"3","sucessor":"7"},
	
	{"chave":"7","predecessor":"5","sucessor":"9"}
	
	
]}'''
       
    data = json.loads(js)
    
    df = json_normalize(data['relacionamento']) #Results contain the required data
    return df

def adciona_erro(codigo,onde,chave,df,indice):
	df.append([],ignore_index=True)  
	df.at[indice,'cdErro']  =codigo
	df.at[indice,'onde']	=onde
	df.at[indice,'chave']	=chave
	return df