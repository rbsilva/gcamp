#====================================================================
# Funções do Processo que atendem o Orquestrador 
#====================================================================
import json
# Pandas
import pandas as pd
from   pandas import json_normalize
# Datas
import datetime
from   datetime import datetime
from   datetime import date
import time
# Requisições ou  Sistemas
import requests
import os
import os.path
import psutil as ps
import shutil
import sys
# Módulos
import pyautogui
import glob
import sys, string
import codecs
import subprocess
from   subprocess import Popen
import getpass

# Funções de Requisição
def requisicaoPOST(endereco,dados,fgtrata,sistema): # Request web POST
    try:
        if sistema=='Siebel6':
            if fgtrata=='Sim':
                endereco = endereco.replace('[','')
                endereco = endereco.replace(']','')
            headers = {'Content-Type': 'application/json'}
            response = requests.request("POST", endereco, headers=headers, data=endereco)
            response = response.text
            response = response.replace('true','"true"')
            response = response.replace('false','"false"')
            response = response.replace('True','"true"')
            response = response.replace('False','"false"')    
            return eval(response)
        else:
            payload={}
            response = requests.request("GET", endereco, auth=('siebel8','T4th0@A5!2021rpa'), data=payload)
            response = response.text
            return response

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname=os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        resultado="Erro {} {} {}".format(exc_type,fname,exc_tb.tb_lineno)
        return 'NOK'

def requisicaoPOST_OCR(endereco,dados,fgtrata): # Request web POST
    try:
        codigo="C:\\Orquestrador\\91OCR\\ocr_calendar.exe "+endereco.replace('/','\\').replace('{"Path":','').replace('}','').replace('"','')        
        nomeUsuarioSistema = getpass.getuser()
        #print('nome usuairo',nomeUsuarioSistema)
        # Verificar se o Diretorio ta vazio
        resposta=glob.glob('C:\\Users\\'+nomeUsuarioSistema+'\\AppData\\Local\\Visuri\\ocr_calendar\\backup\\*.*')              
        for arquivo in resposta:            
            os.remove(arquivo)
        # Executabndo OCR    
        p = Popen(codigo,cwd=r"C:\\Orquestrador\\91OCR\\")
        stdout,stderr = p.communicate()               
        
        # Localixando o Arquivo
        resposta=glob.glob('C:\\Users\\'+nomeUsuarioSistema+'\\AppData\\Local\\Visuri\\ocr_calendar\\backup\\*.*')        
        for arquivo in resposta:            
            if arquivo[-5:]=='.json':
                arqjson = arquivo                
            if arquivo[-4:]=='.png':
                arqpng = arquivo                            
        
        # Lendo para Json
        with open(arqjson) as f:
            response=json.load(f)             
        
        return response
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname=os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        resultado="Erro {} {} {}".format(exc_type,fname,exc_tb.tb_lineno)        
        return 'NOK'

# Funções de Interface
def BarraProgresso(descricao,atual,total): # Generate a progress bar
    percentual = int((atual * 100) / total)
    if percentual >= 100:
        percentual = 100
    if percentual <= 0:
        percentual = 0
    QtX = percentual // 10
    QtY = 10 - QtX 
    y = " " * QtY
    x = "■" * QtX
    print(descricao + " - [" + x + y + "] - " + str(percentual) +"% (" + str(atual) + '/' + str(total) + ')', end="\r")  
    
# Funções de Manipulação de endereco
def move_arq(arquivo,dir,de,para):
    try:
        status='OK'
        dir_ori=os.path.join(dir,de)
        dir_des=os.path.join(dir,para)
        if len(arquivo)>0:
            file_name=arquivo
            shutil.move(os.path.join(dir_ori, file_name), dir_des)
        else:
            file_names = os.listdir(dir_ori)
            for file_name in file_names:
                shutil.move(os.path.join(dir_ori, file_name), dir_des)
    except Exception as e:
        status='NOK' 
    return status

def Leituraendereco(arquivo,tipo,qt): # Read Excel and write in JSON file local
    if tipo=='xls':
        endereco   = pd.read_excel(arquivo,engine='openpyxl')
    return endereco    

def leituraJson(endereco,fgpandas): # Loading a local JSON file   
    try:
        with open(endereco) as json_file:
            response = json.load(json_file) 
            response = response['result']
            if fgpandas:
             response = json_normalize(response)
            return response
    except Exception as e:
        return e.args[0]      
    
def CPFcheck(docnumber): # Check the brazilian document it's valid
    if int(docnumber)>0:
        # Obtém apenas os números do CPF, ignorando pontuações
        numbers = [int(digit) for digit in str(docnumber) if digit.isdigit()]

        # Verifica se o CPF possui 11 números ou se todos são iguais:
        if len(numbers) != 11 or len(set(numbers)) == 1:
            return False

        # Validação do primeiro dígito verificador:
        sum_of_products = sum(a*b for a, b in zip(numbers[0:9], range(10, 1, -1)))
        expected_digit = (sum_of_products * 10 % 11) % 10
        if numbers[9] != expected_digit:
            return False

        # Validação do segundo dígito verificador:
        sum_of_products = sum(a*b for a, b in zip(numbers[0:10], range(11, 1, -1)))
        expected_digit = (sum_of_products * 10 % 11) % 10
        if numbers[10] != expected_digit:
            return False
    else:
        return False
    return True    

def CPFTrata(docnumber):
    x='00000000000'+str(docnumber).replace('.0','')
    docnumber=x[-11:]
    return docnumber

def Monta_Pandas_To_Json(chave,Script,TimeOut,produto,data,hora,ext_sis):    
    url=''  
    sistema = ext_sis   
    if produto.upper()=='SIEBEL6':
        nome_colunas                       = ['programName','parameters','showLog','timeOut']
        pandas_json                        = pd.DataFrame([],columns=nome_colunas)
        pandas_json.append([], ignore_index=True)
        pandas_json.at[0,'programName']    = Script    
        if chave!='':
            pandas_json.at[0,'parameters']     = chave
        else:
            pandas_json.at[0,'parameters']     = ''
        pandas_json.at[0,'showLog']        ='true'
        pandas_json.at[0,'timeOut']        =TimeOut
        js                                 = pandas_json.to_json(orient = 'records')
    else:
        if data=='0':
            js = 'http://127.0.0.1:8090/api/siebelv8/'+Script+'?codigoPedido='+chave+sistema       
        else:
            js = 'http://127.0.0.1:8090/api/siebelv8/'+Script+'?codigoPedido='+chave+'&data='+data+'&hora='+hora                
    return js

def monta_nome_dth(dir,prefixo,sufixo):
    tempo=datetime.now() 
    ano=str(tempo.year)
    mes='00'+str(tempo.month)
    dia='00'+str(tempo.day)
    hora='00'+str(tempo.hour)
    min='00'+str(tempo.minute)
    sec='00'+str(tempo.second)
    saida=dir+prefixo+'_'+ano+mes[-2:]+dia[-2:]+'_'+hora[-2:]+min[-2:]+sec[-2:]+sufixo
    return saida

def gera_html(nome_arquivo,endereco,estilo):
    html=endereco.to_html()
    text_file = open(nome_arquivo, "w")
    text_file.write(estilo)
    text_file.write(html)
    text_file.close()

def diamesano_anomesdia(data,separador):
    if separador=='':
        ano=data[4:9]
        mes=data[2:4]
        dia=data[0:2]
        result=ano+mes+dia
    else:
        ano=data[6:11]
        mes=data[3:5]
        dia=data[0:2]
        result=ano+separador+mes+separador+dia
    return result

class Log:
    
    def __init__(self,arquivo):     # Parametrização       
        self.arquivo        = arquivo
        self.contador       = 0
            
    def log_arq_novo(self):
        programa = open(self.arquivo, 'w',encoding='utf-8')
        programa.close                
        
    def log_escreve(self,mensagem):
        self.contador+=1
        today = datetime.now()
        cnt='000'+str(self.contador)
        cnt=cnt[1:4]        
        print(cnt+'-'+'{:%d-%m-%y %H:%M:%S}'.format(today), " : ",mensagem)
        programa = open(self.arquivo, 'a',encoding='utf-8')
        programa.write(cnt+'-'+'{:%d-%m-%y %H:%M:%S}'.format(today) + " : "+mensagem+'\n')
        programa.close 

# Funções de Processo
def carga_fila(dir,onde):
    colunas_fila=['acao','status','onde','lead_number','processamento','arquivo','data','turno','x','y']
    fila=pd.DataFrame([],columns=colunas_fila)   
    dir_ori=os.path.join(dir,onde)
    file_names = os.listdir(dir_ori)
    #==================================================================
    # Varre os Arquivos no Disco Buscando Par(.fil,json)
    #==================================================================
    contador=0
    for file_name in file_names:
        if '.fil' ==file_name[-4:]:
            file_name_json=os.path.join(dir_ori,file_name.replace('.fil','.json'))
            Arquivo       =file_name.replace('.fil','.txt')
            with open(file_name_json) as json_file:
                fila.append([], ignore_index=True)
                response = json.load(json_file) 
                response = json_normalize(response)
                fila.at[contador,'acao']         =response['acao'][0]
                fila.at[contador,'status']       =response['status'][0]
                fila.at[contador,'onde']         =response['onde'][0]
                fila.at[contador,'lead_number']  =response['lead_number'][0]
                fila.at[contador,'processamento']=response['processamento'][0]
                fila.at[contador,'arquivo']      =Arquivo
                fila.at[contador,'data']         =response['data'][0]
                fila.at[contador,'turno']        =response['turno'][0]
                fila.at[contador,'x']            =response['x'][0] 
                fila.at[contador,'y']            =response['y'][0] 
                contador+=1
    return fila

def marcacao_agenda(pparam,parq,pplog,cpf,data,turno,x,y,pedido,sistema):
    #======================================================
    # Paramentros \ Variáveis
    #======================================================
    try:
        parametros      = pparam
        arquivo         = parq
        plog            = pplog
        qtd_validos     = 0
        qtd_invalidos   = 0
        contador        = 0 
        contador_agenda = 0
        resultado       = 'OK'
        dir             = os.path.dirname(__file__)
        cpf             = CPFTrata(cpf)        
        print('2  parq    {}'.format(parq))
        print('3  cpf     {}'.format(cpf))
        print('4  data    {}'.format(data))
        print('5  turno   {}'.format(turno))
        print('6  x       {}'.format(x))        
        print('7  y       {}'.format(y))
        print('8  pedido  {}'.format(pedido))
        print('9  sistema {}'.format(sistema))        
        #=======================================================
        # Saida do Processo
        #=======================================================
        # Agenda
        colunas_agenda=['cpf','data','turno','x','y','dataAgendamento','mensagemAgendamento','statusAgendamento']
        marcacao=pd.DataFrame([],columns=colunas_agenda)        
        
        #=======================================================
        # Configurações do Log
        #=======================================================
        plog.log_escreve('================================================================================')
        plog.log_escreve('Inicio do Processo de Marcação da Agenda')
        plog.log_escreve(' CPF          {} '.format(cpf))
        servicos,resultado =verifica_servicos()
        #=========================================================
        # Inicio do Processo de Automação
        #=========================================================   
        if servicos=='OK':
            if CPFcheck(cpf): 
                if sistema=='siebel6':  
                    plog.log_escreve('Marcando Agenda no Siebel 6')            
                    parametro_marcacao       = str(int(x)+10)+','+str(int(y)+10)
                    pas_agenda               = Monta_Pandas_To_Json(parametro_marcacao ,'Execucao_Agenda','100000','Siebel6','','')    
                    # Automator -----------------------------------------------------------------------------------------------------
                    resultado_automacao_busca= requisicaoPOST(parametros['PostAutomator'],pas_agenda,'Sim','Siebel6')  
                    print('Siebel 6 Resultados')
                    print(resultado_automacao_busca)
                    print(resultado_automacao_busca['content']['statusAgendamento'])
                    print(resultado_automacao_busca['content']['statusAgendamento'])                    
                    if resultado_automacao_busca['content']['statusAgendamento']=='OK':
                        resultado='OK'
                    else:
                        resultado_automacao_busca= requisicaoPOST(parametros['PostAutomator'],pas_agenda,'Sim','Siebel6')   
                        if resultado_automacao_busca['content']['statusAgendamento']=='OK':
                            resultado='OK'
                        else:
                            resultado='NOK' 
                    #-----------------------------------------------------------------------------------------------------------------
                    if resultado=='OK':
                        colunas_agenda=['cpf','data','turno','x','y','dataAgendamento','mensagemAgendamento','statusAgendamento']
                        marcacao.append([], ignore_index=True)                    
                        marcacao.at[0,'cpf']                              =cpf
                        marcacao.at[contador_agenda,'data']               =data
                        marcacao.at[contador_agenda,'turno']              =turno
                        marcacao.at[contador_agenda,'x']                  =x
                        marcacao.at[contador_agenda,'y']                  =y
                        marcacao.at[contador_agenda,'dataAgendamento']    =resultado_automacao_busca['content']['dataAgendamento']
                        marcacao.at[contador_agenda,'mensagemAgendamento']=resultado_automacao_busca['content']['mensagemAgendamento']
                        marcacao.at[contador_agenda,'statusAgendamento']  =resultado_automacao_busca['content']['statusAgendamento']
                        status='OK'
                        resultado='OK'
                        resultado=resultado_automacao_busca['content']['statusAgendamento']
                    else:                    
                        plog.log_escreve('Erro Marcacao Agenda no Siebel 6')
                        status='NOK'
                        resultado='Erro no Processamento Siebel 6'
                        os.system('envieTelegram.exe "Erro Marcacao Agenda no Siebel 6" 656313093')   
                        os.system('envieTelegram.exe "Erro Marcacao Agenda no Siebel 6" 1939911554')   
                        os.system('envieTelegram.exe "Erro Marcacao Agenda no Siebel 6" 1116879369')   
                        # os.system('envieTelegram.exe "Erro Marcacao Agenda no Siebel 6" 1282419893')   
                        
                        
                if sistema=='siebel8':                                    
                    plog.log_escreve('Marcacao Agenda no Siebel 8')                 
                    pas_mar_agen_siebel8 = Monta_Pandas_To_Json(pedido            ,'agendamento'     ,'200000','SIEBEL8',data,turno)                
                    plog.log_escreve(pas_mar_agen_siebel8)
                    resultado_automacao_busca=requisicaoPOST(pas_mar_agen_siebel8,'','','Siebel8')                                
                    siebel8_extracao         = json.loads(resultado_automacao_busca)                    
                    siebel8_erro             = siebel8_extracao['Data']['erro']   
                    status                   = 'OK' if siebel8_extracao['Success'] else 'NOK Erro na Automacao Siebel8'
                    if siebel8_erro is not None:
                        status    = 'NOK '+siebel8_erro
                        resultado='Erro no Processamento Siebel'        
                        os.system('envieTelegram.exe "Erro Marcacao Agenda no Siebel8" 656313093')   
                        os.system('envieTelegram.exe "Erro Marcacao Agenda no Siebel8" 1939911554')   
                        os.system('envieTelegram.exe "Erro Marcacao Agenda no Siebel8" 1116879369')   
                        # os.system('envieTelegram.exe "Erro Marcacao Agenda no Siebel8" 1282419893')   
                        
                    else:
                        status    = 'OK'
                        resultado = 'OK'
                    if status=='OK':   
                        marcacao.at[contador_agenda,'cpf']                =cpf
                        marcacao.at[contador_agenda,'data']               =data
                        marcacao.at[contador_agenda,'turno']              =turno
                        marcacao.at[contador_agenda,'x']                  =x
                        marcacao.at[contador_agenda,'y']                  =y
                        marcacao.at[contador_agenda,'dataAgendamento']    =data
                        marcacao.at[contador_agenda,'mensagemAgendamento']='Agendamento Efetuado'
                        marcacao.at[contador_agenda,'statusAgendamento']  =status                                                
            else:
                resultado='Erro no CPF'
                status   ='NOK'                   
                                    
        #=================================================
        # Avalia o Resulado Final
        #=================================================
        if resultado=='OK':
            marcacao.to_csv(os.path.join(dir+'//03Executado//','Marcacao_')+arquivo, sep = ';',index = False)
        plog.log_escreve(' Marcacao Processada {}'.format('Sim' if resultado=='OK' else 'Não'))
        plog.log_escreve('Fim da Marcacao')        
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname=os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        resultado="Erro {} {} {}".format(exc_type,fname,exc_tb.tb_lineno) 
        plog.log_escreve(' Marcacao Processada {}'.format('Sim' if resultado=='OK' else 'Não'))
        plog.log_escreve('Fim do Processamento da Marcacao')        
    return resultado

def extracao_agenda(pparam,parq,pplog,cpf,pedido,sistema):
    try:
        #======================================================
        # Paramentros \ Variáveis
        #======================================================
        parametros      = pparam
        arquivo         = parq
        chave_aux       = arquivo.split('_')
        chave           = chave_aux[1]+'_'+chave_aux[2]
        chave           = chave.replace('.txt','')
        plog            = pplog
        qtd_validos     = 0
        qtd_invalidos   = 0
        contador        = 0 
        contador_agenda = 0
        resultado       = 'OK'
        dir             = os.path.dirname(__file__)
        cpf             = CPFTrata(cpf)
        colunas_agenda       =['cpf','color','height','ocr_date','ocr_time','width','x','y']
        agenda               =pd.DataFrame([],columns=colunas_agenda)
        pas_ext_lead_siebel8 = Monta_Pandas_To_Json(pedido,'consultapedido'    ,'200000','SIEBEL8','','')
        pas_ext_agen_siebel8 = Monta_Pandas_To_Json(pedido,'consultaagenda'    ,'200000','SIEBEL8','','')
        #=======================================================
        # Configurações do Log
        #=======================================================
        plog.log_escreve('================================================================================')
        plog.log_escreve('Inicio do Processo de Extracao de Agenda')
        plog.log_escreve(' CPF          {} '.format(cpf))        
        resultado,servicos=verifica_servicos()
        plog.log_escreve(' Serviços : {}'.format(resultado))
        #=========================================================
        # Inicio do Processo de Automação
        #=========================================================     
        if servicos=='OK':           
            if sistema=='Siebel6': 
                if CPFcheck(cpf): 
                    status='NOK'
                    #=================================================
                    # Montagem das Chamadas para o Automator e o OCR
                    #=================================================            
                    caminhoImg               = os.path.join(dir+'\\04Imagens\\',cpf+'_'+chave+'.png')            
                    pas_ocr                  = {} 
                    pas_ocr['Path']          = caminhoImg.replace('\\','/')    
                    pas_ocr=json.dumps(pas_ocr)     
                                    
                    pas_agenda               = Monta_Pandas_To_Json(cpf ,'Extracao_Agenda','100000',sistema,'','')            
                    resultado_automacao_busca= requisicaoPOST(parametros['PostAutomator'],pas_agenda,'Sim','Siebel6')
                    print('Retorno da Automacao Siebel6')
                    print(resultado_automacao_busca)
                    
                    #===========================================================#
                    #Joao                                                       #
                    #Resolver bug que da erro ao iniciar automacao              #
                    #===========================================================#
                    if resultado_automacao_busca['content']=='' or resultado_automacao_busca['status']=='erro':
                        fgautomacao = 'NOK'
                    else:
                        if resultado_automacao_busca['content']['statusTelaAgenda']=='OK':
                            fgautomacao = 'OK'
                        else:
                            fgautomacao = 'NOK'
                    
                    if fgautomacao=='OK':
                        myScreenshot  = pyautogui.screenshot()
                        myScreenshot.save(caminhoImg)
                        respostaOcr   = requisicaoPOST_OCR("http://localhost:9091",pas_ocr,'Nao')
                        if checkKey(respostaOcr, 'ERROR')=='OK':
                            fgagendamento =False
                            status        ='NOK'
                            resultado     ='Erro no Processamento da Imagem'
                        else:
                            fgagendamento =True
                            status        ='OK'      
                            resultado     ='OK'
                        if fgagendamento:                    
                            dataframe_ocr = pd.DataFrame.from_dict(respostaOcr)
                            agenda.append([], ignore_index=True)
                            contador_agenda_local=0
                            for index, row in dataframe_ocr.iterrows():
                                agenda.at[contador_agenda,'cpf']     =cpf
                                agenda.at[contador_agenda,'color']   =dataframe_ocr.calendar[index]['color']
                                agenda.at[contador_agenda,'height']  =dataframe_ocr.calendar[index]['height']
                                agenda.at[contador_agenda,'ocr_date']=dataframe_ocr.calendar[index]['ocr date']
                                agenda.at[contador_agenda,'ocr_time']=dataframe_ocr.calendar[index]['ocr time']
                                agenda.at[contador_agenda,'width']   =dataframe_ocr.calendar[index]['width']
                                agenda.at[contador_agenda,'x']       =dataframe_ocr.calendar[index]['x']    
                                agenda.at[contador_agenda,'y']       =dataframe_ocr.calendar[index]['y']               
                                contador_agenda+=1
                                contador_agenda_local=contador_agenda_local+1
                            contador+=1
                        if contador_agenda_local==0:
                            status   = 'NOK'
                            resultado= 'NOK Agenda Vazia' 
                        else:
                            status   ='OK'
                            resultado='OK'                        
                    else:                    
                        plog.log_escreve('Erro Busca Agenda Siebel 6, Buscando Agenda no Siebel 8')
                        sistema='Siebel8'
                        resultado_automacao_busca=requisicaoPOST(pas_ext_agen_siebel8,'','','Siebel8')
                        #=================================================
                        # Carga de Lead´s Sibel 8
                        #=================================================
                        endereco_Busca_CPF = json.loads(resultado_automacao_busca)
                        print(endereco_Busca_CPF)
                        leads_ori       = endereco_Busca_CPF['Data']
                        lead            = leads_ori['lead']     
                        erro_lead       = leads_ori['erro'] 
                        lead_calendar          = leads_ori['calendar']
                        status          = 'OK' if endereco_Busca_CPF['Success']=='True' else 'NOK Erro na Automacao Siebel8'
                        status          = 'OK'
                        if erro_lead is not None:
                            status   = 'NOK'
                            resultado= 'NOK '+erro_lead
                            codigos  =['7029','7016','7037','7049','7065']
                            if erro_lead[0:4] in codigos:
                                status    ='OK'
                                resultado ='OK'                                                                           
                
                        if status=='OK':   
                            contador_agenda_local=0                        
                            for saida in lead_calendar:    
                                agenda.at[contador_agenda,'cpf']     = cpf
                                agenda.at[contador_agenda,'color']   = saida['color']
                                agenda.at[contador_agenda,'height']  = 0
                                agenda.at[contador_agenda,'width']   = 0
                                agenda.at[contador_agenda,'ocr_date']= saida['data']
                                agenda.at[contador_agenda,'ocr_time']= saida['hora']    
                                agenda.at[contador_agenda,'x']       = 0
                                agenda.at[contador_agenda,'y']       = 0
                                contador_agenda+=1
                                contador_agenda_local=contador_agenda_local+1
                            contador+=1
                            
                        if contador_agenda_local==0:
                            status   = 'NOK'
                            resultado= 'NOK Agenda Vazia' 
                        else:
                            status   ='OK'
                            resultado='OK'
                        #print(agenda)
                        if status=='OK':
                            resultado='OK'                           
            else:
                #siebel8
                sistema='Siebel8'
                resultado_automacao_busca=requisicaoPOST(pas_ext_agen_siebel8,'','','Siebel8')
                #=================================================
                # Carga de Lead´s Sibel 8
                #=================================================
                endereco_Busca_CPF = json.loads(resultado_automacao_busca)
                #print(endereco_Busca_CPF)
                leads_ori       = endereco_Busca_CPF['Data']
                lead            = leads_ori['lead']     
                erro_lead       = leads_ori['erro'] 
                lead_calendar          = leads_ori['calendar']
                status          = 'OK' if endereco_Busca_CPF['Success'] else 'NOK Erro na Automacao Siebel8'
                contador_agenda_local          = 0
                if erro_lead is not None:
                    status   ='NOK'
                    resultado='NOK '+erro_lead
                    codigos  =['7029','7016','7037','7049','7065']
                    #print(erro_lead[0:4])
                    if erro_lead[0:4] in codigos:
                        status    ='OK'
                        resultado ='OK'                    
                if status=='OK': 
                    contador_agenda_local=0
                    for saida in lead_calendar:    
                        agenda.at[contador_agenda,'cpf']     = cpf
                        agenda.at[contador_agenda,'color']   = saida['color']
                        agenda.at[contador_agenda,'height']  = 0
                        agenda.at[contador_agenda,'width']   = 0
                        agenda.at[contador_agenda,'ocr_date']= saida['data']
                        agenda.at[contador_agenda,'ocr_time']= saida['hora']    
                        agenda.at[contador_agenda,'x']       = 0
                        agenda.at[contador_agenda,'y']       = 0
                        contador_agenda+=1
                        contador_agenda_local=contador_agenda_local+1
                    contador+=1
                if contador_agenda_local==0:
                    status   = 'NOK'
                    resultado= 'NOK Agenda Vazia'
                else:
                    status   = 'OK'
                    resultado= 'OK'
                #print(agenda)                

        #=================================================
        # Avalia o Resulado Final
        #=================================================
        if sistema=='Siebel8':
            pas_agenda                = Monta_Pandas_To_Json('' ,'Clique_Inatividade','100000','Siebel6','','')            
            resultado_automacao_busca = requisicaoPOST(parametros['PostAutomator'],pas_agenda,'Sim','Siebel6')
            print('Ativar Siebel6')
            
        if resultado=='OK':
            agenda.to_csv(os.path.join(dir+'//03Executado//','Agenda_')+arquivo, sep = ';',index = False)
        plog.log_escreve(' Agenda Processada {}'.format('Sim' if resultado=='OK' else 'Não'))
        plog.log_escreve('Fim do Processamento da Agenda')      
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname=os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        resultado="Erro {} {} {}".format(exc_type,fname,exc_tb.tb_lineno) 
        if agenda.shape[0]>0:
            agenda.to_csv(os.path.join(dir+'//03Executado//','Agenda_')+arquivo, sep = ';',index = False)
        plog.log_escreve(' Agenda Processada {}'.format('Sim' if resultado=='OK' else 'Não'))
        plog.log_escreve('Fim do Processamento da Agenda')        
    return resultado
    
def extracao_leads(pparam,parq,pplog,qtdreg,arquivo_endereco):
    #======================================================
    # Paramentros \ Variáveis
    #======================================================
    try:
        parametros      = pparam
        arquivo         = parq
        chave_aux       = arquivo.split('_')
        chave           = chave_aux[2]+'_'+chave_aux[3]
        chave           = chave.replace('.txt','')
        plog            = pplog
        qtd_validos     = 0
        qtd_invalidos   = 0
        contador        = 0 
        contador_agenda = 0
        resultado       = 'OK'
        dir             = os.path.dirname(__file__)
        parametros['qtlinhasexcel']  = qtdreg
        # Saida do Processo Leads
        colunas_leads =['cpf','nome','plano','velocidade','ID_Bundle','datanasc','sexo','endereco','numconta','statusEnriquecimento']
        leads=pd.DataFrame([],columns=colunas_leads)
        # Saida do Processo Agenda
        colunas_agenda=['cpf','color','height','ocr_date','ocr_time','width','x','y']
        agenda=pd.DataFrame([],columns=colunas_agenda)        
        # Configurações do Log
        plog.log_escreve('================================================================================')
        plog.log_escreve('Inicio do Processo de Enriquecimento do Mailing(extracao_leads)')
        plog.log_escreve('Parametros')
        plog.log_escreve(' Mailing         {} '.format(arquivo_endereco))                
        plog.log_escreve('Leitura de endereco')
        # Leitura do Arquivo do Mailning para o Agente Específico           
        if os.path.isfile(os.path.join(dir+'\\00Entrada\\',arquivo_endereco)):
            endereco         = pd.read_csv(os.path.join(dir+'\\00Entrada\\',arquivo_endereco),sep=';')        
        else:
            os.system('envieTelegram.exe "Arquivo de Leads não existe" 656313093')   
            os.system('envieTelegram.exe "Arquivo de Leads não existe" 1939911554')   
            os.system('envieTelegram.exe "Arquivo de Leads não existe" 1116879369')   
            # os.system('envieTelegram.exe "Arquivo de Leads não existe" 1282419893')   

        qtd_registros = endereco.shape[0]        
        endereco['COD_SISTEMA']='X'
        plog.log_escreve(' Quantidade de Registros Lidos {}'.format(endereco.shape[0]))
        plog.log_escreve('')
        plog.log_escreve('Inicio do Enriquecimento'.format(endereco.shape[0]))   
        # Avalia se os Servições estão de Pé 
        servicos,resultado =verifica_servicos()                            
        if servicos=='OK':
            cont=0
            for index, linhas in endereco.iterrows():
                # ==============================================================================================
                # Verificações Básicas CPF e Sistema Abertos
                # ==============================================================================================
                # Chaves
                cpf      = CPFTrata(endereco.at[index,parametros['chave']])
                print('pqp cpf',cpf)
                pedido   = endereco.at[index,'CAMPOX1']                         
                produto  = endereco.at[index,'CAMPOX8']    
                endereco = endereco.at[index,'CAMPOX21']
                if type(produto) is float:
                    erro='sim'
                else:
                    erro='nao'
                if CPFcheck(cpf) and erro=='nao':                
                    pedido  = endereco.at[index,'CAMPOX1']                         
                    produto = endereco.at[index,'CAMPOX8']    
                    caminhoImg    = os.path.join(dir+'\\04Imagens\\',cpf+'_'+chave+'.png')                
                    pas_ocr = {} 
                    pas_ocr['Path'] = caminhoImg.replace('\\','/')    
                    pas_ocr=json.dumps(pas_ocr)            
                    # Automator
                    pas_ext_lead         = Monta_Pandas_To_Json(cpf,'Extracao_Leads'    ,'200000','SIEBEL6','','')
                    pas_ava_agen         = Monta_Pandas_To_Json('' ,'Avalia_Agendamento','100000','SIEBEL6','','')            
                    # Siebel 8
                    pas_ext_lead_siebel8 = Monta_Pandas_To_Json(pedido,'consultapedido'    ,'200000','SIEBEL8','','')
                    pas_ext_agen_siebel8 = Monta_Pandas_To_Json(pedido,'consultaagenda'    ,'200000','SIEBEL8','','')                                       
                    COD_SISTEMA='X' 
                    if produto.upper()=='OI TOTAL': # Siebel6    
                        sistema='Siebel6'                                                       
                        status='NOK'                    
                        #=================================================
                        # Inicio da Automação
                        #=================================================                                             
                        resultado_automacao_busca=requisicaoPOST(parametros['PostAutomator'],pas_ext_lead,'Sim','Siebel6')

                        #=================================================
                        # Carga de Lead´s Sibel 6
                        #=================================================
                        leads.append([], ignore_index=True)
                        endereco_Busca_CPF = pd.DataFrame.from_dict(resultado_automacao_busca)
                        
                        for index, linha in endereco_Busca_CPF.iterrows():
                            if   index=='cpf':
                                leads.at[contador,'cpf']                 =endereco_Busca_CPF.at[index,'content']
                            elif index=='nome':
                                leads.at[contador,'nome']                =endereco_Busca_CPF.at[index,'content'] 
                            elif index=='plano':
                                leads.at[contador,'plano']               =endereco_Busca_CPF.at[index,'content']
                            elif index=='velocidade':
                                leads.at[contador,'velocidade']          =endereco_Busca_CPF.at[index,'content']
                            elif index=='ID_Bundle':
                                leads.at[contador,'ID_Bundle']           =endereco_Busca_CPF.at[index,'content']
                            elif index=='datanasc':
                                leads.at[contador,'datanasc']            =endereco_Busca_CPF.at[index,'content']
                            elif index=='sexo':
                                leads.at[contador,'sexo']                =endereco_Busca_CPF.at[index,'content']
                            elif index=='endereco':
                                leads.at[contador,'endereco']            =endereco
                            elif index=='numconta':
                                leads.at[contador,'numconta']            =endereco_Busca_CPF.at[index,'content']
                            elif index=='statusEnriquecimento':
                                leads.at[contador,'statusEnriquecimento']=endereco_Busca_CPF.at[index,'content']
                                status                                   =endereco_Busca_CPF.at[index,'content']
                            #===============================================
                            # Modificação para o Status de Enriquecimento
                            #===============================================
                            if status=='OK':
                                valor =1 if len(leads.at[contador,'cpf'])>0        else 0
                                valor+=1 if len(leads.at[contador,'nome'])>0       else 0
                                valor+=1 if len(leads.at[contador,'plano'])>0      else 0
                                valor+=1 if len(leads.at[contador,'velocidade'])>0 else 0
                                valor+=1 if len(leads.at[contador,'ID_Bundle'])>0  else 0
                                valor+=1 if len(leads.at[contador,'datanasc'])>0   else 0
                                valor+=1 if len(leads.at[contador,'sexo'])>0       else 0
                                valor+=1 if len(endereco)>0                        else 0
                                valor+=1 if len(leads.at[contador,'numconta'])>0   else 0
                                if valor < 9:
                                    status ='NOK'
                                    leads.at[contador,'statusEnriquecimento']='NOK'                            
                        # Avalia se a tela é do agendamento seguindo direto
                        myScreenshot  = pyautogui.screenshot()
                        myScreenshot.save(caminhoImg)
                        
                        if resultado_automacao_busca['content']['statusEnriquecimento']!='OK':                        
                            status=='NOK'                                
                            sistema='Siebel8'
                            resultado_automacao_busca=requisicaoPOST(pas_ext_lead_siebel8,'','','Siebel8')
                            #=================================================
                            # Carga de Lead´s Sibel 8
                            #=================================================
                            endereco_Busca_CPF = json.loads(resultado_automacao_busca)
                            leads_ori       = endereco_Busca_CPF['Data']
                            #       lead            = leads_ori['lead']     
                            erro_lead       = leads_ori['erro'] 
                            lead_calendar          = leads_ori['calendar']                            
                            status          = 'OK' if endereco_Busca_CPF['Success'] else 'NOK Erro na Automacao Siebel8'
                            if erro_lead is not None:
                                status   = 'NOK'
                                resultado= 'NOK '+erro_lead
                                codigos  =['7029','7016','7037','7049','7065']
                                if erro_lead[0:4] in codigos:
                                    status    ='OK'
                                    resultado ='OK'                              

                            if status=='OK': 
                                cnt=0
                                for saida in lead_calendar:    
                                    agenda.at[contador_agenda,'cpf']     = cpf
                                    agenda.at[contador_agenda,'color']   = saida['color']
                                    agenda.at[contador_agenda,'height']  = 0
                                    agenda.at[contador_agenda,'width']   = 0
                                    agenda.at[contador_agenda,'ocr_date']= saida['data']
                                    agenda.at[contador_agenda,'ocr_time']= saida['hora']    
                                    agenda.at[contador_agenda,'x']       = 0
                                    agenda.at[contador_agenda,'y']       = 0
                                    contador_agenda+=1
                                    cnt=cnt+1
                                if cnt ==0:
                                    status = 'NOK Sem Calendario'
                                if status=='OK':
                                    leads.at[contador,'statusEnriquecimento']='OK'
                                    COD_SISTEMA='Siebel8'  
                                        
                        else:
                            status=='OK'                    
                            #===============================================
                            # Chama o OCR
                            #===============================================                        
                            respostaOcr   = requisicaoPOST_OCR("http://localhost:9091",pas_ocr,'Nao')
                            if checkKey(respostaOcr, 'ERROR')!='OK':
                                dataframe_ocr = pd.DataFrame.from_dict(respostaOcr)
                                agenda.append([], ignore_index=True)
                                for index, row in dataframe_ocr.iterrows():
                                    agenda.at[contador_agenda,'cpf']     =cpf
                                    agenda.at[contador_agenda,'color']   =dataframe_ocr.calendar[index]['color']
                                    agenda.at[contador_agenda,'height']  =dataframe_ocr.calendar[index]['height']
                                    agenda.at[contador_agenda,'ocr_date']=dataframe_ocr.calendar[index]['ocr date']                                
                                    agenda.at[contador_agenda,'ocr_time']=dataframe_ocr.calendar[index]['ocr time']
                                    agenda.at[contador_agenda,'width']   =dataframe_ocr.calendar[index]['width']
                                    agenda.at[contador_agenda,'x']       =dataframe_ocr.calendar[index]['x']    
                                    agenda.at[contador_agenda,'y']       =dataframe_ocr.calendar[index]['y']               
                                    contador_agenda+=1       
                                COD_SISTEMA='Siebel6'  
                            else:
                                sistema='Sibel 6 > Siebel 8'
                                status='NOK'
                    else:
                        sistema='Siebel8'
                        resultado_automacao_busca=requisicaoPOST(pas_ext_lead_siebel8,'','','Siebel8')
                        #=================================================
                        # Carga de Lead´s Sibel 8
                        #=================================================
                        endereco_Busca_CPF = json.loads(resultado_automacao_busca)
                        leads_ori       = endereco_Busca_CPF['Data']
                        lead            = leads_ori['lead']     
                        erro_lead       = leads_ori['erro'] 
                        lead_calendar          = leads_ori['calendar']
                        status          = 'OK' if endereco_Busca_CPF['Success'] else 'NOK Erro na Automacao Siebel8'
                        resultado       = status
                        if erro_lead is not None:
                            status   = 'NOK'
                            resultado= 'NOK '+erro_lead
                            codigos  =['7029','7016','7037','7049','7065']
                            if erro_lead[0:4] in codigos:
                                status    ='OK'
                                resultado ='OK'                                                                                                                           
                        
                        if status=='OK':                            
                            leads.append([], ignore_index=True)
                            leads.at[contador,'cpf']                 = lead[0:1][0]['CPF']
                            leads.at[contador,'nome']                = lead[0:1][0]['nome']
                            leads.at[contador,'plano']               = ''
                            leads.at[contador,'velocidade']          = lead[0:1][0]['velocidade']
                            leads.at[contador,'ID_Bundle']           = ''
                            leads.at[contador,'datanasc']            = lead[0:1][0]['dataNascimento']
                            leads.at[contador,'sexo']                = lead[0:1][0]['sexo'] 
                            leads.at[contador,'endereco']            = endereco
                            leads.at[contador,'numconta']            = ''
                            leads.at[contador,'statusEnriquecimento']= status
                            contador_agenda_local=0
                            for saida in lead_calendar:    
                                agenda.at[contador_agenda,'cpf']     = cpf
                                agenda.at[contador_agenda,'color']   = saida['color']
                                agenda.at[contador_agenda,'height']  = 0
                                agenda.at[contador_agenda,'width']   = 0
                                agenda.at[contador_agenda,'ocr_date']= saida['data']
                                agenda.at[contador_agenda,'ocr_time']= saida['hora']    
                                agenda.at[contador_agenda,'x']       = 0
                                agenda.at[contador_agenda,'y']       = 0
                                contador_agenda_local=contador_agenda_local+1
                                contador_agenda+=1
                            
                            if contador_agenda_local==0:
                                status   = 'NOK'
                                resultado= 'NOK Agenda Vazia' 
                            else:
                                status   ='OK'
                                COD_SISTEMA='Siebel8'  
                                resultado='OK'           
                else:
                    resultado='Erro no CPF'        
                    status   ='NOK Erro no CPF'
                    sistema  =''                    
                                        
                contador+=1
                if status=='OK':
                    qtd_validos+=1
                else:
                    qtd_invalidos+=1                        
                plog.log_escreve('Evolução {} / {} : CPF {} Status {} Sistema {}'.format(contador,qtd_registros,cpf,status,sistema))
                # print('cod sistema ',COD_SISTEMA) #Dorival 20211007
                # print('index ',cont) #Dorival 20211007
                # endereco['COD_SISTEMA'] = COD_SISTEMA #Dorival 20211007
                endereco.at[cont,'COD_SISTEMA'] = COD_SISTEMA               
                cont=cont+1
                if qtd_invalidos>=int(qtd_registros*0.30):
                    os.system('envieTelegram.exe "Erro Enriquecimento - 30% erros" 656313093')   
                    os.system('envieTelegram.exe "Erro Enriquecimento - 30% erros" 1939911554')   
                    os.system('envieTelegram.exe "Erro Enriquecimento - 30% erros" 1116879369')   
                    # os.system('envieTelegram.exe "Erro Enriquecimento - 30% erros" 1282419893')   

        plog.log_escreve(' Leads Processados {} Validos {} Invalidos {}'.format(qtd_registros,qtd_validos,qtd_invalidos))
        plog.log_escreve('Fim do Enriquecimento')
        plog.log_escreve('')
        #=================================================
        # Avalia o Resulado Final
        #=================================================
        if qtd_validos>0:
            leads.to_csv(os.path.join(dir+'//03Executado//','Leads_')+arquivo, sep = ';',index = False)
            agenda.to_csv(os.path.join(dir+'//03Executado//','Agenda_')+arquivo, sep = ';',index = False)                   
            arquivo_endereco  = hoje_anomedia()+'_'+'Consultaendereco.csv'     
            endereco.to_csv(os.path.join(dir+'\\00Entrada\\',arquivo_endereco), sep = ';',index = False) 
            
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname=os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        resultado="Erro {} {} {}".format(exc_type,fname,exc_tb.tb_lineno) 
        if leads.shape[0]>0:
            leads.to_csv(os.path.join(dir+'//03Executado//','Leads_')+arquivo, sep = ';',index = False)
            agenda.to_csv(os.path.join(dir+'//03Executado//','Agenda_')+arquivo, sep = ';',index = False)
        plog.log_escreve(' Lead´s Processados {} Validos {} Enriquecidos {}'.format(qtd_registros,qtd_validos,qtd_invalidos))
        plog.log_escreve('Fim do Enriquecimento')
        plog.log_escreve('')
        os.system('envieTelegram.exe "Erro Extracao Leads" 656313093')   
        os.system('envieTelegram.exe "Erro Extracao Leads" 1939911554')   
        os.system('envieTelegram.exe "Erro Extracao Leads" 1116879369')   
        # os.system('envieTelegram.exe "Erro Extracao Leads" 1282419893')   

    return resultado

##JOAO
def avalia_sistema(pparam,parq,pplog,qtdreg,arquivo_endereco):
    #======================================================
    # Paramentros \ Variáveis
    #======================================================
    try:
        parametros      = pparam
        arquivo         = parq
        chave_aux       = arquivo.split('_')
        chave           = chave_aux[2]+'_'+chave_aux[3]
        chave           = chave.replace('.txt','')
        plog            = pplog
        qtd_validos     = 0
        qtd_invalidos   = 0
        contador        = 0 
        contador_agenda = 0
        resultado       = 'OK'
        dir             = os.path.dirname(__file__)
        parametros['qtlinhasexcel']  = qtdreg
        # Saida do Processo Leads
        colunas_leads =['cpf','nome','plano','velocidade','ID_Bundle','datanasc','sexo','endereco','numconta','statusEnriquecimento','sistema']
        leads=pd.DataFrame([],columns=colunas_leads)
        # Saida do Processo Agenda
        colunas_agenda=['cpf','color','height','ocr_date','ocr_time','width','x','y']
        agenda=pd.DataFrame([],columns=colunas_agenda)        
        # Configurações do Log
        plog.log_escreve('================================================================================')
        plog.log_escreve('Inicio do Processo de Enriquecimento do Mailing(extracao_leads)')
        plog.log_escreve('Parametros')
        plog.log_escreve(' Mailing         {} '.format(arquivo_endereco))                
        plog.log_escreve('Leitura de endereco')
        # Leitura do Arquivo do Mailning para o Agente Específico           
        if os.path.isfile(os.path.join(dir+'\\00Entrada\\',arquivo_endereco)):
            endereco         = pd.read_csv(os.path.join(dir+'\\00Entrada\\',arquivo_endereco),sep=';')        
        else:
            os.system('envieTelegram.exe "Arquivo de Leads não existe" 656313093')   
            os.system('envieTelegram.exe "Arquivo de Leads não existe" 1939911554')   
            os.system('envieTelegram.exe "Arquivo de Leads não existe" 1116879369')   
            # os.system('envieTelegram.exe "Arquivo de Leads não existe" 1282419893')   

        qtd_registros = endereco.shape[0]        
        endereco['COD_SISTEMA']='X'
        plog.log_escreve(' Quantidade de Registros Lidos {}'.format(endereco.shape[0]))
        plog.log_escreve('')
        plog.log_escreve('Inicio do Enriquecimento'.format(endereco.shape[0]))   
        # Avalia se os Servições estão de Pé 
        servicos,resultado =verifica_servicos()                            
        if servicos=='OK':
            cont=0
            for index, linhas in endereco.iterrows():
                # ==============================================================================================
                # Verificações Básicas CPF e Sistema Abertos
                # ==============================================================================================
                # Chaves
                cpf      = CPFTrata(endereco.at[index,parametros['chave']])
                pedido   = endereco.at[index,'CAMPOX1']                         
                produto  = endereco.at[index,'CAMPOX8']    
                endereco = endereco.at[index,'CAMPOX21']
                if type(produto) is float:
                    erro='sim'
                else:
                    erro='nao'
                if CPFcheck(cpf) and erro=='nao':                
                    pedido  = endereco.at[index,'CAMPOX1']                         
                    produto = endereco.at[index,'CAMPOX8']    
                    caminhoImg    = os.path.join(dir+'\\04Imagens\\',cpf+'_'+chave+'.png')                
                    pas_ocr = {} 
                    pas_ocr['Path'] = caminhoImg.replace('\\','/')    
                    pas_ocr=json.dumps(pas_ocr)            
                    # Automator
                    pas_ext_lead         = Monta_Pandas_To_Json(cpf,'Extracao_Leads'    ,'200000','SIEBEL6','','','')
                    pas_ava_agen         = Monta_Pandas_To_Json('' ,'Avalia_Agendamento','100000','SIEBEL6','','','') 
                    pas_ext_sist         = Monta_Pandas_To_Json(cpf,'Extracao_Sistema'    ,'200000','SIEBEL6','','','')           
                    # Siebel 8
                    pas_ext_lead_siebel8 = Monta_Pandas_To_Json(pedido,'consultapedido'    ,'200000','SIEBEL8','','','')
                    pas_ext_agen_siebel8 = Monta_Pandas_To_Json(pedido,'consultaagenda'    ,'200000','SIEBEL8','','','') 
                    pas_ext_sist_siebel8 = Monta_Pandas_To_Json(pedido,'consultaagenda'    ,'200000','SIEBEL8','0','','&extraiAgenda=false')                   
                    COD_SISTEMA='X' 
                    if produto.upper()=='OI TOTAL': # Siebel6    
                        sistema='Siebel6'                                                       
                        status='NOK'                    
                        #=================================================
                        # Inicio da Automação
                        #=================================================                                             
                        resultado_automacao_busca=requisicaoPOST(parametros['PostAutomator'],pas_ext_sist,'Sim','Siebel6')

                        #=================================================
                        # Carga de Lead´s Sibel 6
                        #=================================================
                        leads.append([], ignore_index=True)
                        endereco_Busca_CPF = pd.DataFrame.from_dict(resultado_automacao_busca)
                        
                        for index, linha in endereco_Busca_CPF.iterrows():
                            if   index=='cpf':
                                leads.at[contador,'cpf']                 =endereco_Busca_CPF.at[index,'content']
                            elif index=='nome':
                                leads.at[contador,'nome']                =endereco_Busca_CPF.at[index,'content'] 
                            elif index=='plano':
                                leads.at[contador,'plano']               =endereco_Busca_CPF.at[index,'content']
                            elif index=='velocidade':
                                leads.at[contador,'velocidade']          =endereco_Busca_CPF.at[index,'content']
                            elif index=='ID_Bundle':
                                leads.at[contador,'ID_Bundle']           =endereco_Busca_CPF.at[index,'content']
                            elif index=='datanasc':
                                leads.at[contador,'datanasc']            =endereco_Busca_CPF.at[index,'content']
                            elif index=='sexo':
                                leads.at[contador,'sexo']                =endereco_Busca_CPF.at[index,'content']
                            elif index=='endereco':
                                leads.at[contador,'endereco']            =endereco
                            elif index=='numconta':
                                leads.at[contador,'numconta']            =endereco_Busca_CPF.at[index,'content']
                            elif index=='statusEnriquecimento':
                                leads.at[contador,'statusEnriquecimento']=endereco_Busca_CPF.at[index,'content']
                                status                                   =endereco_Busca_CPF.at[index,'content']
                            elif index=='sistema':
                                leads.at[contador,'sistema']             =endereco_Busca_CPF.at[index,'content']
                            #===============================================
                            # Modificação para o Status de Enriquecimento
                            #===============================================
                            if status=='OK':
                                valor =1 if len(leads.at[contador,'cpf'])>0        else 0
                                valor+=1 if len(leads.at[contador,'nome'])>0       else 0
                                valor+=1 if len(leads.at[contador,'plano'])>0      else 0
                                valor+=1 if len(leads.at[contador,'velocidade'])>0 else 0
                                valor+=1 if len(leads.at[contador,'ID_Bundle'])>0  else 0
                                valor+=1 if len(leads.at[contador,'datanasc'])>0   else 0
                                valor+=1 if len(leads.at[contador,'sexo'])>0       else 0
                                valor+=1 if len(endereco)>0                        else 0
                                valor+=1 if len(leads.at[contador,'numconta'])>0   else 0
                                if valor < 9:
                                    status ='NOK'
                                    leads.at[contador,'statusEnriquecimento']='NOK'                            
                        # Avalia se a tela é do agendamento seguindo direto
                        myScreenshot  = pyautogui.screenshot()
                        myScreenshot.save(caminhoImg)
                        
                        if resultado_automacao_busca['content']['statusEnriquecimento']!='OK': 
                            
                            status=='NOK'                                
                            sistema='Siebel8'
                            resultado_automacao_busca=requisicaoPOST(pas_ext_sist_siebel8,'','','Siebel8')
                            #=================================================
                            # Carga de Lead´s Sibel 8
                            #=================================================
                            endereco_Busca_CPF = json.loads(resultado_automacao_busca)
                            leads_ori       = endereco_Busca_CPF['Data']
                            #       lead            = leads_ori['lead']     
                            erro_lead       = leads_ori['erro'] 
                            lead_calendar          = leads_ori['calendar']                            
                            status          = 'OK' if endereco_Busca_CPF['Success'] and leads_ori['sistema']!= 'X' else 'NOK Sem agenda'
                            sistema_auto     = leads_ori['sistema'] 
                            print('leads ori:',leads_ori)
                            print(sistema_auto)
                            if erro_lead is not None:
                                status   = 'NOK'
                                resultado= 'NOK '+erro_lead
                                codigos  =['7029','7016','7037','7049','7065']
                                if erro_lead[0:4] in codigos:
                                    status    ='OK'
                                    resultado ='OK' 
                                    
                            #if status == 'OK':   
                            #    leads.at[contador,'statusEnriquecimento']='OK'
                            #    print('sistema:'+sistema_auto)
                            if sistema_auto == '8':
                                COD_SISTEMA='Siebel8'
                                leads.at[contador,'statusEnriquecimento'] = status
                                print('sistema_auto == 8',COD_SISTEMA)
                            else:
                                COD_SISTEMA='X'
                                print('sistema_auto else',COD_SISTEMA)
                        else:
                            status=='OK'                    
                            #===============================================
                            # Chama o OCR
                            #===============================================                        
                            respostaOcr   = requisicaoPOST_OCR("http://localhost:9091",pas_ocr,'Nao')
                            if checkKey(respostaOcr, 'ERROR')!='OK':
                                dataframe_ocr = pd.DataFrame.from_dict(respostaOcr)
                                agenda.append([], ignore_index=True)
                                for index, row in dataframe_ocr.iterrows():
                                    agenda.at[contador_agenda,'cpf']     =cpf
                                    agenda.at[contador_agenda,'color']   =dataframe_ocr.calendar[index]['color']
                                    agenda.at[contador_agenda,'height']  =dataframe_ocr.calendar[index]['height']
                                    agenda.at[contador_agenda,'ocr_date']=dataframe_ocr.calendar[index]['ocr date']                                
                                    agenda.at[contador_agenda,'ocr_time']=dataframe_ocr.calendar[index]['ocr time']
                                    agenda.at[contador_agenda,'width']   =dataframe_ocr.calendar[index]['width']
                                    agenda.at[contador_agenda,'x']       =dataframe_ocr.calendar[index]['x']    
                                    agenda.at[contador_agenda,'y']       =dataframe_ocr.calendar[index]['y']               
                                    contador_agenda+=1   
                                
                                print('siebel 6: ',leads.at[contador,'sistema'])
                                if leads.at[contador,'sistema'] == '6':
                                    COD_SISTEMA='Siebel6'
                                    status = 'OK'
                                else:
                                    COD_SISTEMA='X'
                                print('OK = Siebel6: ',COD_SISTEMA)    
                            else:
                                sistema='Sibel 6 > Siebel 8'
                                status='NOK'
                    else:
                        sistema='Siebel8'
                        resultado_automacao_busca=requisicaoPOST(pas_ext_sist_siebel8,'','','Siebel8')
                        #=================================================
                        # Carga de Lead´s Sibel 8
                        #=================================================
                        endereco_Busca_CPF = json.loads(resultado_automacao_busca)
                        leads_ori       = endereco_Busca_CPF['Data']
                        lead            = leads_ori['lead']     
                        erro_lead       = leads_ori['erro'] 
                        lead_calendar          = leads_ori['calendar']
                        status          = 'OK' if endereco_Busca_CPF['Success'] and leads_ori['sistema']!= 'X' else 'NOK Sem agenda'
                        resultado       = status
                        sistema_auto    = leads_ori['sistema'] 
                        print('sistema auto 2: ',sistema_auto)
                        if erro_lead is not None:
                            status   = 'NOK'
                            resultado= 'NOK '+erro_lead
                            codigos  =['7029','7016','7037','7049','7065']
                            if erro_lead[0:4] in codigos:
                                status    ='OK'
                                resultado ='OK'                                                                                                                           
                        
                        if status=='OK':                            
                            leads.append([], ignore_index=True)
                            leads.at[contador,'cpf']                 = lead[0:1][0]['CPF']
                            leads.at[contador,'nome']                = lead[0:1][0]['nome']
                            leads.at[contador,'plano']               = ''
                            leads.at[contador,'velocidade']          = lead[0:1][0]['velocidade']
                            leads.at[contador,'ID_Bundle']           = ''
                            leads.at[contador,'datanasc']            = lead[0:1][0]['dataNascimento']
                            leads.at[contador,'sexo']                = lead[0:1][0]['sexo'] 
                            leads.at[contador,'endereco']            = endereco
                            leads.at[contador,'numconta']            = ''
                            leads.at[contador,'statusEnriquecimento']= status
                            leads.at[contador,'sistema']             = lead[0:1][0]['sistema'] 
                            
                            if sistema_auto == '8':
                                COD_SISTEMA='Siebel8'
                                resultado='OK' 
                                status   ='OK'
                                
                            else:
                                COD_SISTEMA='X'          
                else:
                    resultado='Erro no CPF'        
                    status   ='NOK Erro no CPF'
                    sistema  =''   
                    
                leads.at[contador,'sistema'] = COD_SISTEMA
                contador+=1
                if status=='OK':
                    qtd_validos+=1
                else:
                    qtd_invalidos+=1                        
                plog.log_escreve('Evolução {} / {} : CPF {} Status {} Sistema {}'.format(contador,qtd_registros,cpf,status,sistema))
                print('cod sistema final: ',COD_SISTEMA) #Dorival 20211007
                # print('index ',cont) #Dorival 20211007
                # endereco['COD_SISTEMA'] = COD_SISTEMA #Dorival 20211007
                endereco.at[cont,'COD_SISTEMA'] = COD_SISTEMA 
                               
                cont=cont+1
                if qtd_invalidos>=int(qtd_registros*0.30):
                    os.system('envieTelegram.exe "Erro Enriquecimento - 30% erros" 656313093')   
                    os.system('envieTelegram.exe "Erro Enriquecimento - 30% erros" 1939911554')   
                    os.system('envieTelegram.exe "Erro Enriquecimento - 30% erros" 1116879369')   
                    # os.system('envieTelegram.exe "Erro Enriquecimento - 30% erros" 1282419893')   

        plog.log_escreve(' Leads Processados {} Validos {} Invalidos {}'.format(qtd_registros,qtd_validos,qtd_invalidos))
        plog.log_escreve('Fim do Enriquecimento')
        plog.log_escreve('')
        #=================================================
        # Avalia o Resulado Final
        #=================================================
        if qtd_validos>0:
            leads.to_csv(os.path.join(dir+'//03Executado//','Leads_')+arquivo, sep = ';',index = False)
            agenda.to_csv(os.path.join(dir+'//03Executado//','Agenda_')+arquivo, sep = ';',index = False)                   
            arquivo_endereco  = hoje_anomedia()+'_'+'Consultaendereco.csv'     
            endereco.to_csv(os.path.join(dir+'\\00Entrada\\',arquivo_endereco), sep = ';',index = False) 
            
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname=os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        resultado="Erro {} {} {}".format(exc_type,fname,exc_tb.tb_lineno) 
        if leads.shape[0]>0:
            leads.to_csv(os.path.join(dir+'//03Executado//','Leads_')+arquivo, sep = ';',index = False)
            agenda.to_csv(os.path.join(dir+'//03Executado//','Agenda_')+arquivo, sep = ';',index = False)
        plog.log_escreve(' Lead´s Processados {} Validos {} Enriquecidos {}'.format(qtd_registros,qtd_validos,qtd_invalidos))
        plog.log_escreve('Fim do Enriquecimento')
        plog.log_escreve('')
        os.system('envieTelegram.exe "Erro Extracao Leads" 656313093')   
        os.system('envieTelegram.exe "Erro Extracao Leads" 1939911554')   
        os.system('envieTelegram.exe "Erro Extracao Leads" 1116879369')   
        # os.system('envieTelegram.exe "Erro Extracao Leads" 1282419893')   

    return resultado

def verifica_servicos(): 
    statusAutomator = 'NOK'    
    statusSiebel    = 'NOK'
    statusSiebel8   = 'NOK'    
    for p in ps.process_iter():
        if p.name()=='Autormator.server.exe':
            statusAutomator = 'OK'
        if p.name()=='wfica32.exe':
            statusSiebel    = 'OK'
        if p.name()=='PAVirtual.RPA.Siebel.API.exe' or p.name()=='pavirtual.rpa.siebel.api.exe':            
            statusSiebel8    = 'OK'    
    if statusSiebel=='OK' and statusAutomator == 'OK' and statusSiebel8 == 'OK':
        msg1='OK'
        msg2='OK'
    else:
        msg1='Erro Fatal Sistema Fechados'
        msg2='NOK'
    if statusSiebel=='NOK':
        os.system('envieTelegram.exe "Siebel 6 Fechado" 656313093')   
        os.system('envieTelegram.exe "Siebel 6 Fechado" 1939911554')   
        os.system('envieTelegram.exe "Siebel 6 Fechado" 1116879369')   
        # os.system('envieTelegram.exe "Siebel 6 Fechado" 1282419893')           
        
        #os.startfile('c:\\Orquestrador\\Siebel6Fechado.bat')   
    if statusAutomator == 'NOK':
        # os.system('envieTelegram.exe "Automator Fechado" 656313093,1939911554,1116879369')       
        os.system('envieTelegram.exe "Automator Fechado" 656313093')   
        os.system('envieTelegram.exe "Automator Fechado" 1939911554')   
        os.system('envieTelegram.exe "Automator Fechado" 1116879369')   
        # os.system('envieTelegram.exe "Automator Fechado" 1282419893')        
        
    if statusSiebel8 == 'NOK':
        # os.system('envieTelegram.exe "Siebel 8 Fechado" 656313093,1939911554,1116879369')       
        os.system('envieTelegram.exe "Siebel 8 Fechado" 656313093')   
        os.system('envieTelegram.exe "Siebel 8 Fechado" 1939911554')   
        os.system('envieTelegram.exe "Siebel 8 Fechado" 1116879369')         
        # os.system('envieTelegram.exe "Siebel 8 Fechado" 1282419893')   
    return msg1,msg2

def avalia_saida_sibel8(endereco):
    return 

def checkKey(dict, key):    
    if key in dict.keys(): 
        return 'OK' 
    else: 
        return 'NOK'

def mes_em_numero(endereco):
    endereco=converteDataSiebel8para6(endereco)
    mes=''
    #Pega o Mes nro
    if(endereco[-3:]) == 'Jan':
        mes='01'
    elif(endereco[-3:]) == 'Fev':
        mes='02'
    elif(endereco[-3:]) == 'Mar':
        mes='03'
    elif(endereco[-3:]) == 'Abr':
        mes='04'
    elif(endereco[-3:]) == 'Mai':
        mes='05'
    elif(endereco[-3:]) == 'Jun':
        mes='06'
    elif(endereco[-3:]) == 'Jul':
        mes='07'
    elif(endereco[-3:]) == 'Ago':
        mes='08'
    elif(endereco[-3:]) == 'Set':
        mes='09'
    elif(endereco[-3:]) == 'Out':
        mes='10'
    elif(endereco[-3:]) == 'Nov':
        mes='11'
    elif(endereco[-3:]) == 'Dez':
        mes='12'
    else:
        mes=''    
    return mes

def agendamento_dia(endereco):
    endereco=converteDataSiebel8para6(endereco)
    dia=''
    if(endereco[:4])=='Quar':
        primeiras_posicoes=endereco[:7]
    else:   
        primeiras_posicoes=endereco[:6]
    ultima_posicao=primeiras_posicoes[-2:]
    dia='Dia_'+ultima_posicao
    return dia    

def agendamento_mes(endereco):
    mes=''
    endereco=converteDataSiebel8para6(endereco)

    if(endereco[-3:]) == 'Jan':
        mes='Janeiro'
    elif(endereco[-3:]) == 'Fev':
        mes='Fevereiro'
    elif(endereco[-3:]) == 'Mar':
        mes='Marco'
    elif(endereco[-3:]) == 'Abr':
        mes='Abril'
    elif(endereco[-3:]) == 'Mai':
        mes='Maio'
    elif(endereco[-3:]) == 'Jun':
        mes='Junho'
    elif(endereco[-3:]) == 'Jul':
        mes='Julho'
    elif(endereco[-3:]) == 'Ago':
        mes='Agosto'
    elif(endereco[-3:]) == 'Set':
        mes='Setembro'
    elif(endereco[-3:]) == 'Out':
        mes='Outubro'
    elif(endereco[-3:]) == 'Nov':
        mes='Novembro'
    elif(endereco[-3:]) == 'Dez':
        mes='Dezembro'
    else:
        mes=''
    return mes
    
def agendamento_dia_da_semana(endereco):
    diasemana=''
    endereco=converteDataSiebel8para6(endereco)

    if(endereco[:3]) == 'Dom':
        diasemana='Domingo'
    elif(endereco[:3]) == 'Seg':
        diasemana='Segunda_Feira'
    elif(endereco[:3]) == 'Ter':
        diasemana='Terca_Feira'
    elif(endereco[:3]) == 'Qua':
        diasemana='Quarta_Feira'
    elif(endereco[:3]) == 'Qui':
        diasemana='Quinta_Feira'
    elif(endereco[:3]) == 'Sex':
        diasemana='Sexta_Feira'
    elif(endereco[:3]) == 'Sab':
        diasemana='Sabado'
    else:
        diasemana=''
    return diasemana

def agendamento_periodo(endereco):
    periodo=''
    endereco=converteDataSiebel8para6(endereco)

    if(endereco) == '08:00 - 12:00':
        periodo='No_Periodo_Manha'
    elif(endereco) == '13:00 - 18:00':
        periodo='No_Periodo_Tarde'
    elif(endereco) == '18:00 - 22:00':
        periodo='No_Periodo_Noite'
    else:
        periodo=''
    return periodo

def agendamento_date_format(endereco):
    data=''
    mes=''
    dia=''
    endereco=converteDataSiebel8para6(endereco)

    #Pega o Mes nro
    if(endereco[-3:]) == 'Jan':
        mes='01'
    elif(endereco[-3:]) == 'Fev':
        mes='02'
    elif(endereco[-3:]) == 'Mar':
        mes='03'
    elif(endereco[-3:]) == 'Abr':
        mes='04'
    elif(endereco[-3:]) == 'Mai':
        mes='05'
    elif(endereco[-3:]) == 'Jun':
        mes='06'
    elif(endereco[-3:]) == 'Jul':
        mes='07'
    elif(endereco[-3:]) == 'Ago':
        mes='08'
    elif(endereco[-3:]) == 'Set':
        mes='09'
    elif(endereco[-3:]) == 'Out':
        mes='10'
    elif(endereco[-3:]) == 'Nov':
        mes='11'
    elif(endereco[-3:]) == 'Dez':
        mes='12'
    else:
        mes=''
    
    #Pega o dia
    if(endereco[:4])=='Quar':
        primeiras_posicoes=endereco[:7]
    else:   
        primeiras_posicoes=endereco[:6]
    #primeiras_posicoes=endereco[:6]
    ultima_posicao=primeiras_posicoes[-2:]
    dia=ultima_posicao
    
    #Pega o ano
    date = datetime.now()
    data=dia+mes+str(date.year)

    return data

def agendamento_expressao(endereco):
    #Sab 10/Jul
    expressao   = ''
    endereco=converteDataSiebel8para6(endereco)

    #Pega o dia
    if(endereco[:4])=='Quar':
        primeiras_posicoes=endereco[:7]
    else:
        primeiras_posicoes=endereco[:6]
    ultima_posicao=primeiras_posicoes[-2:]

    #monta data, hoje, amanha, depois amanha e proxima semana
    data_hoje = datetime.now()
    hoje=data_hoje.day
    amanha=data_hoje.day+1
    depois_amanha=data_hoje.day+2
    numero_do_dia_da_semana_atual = data_hoje.isocalendar()[1] #.isoweekday()
    mes=mes_em_numero(endereco[-3:])
    dia=ultima_posicao
    ano=str(data_hoje.year)

    str_date=ano+'-'+mes+'-'+dia
    data_agendamento=datetime.strptime(str_date, '%Y-%m-%d').date()  
    proxima_semana = data_agendamento.isocalendar()[1] #.isoweekday()

    if(amanha == int(ultima_posicao)):
        expressao='Amanha'
    elif(depois_amanha) == int(ultima_posicao):
        expressao='Depois_de_Amanha'
    elif(numero_do_dia_da_semana_atual) < proxima_semana:
        expressao='Proxima_Semana'
    elif(hoje) == dia:
        expressao='Hoje'
    elif(int(ultima_posicao)) < hoje:
         expressao='Ontem'
    # elif(amanha) < depois_amanha:
    #      expressao='Depois_Amanha'
    else:
        expressao=''

    return expressao        


def le_pedido(dir,pcpf,parquivo,ppasta):  
    try:        
        pedido='Erro'      
        sistema=''
        registros=pd.read_csv(os.path.join(dir+'\\00Entrada\\',parquivo),sep=';')               
        resultado='Erro CPF Nao Encontrado'
        for index, linhas in registros.iterrows():        
            cpf     = CPFTrata(registros.at[index,'COD_CPF_CGC'])
            pcpf    = CPFTrata(pcpf)        
            if cpf==pcpf:
                pedido   = registros.at[index,'CAMPOX1']
                sistema  = registros.at[index,'COD_SISTEMA']
                break
    except Exception as e:        
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname=os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        resultado="Erro {} {} {}".format(exc_type,fname,exc_tb.tb_lineno)                 
    return pedido,sistema 

def hoje_anomedia():
    ano            = str(date.today().year)
    mes            = '00'+str(date.today().month)
    dia            = '00'+str(date.today().day)
    data_atual     = ano+mes[-2:]+dia[-2:]
    return data_atual

def converteDataSiebel8para6(endereco):
    dia_semana  = ['Seg','Ter','Quar','Qui','Sex','Sab','Dom']
    mes_nome    = ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez']
    str_date    = endereco
    if str_date[2:3]=='/':  
        data                   = datetime.strptime(str_date, '%d/%m/%Y').date()
        nr_mes                 = int(str_date[3:5])-1
        nrdia_semana           = data.weekday()
        dia                    = '0'+str(data.day)
        dia                    = dia[-2:3]
        endereco=dia_semana[nrdia_semana]+' '+dia+'/'+mes_nome[nr_mes]
    return(endereco)

def hora_seg(tempo):
    hora=int(tempo[0:2])*3600
    min =int(tempo[3:5])*60
    seg =int(tempo[6:8])
    return hora+min+seg

def monta_chave(evento,LeadNumber,fonte,tarefa,status):
    if evento=='Agente Disponivel':# or 'Pedido Monitor PA' or 'Pedido Rertorno dde Monitor PA':
        tipo_evento=0        
        chaves=fonte+'|'+tarefa+'|'+status
    else:
        chaves=LeadNumber+'|'+tarefa
        tipo_evento=1
		
    return chaves,tipo_evento

def msg_ignorar(linha):
    msg_ignorar=['disconnected','connected','InicioProcesso','--','==','Rodando','monitorpa']
    achou=False
    for palavra in msg_ignorar:
        if linha.find(palavra)>0:
            achou=True
    return achou

def msg_busca_orquestrador_enviado(linha,LeadNumber,Chave,ano,mes,dia,hora,evento):
        try:
            saida='NOK'
            if linha.find('Enviado')>0:
                recebido    = linha[34:20000]           
                recebido    = eval(recebido)
                if   evento=='Agente Disponivel' and linha.find('agente_disponivel')>0:
                    if linha.find('data')>0:
                        if recebido['data']['status']=='PA Errada':
                            saida='PA Errada'
                    else:
                        if recebido['content']['status']=='PA Errada':
                            saida='PA Errada'
                        else:
                            saida='OK'

                elif evento=='Pedido de Agenda':
                    saida=recebido['result']
                elif evento=='Pedido Retorno da Agenda':
                    saida=recebido['result']
                elif evento=='Pedido de Marcacao':
                    saida=recebido['result']    
                elif evento=='Pedido Rertorno de Marcacao':
                    saida=recebido['result']  
                elif evento=='Extracao de Leads':
                    saida=recebido['result']  
            return saida
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname=os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            resultado="Erro {} {} {}".format(exc_type,fname,exc_tb.tb_lineno)         
            ##print(resultado)  
            return resultado

def msg_busca_orquestrador_recebido(linha,ano,mes,dia,hora):
        try:
            if linha.find('Recebido')>0:
                recebido    = linha[34:20000]   
                recebido    = eval(recebido.replace('"','\''))     
                resultado   = 'OK'
                Fonte       = 'Orquestrador'
                if     recebido['data']['acao']=='agente_disponivel':
                    Evento      = 'Agente Disponivel'
                    Tarefa      = 'Agente Disponivel'
                elif   recebido['data']['acao']=='extracao_leads':
                        Evento      = 'Extracao de Leads'  
                        Tarefa      = 'Extracao de Leads'        
                elif   recebido['data']['acao']=='agenda':
                    if recebido['data']['status']=='fila':
                        Evento      = 'Pedido de Agenda'  
                        Tarefa      = 'Agenda'
                    else:
                        Evento      = 'Pedido Retorno da Agenda'  
                        Tarefa      = 'Agenda'                        
                elif   recebido['data']['acao']=='marcacao':
                    if recebido['data']['status']=='fila':
                        Evento      = 'Pedido de Marcacao'       
                        Tarefa      = 'Marcacao'                               
                    else:
                        Evento      = 'Pedido Rertorno de Marcacao'  
                        Tarefa      = 'Marcacao'  
                Sistema     = recebido['data']['onde']
					
                if Evento      == 'Agente Disponivel':
                    LeadNumber  = 'X'
                    Chave       = 'X'
                else:
                    LeadNumber  = recebido['data']['lead_number']            
                    Chave       = recebido['data']['arquivo']
                DataInicio  = ano+mes+dia
                HoraInicio  = hora
                Status      = recebido['data']['status']
                acao        = recebido['data']['acao']
                status      = recebido['data']['status']        
            else:
                resultado   = 'NOK'
                Fonte       = ''
                Tarefa      = ''
                Evento      = ''
                Sistema     = ''
                LeadNumber  = ''
                DataInicio  = ''
                HoraInicio  = ''
                Status      = ''
                acao        = ''
                status      = ''
                Chave       = ''
            return resultado,Fonte,Tarefa,Evento,Sistema,Chave,LeadNumber,DataInicio,HoraInicio,Status
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname=os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            resultado="Erro msg_busca_orquestrador_recebido {} {} {}".format(exc_type,fname,exc_tb.tb_lineno)             


def TrataEnd_CriaAbrev():
    tipoend   = [["10AR","Décima Rua"],
                ["10ATV","Décima Travessa"],
                ["11AR","Décima Primeira Rua"],
                ["11ATV","Décima Primeira Travessa"],
                ["12AR","Décima Segunda Rua"],
                ["12ATV","Décima Segunda Travessa"],
                ["13ATV","Décima Terceira Travessa"],
                ["14ATV","Décima Quarta Travessa"],
                ["15ATV","Décima Quinta Travessa"],
                ["16ATV","Décima Sexta Travessa"],
                ["1APAR","Primeira Paralela"],
                ["1AR","Primeira Rua"],
                ["1ASUB","Primeira Subida"],
                ["1AT","Primeira Travessa"],
                ["1AVL","Primeira Vila"],
                ["1OAT","Primeiro Alto"],
                ["1OBC","Primeiro Beco"],
                ["1OPRQ","Primeiro Parque"],
                ["2APAR","Segunda Paralela"],
                ["2AR","Segunda Rua"],
                ["2ASUB","Segunda Subida"],
                ["2ATV","Segunda Travessa"],
                ["2AVL","Segunda Vila"],
                ["2OAT","Segundo Alto"],
                ["2OBC","Segundo Beco"],
                ["2OPRQ","Segundo Parque"],
                ["3APAR","Terceira Paralela"],
                ["3AR","Terceira Rua"],
                ["3ASUB","Terceira Subida"],
                ["3ATV","Terceira Travessa"],
                ["3AVL","Terceira Vila"],
                ["3OAT","Terceiro Alto"],
                ["3OBC","Terceiro Beco"],
                ["3OPRQ","Terceiro Parque"],
                ["4APAR","Quarta Paralela"],
                ["4AR","Quarta Rua"],
                ["4ASUB","Quarta Subida"],
                ["4ATV","Quarta Travessa"],
                ["4AVL","Quarta Vila"],
                ["4OAT","Quarto Alto"],
                ["4OBC","Quarto Beco"],
                ["5APAR","Quinta Paralela"],
                ["5AR","Quinta Rua"],
                ["5ASUB","Quinta Subida"],
                ["5ATV","Quinta Travessa"],
                ["5AVL","Quinta Vila"],
                ["5OAT","Quinto Alto"],
                ["5OBC","Quinto Beco"],
                ["6AR","Sexta Rua"],
                ["6ASUB","Sexta Subida"],
                ["6ATV","Sexta Travessa"],
                ["7AR","Sétima Rua"],
                ["7ATV","Sétima Travessa"],
                ["8AR","Oitava Rua"],
                ["8ATV","Oitava Travessa"],
                ["9AR","Nona Rua"],
                ["9ATV","Nona Travessa"],
                ["ACAMP","Acampamento"],
                ["AC","Acesso"],
                ["ACS","Acesso"],
                ["AD","Adro"],
                ["AER","Aeroporto"],
                ["AE","Area Especial"],
                ["AL","Alameda"],
                ["ALF","Alfredo"],
                ["ALM","Almirante"],
                ["AT","Alto"],
                ["AP","Apartamento"],
                ["APTO","Apartamento"],
                ["A","Área"],
                ["AE","Área Especial"],
                ["ARQ","Arquiteto"],
                ["ART","Artéria"],
                ["ATL","Atalho"],
                ["AV","Avenida"],
                ["AV-CONT","Avenida Contorno"],
                ["BX","Baixa"],
                ["BLO","Balão"],
                ["BAL","Balneario"],
                ["BAR","Barao"],
                ["BC","Beco"],
                ["BELV","Belvedere"],
                ["BL","Bloco"],
                ["BSQ","Bosque"],
                ["BVD","Boulevard"],
                ["BRIG","Brigadeiro"],
                ["BCO","Buraco"],
                ["C","Cais"],
                ["CALC","Calçada"],
                ["CAM","Caminho"],
                ["CPO","Campo"],
                ["CAN","Canal"],
                ["CAP","Capitao"],
                ["CARD","Cardeal"],
                ["CA","Casa"],
                ["CAS","Casa"],
                ["CS","Casa"],
                ["CH","Chácara"],
                ["CHAP","Chapadão"],
                ["CIRC","Circular"],
                ["COL","Colônia"],
                ["CMDT","Comandante"],
                ["COMEND","Comendador"],
                ["CMP-VR","Complexo Viário"],
                ["COND","Condomínio"],
                ["CDO","Condomínio"],
                ["CJ","Conjunto"],
                ["CONJ","Conjunto"],
                ["CONS","Conselheiro"],
                ["CEL","Coronel"],
                ["COR","Corredor"],
                ["CRG","Córrego"],
                ["DEP","Deputado"],
                ["DSC","Descida"],
                ["DSV","Desvio"],
                ["DT","Distrito"],
                ["DR","Doutor"],
                ["EVD","Elevada"],
                ["EMB","Embaixador"],
                ["ENG","Engenheiro"],
                ["ENT-PART","Entrada Particular"],
                ["EQ","Entre-quadra"],
                ["ESC","Escada"],
                ["ESD","Escada"],
                ["ESP","Esplanada"],
                ["ETC","Estação"],
                ["ESTC","Estacionamento"],
                ["ETD","Estádio"],
                ["ETN","Estância"],
                ["EST","Estrada"],
                ["ESTR","Estrada"],
                ["EST-MUN","Estrada Municipal"],
                ["EXP","Expedicionario"],
                ["FAV","Favela"],
                ["FAZ","Fazenda"],
                ["FRA","Feira"],
                ["FER","Ferrovia"],
                ["FNT","Fonte"],
                ["FTE","Forte"],
                ["FR","Frei"],
                ["FT","Frente"],
                ["FD","Fundos"],
                ["FDS","Fundos"],
                ["FU","Fundos"],
                ["FUN","Fundos"],
                ["GAL","Galeria"],
                ["GA","Galeria"],
                ["GEN","General"],
                ["GOV","Governador"],
                ["GJA","Granja"],
                ["HAB","Habitacional"],
                ["IA","Ilha"],
                ["JD","Jardim"],
                ["JDE","Jardinete"],
                ["LD","Ladeira"],
                ["LAD","Ladeira"],
                ["LG","Lago"],
                ["LGA","Lagoa"],
                ["LRG","Largo"],
                ["LJ","Loja"],
                ["LT","Lote"],
                ["LOT","Loteamento"],
                ["LOTM","Loteamento"],
                ["LTM","Loteamento"],
                ["MAJ","Major"],
                ["MAL","Marechal"],
                ["MNA","Marina"],
                ["ME","MESTRE"],
                ["MOD","Módulo"],
                ["MTE","Monte"],
                ["MRO","Morro"],
                ["NUC","Núcleo"],
                ["NR","Numero"],
                ["PE","Padre"],
                ["PCA","Praca"],
                ["PDA","Parada"],
                ["PD","Parada"],                
                ["PDO","Paradouro"],
                ["PAR","Paralela"],
                ["PQ","Parque"],
                ["PQE","Parque"],
                ["PRQ","Parque"],
                ["PSG","Passagem"],
                ["PSG-SUB","Passagem Subterrânea"],
                ["PSA","Passarela"],
                ["PAS","Passeio"],
                ["PAT","Pátio"],
                ["PNT","Ponta"],
                ["PTE","Ponte"],
                ["PTO","Porto"],
                ["PÇ","Praça"],
                ["PÇ-ESP","Praça de Esportes"],
                ["PR","Praia"],
                ["PREF","Prefeito"],
                ["PRES","Presidente"],
                ["1A","Primeira"],
                ["PROF","Professor"],
                ["PROFA","Professora"],
                ["PROJ","Projetada"],
                ["PRL","Prolongamento"],
                ["Q","Quadra"],
                ["QD","Quadra"],
                ["QU","Quadra"],
                ["4A","Quarta"],
                ["5A","Quinta"],
                ["QTA","Quinta"],
                ["QTAS","Quintas"],
                ["RAM","Ramal"],
                ["RMP","Rampa"],
                ["RM","Rampa"],
                ["REC","Recanto"],
                ["RES","Residencial"],
                ["RET","Reta"],
                ["RER","Retiro"],
                ["RTN","Retorno"],
                ["ROD-AN","Rodo Anel"],
                ["ROD","Rodovia"],
                ["RTT","Rotatória"],
                ["ROT","Rotula"],
                ["R","Rua"],
                ["R-LIG","Rua de Ligação"],
                ["R-PED","Rua de Pedestre"],
                ["RUA","Rua"],
                ["RUAFR","Rua Frei"],
                ["SL","Sala"],
                ["STA","Santa"],
                ["STO","Santo"],
                ["S","Sao"],
                ["SARG","Sargento"],
                ["SRG","Sargento"],
                ["2A","Segunda"],
                ["SN","Sem Número"],
                ["SEN","Senador"],
                ["SR","Senhor"],
                ["SRA","Senhora"],
                ["SRV","Servidão"],
                ["SET","Setor"],
                ["ST","Setor"],
                ["SIT","Sítio"],
                ["SB","Sobrado"],
                ["SLJ","Sobreloja"],
                ["SD","Soldado"],
                ["SUB","Subida"],
                ["TEN","Tenente"],
                ["3A","Terceira"],
                ["TER","Terminal"],
                ["TE","Terreo"],
                ["TRANSV","Transversal"],
                ["TV","Travessa"],
                ["TV-PART","Travessa Particular"],
                ["TR","Trecho"],
                ["TRV","Trevo"],
                ["TCH","Trincheira"],
                ["TUN","Túnel"],
                ["UNID","Unidade"],
                ["VAL","Vala"],
                ["VLE","Vale"],
                ["VRTE","Variante"],
                ["VER","Vereador"],
                ["V","Via"],
                ["V-AC","Via de Acesso"],
                ["V-PED","Via de pedestre"],
                ["VD","Viaduto"],
                ["V-EVD","Via Elevada"],
                ["V-EXP","Via Expressa"],
                ["VLA","Viela"],
                ["VL","Vila"],
                ["VIA","Via"],
                ["VISC","Visconde"],
                ["VSC","Visconde"],
                ["VV","Vivenda"],
                ["ZIG-ZAG","Zigue-zague"],
                ["ZN","Zona"]]
    coluna=['abreviacao','descritivo']
    dados=pd.DataFrame(tipoend,columns=coluna)  
    return dados
 
def TrataEnd_LocalizaAbrev(dados,abrev):
    resultado='Nao Achou'
    res      ='Nao Achou'
    filtro    = (dados.abreviacao==abrev)
    saida=dados.loc[filtro]
    for index, linhas in saida.iterrows():       
        resultado=saida.at[index,'descritivo']  
        res='Ok'
    if res=='Nao Achou':
        resultado=abrev        
    return resultado,res

def TrataEnd_CaractEspeciais(Endereco):
    if len(Endereco)==0:
        res='Erro01'
        resultado=''
    else:
        res='Ok'
        # Tratando 0 Solto / ou \
        resultado=Endereco.replace('/',' ').replace(' \ ',' ')
        # Tratando Espaço Duplo e .0
        resultado=resultado.replace('  ',' ')
        resultado=resultado.replace('.0','')
    return resultado,res

def TrataEnd_Decupa(endereco):
    # Contabiliza Quantos - Tem para identificar se temos Complemento
    endereco_limpo=endereco[endereco.find(','):200]
    qtddivisores=endereco_limpo.count('-')      
    estrutura={"tipo"   :"",
               "logr"   :"",
               "num"    :"",               
               "compl"  :"",               
               "cep"    :"",                
               "bairro" :"", 
               "cidade" :"",                              
               "estado" :""                                             
              }
    decupado=endereco.split(' ')
    qtdcampos=len(decupado)
    posfimdec=qtdcampos-1
    # Atribui o Tipo
    estrutura['tipo']   =decupado[0].upper()
    # Atribui o Numero
    posfimlogr=0
    i=0
    pallimpa=''
    for linha in decupado:
        if linha.find(',')>0 and posfimlogr==0:
            posfimlogr=i
            posnumero=posfimlogr+1
            estrutura['num']=decupado[posnumero].upper()
            pallimpa=decupado[posfimlogr].replace(',','')
        i=i+1 
    logr=''
    # Atribui o Logradouro
    for i in range(1,posfimlogr):
        if i==1:
            logr=decupado[i]
        else:
            logr=logr+' '+decupado[i]
    estrutura['logr']=logr+' '+pallimpa  
    estrutura['logr']=estrutura['logr'].strip().upper()
        
    # Atribui CEP
    posicaocep=0
    for i in range(len(decupado)-1,0,-1):
        if decupado[i].isdigit() and posicaocep==0:
            posicaocep=i    
    estrutura['cep']    =decupado[posicaocep].upper()      
    
    # Atribui Cidade
    qtdpalavras=0
    for i in range(posicaocep+1,posfimdec-1):
        if qtdpalavras==0:
            estrutura['cidade'] =decupado[i].upper()
        else:
            estrutura['cidade'] =estrutura['cidade']+' '+decupado[i].upper()
        qtdpalavras=qtdpalavras+1
    
    # Atribui o Estado
    estrutura['estado'] =decupado[posfimdec].upper()
    
    # Atribui Bairro
    if qtddivisores==2:
        qtdpalavaras=0
        for i in range(posnumero+2,posicaocep):            
            if qtdpalavras==0:
                estrutura['bairro']=decupado[i].upper()
            else:
                estrutura['bairro']=estrutura['bairro']+' '+decupado[i].upper()
        estrutura['compl']=''
    elif qtddivisores==3:
        poscontrole =0
        qtdpalcomp  =0
        qtdpalbairro=0
        for i in range(0,posfimdec):
            if decupado[i].find(',') and poscontrole==0:
                poscontrole=1
            else:
                if poscontrole==1 and decupado[i]=='-':
                    poscontrole=2
                else:
                    if poscontrole==2:
                        if decupado[i]!='-':
                            if qtdpalcomp==0:
                                estrutura['compl']=decupado[i].upper()
                            else:
                                estrutura['compl']=estrutura['compl']+' '+decupado[i].upper()
                            qtdpalcomp=qtdpalcomp+1
                        else:
                            poscontrole=3
                    else:
                        if poscontrole==3:
                            if decupado[i].isdigit()==False:
                                if qtdpalbairro==0:
                                    estrutura['bairro']=decupado[i].upper()
                                else:
                                    estrutura['bairro']=estrutura['bairro']+' '+decupado[i].upper()
                                qtdpalbairro=qtdpalbairro+1       
                            else:
                                poscontrole=4     

    return estrutura

def TrataEnd_Estado(uf):
    estado=uf
    if(uf=='AC'):
        estado='Acre'
    if(uf=='AP'):
        estado='Amapa'
    if(uf=='AM'):
        estado='Amazonas'
    if(uf=='PA'):
        estado='Para'
    if(uf=='RO'):
        estado='Rondonia'
    if(uf=='RR'):
        estado='Roraima'
    if(uf=='TO'):
        estado='Tocantis'
    if(uf=='AL'):
        estado='Alagoas'
    if(uf=='BA'):
        estado='Bahia'
    if(uf=='CE'):
        estado='Ceara'
    if(uf=='MA'):
        estado='Maranhao'
    if(uf=='PB'):
        estado='Paraiba'
    if(uf=='PI'):
        estado='Piaui'
    if(uf=='PE'):
        estado='Pernambuco'
    if(uf=='RN'):
        estado='Rio Grande do Norte'
    if(uf=='SE'):
        estado='Sergipe'
    if(uf=='DF'):
        estado='Distrito Federal'
    if(uf=='GO'):
        estado='Goias'
    if(uf=='MT'):
        estado='Mato Grosso'
    if(uf=='MS'):
        estado='Mato Grosso do Sul'
    if(uf=='ES'):
        estado='Espirito Santo'
    if(uf=='MG'):
        estado='Minas Gerais'
    if(uf=='RJ'):
        estado='Rio de Janeiro'
    if(uf=='SP'):
        estado='Sao Paulo'
    if(uf=='PR'):
        estado='Parana'
    if(uf=='RS'):
        estado='Rio Grande do Sul'
    if(uf=='SC'):
        estado='Santa Catarina'   
    if estado==uf:
        res='Erro' 
    else:
        res='Ok'
    return estado,res
    