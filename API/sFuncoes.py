import psycopg2
import requests
import json
import pandas as pd



# Função para conectar com o banco de dados
def conecta_bd():
    con = psycopg2.connect(
        user='postgres',
        password='1234mi',
        host='localhost',
        port='5432',
        database='Wizard')
    return con


# Função de inserir no banco de dados
def inserir_bd(sql):
    con = conecta_bd()
    cur = con.cursor()
    try:
        cur.execute(sql)
        con.commit()
        return 0
    except (Exception, psycopg2.DatabaseError) as error:
        print("Error: %s" % error)
        con.rollback()
        cur.close()
        return 1
    cur.close()


# Função de deletar no banco de dados
def deletar_bd(sql):
    con = conecta_bd()
    cur = con.cursor()
    try:
        cur.execute(sql)
        con.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print("Error: %s" % error)
        con.rollback()
        cur.close()
        return 1
    cur.close()


# Função de editar/update no banco de dados
def editar_bd(sql):
    con = conecta_bd()
    cur = con.cursor()
    try:
        cur.execute(sql)
        con.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print("Error: %s" % error)
        con.rollback()
        cur.close()
        return 1
    cur.close()


def truncate(table):
    con = conecta_bd()
    cur = con.cursor()
    try:
        cur.execute("TRUNCATE TABLE %s" % table)
        con.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print("Error: %s" % error)
        con.rollback()
        cur.close()
        return 1
    cur.close()


# Função para consultar no banco de dados
def consulta_bd(sql):
    con = conecta_bd()
    cur = con.cursor()
    cur.execute(sql)
    recset = cur.fetchall()
    registros = []
    for rec in recset:
        registros.append(rec)
    con.close()
    return registros


# Função criar no banco de dados
def criar_db(sql):
  con = conecta_bd()
  cur = con.cursor()
  cur.execute(sql)
  con.commit()
  con.close()


# Função para consultar no banco de dados
def consulta_bd(sql):
    con = conecta_bd()
    cur = con.cursor()
    cur.execute(sql)
    recset = cur.fetchall()
    registros = []
    for rec in recset:
        registros.append(rec)
    con.close()
    return registros




##########################################################################################

