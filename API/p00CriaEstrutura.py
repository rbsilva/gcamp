import sFuncoes as f
import datetime

data = datetime.datetime.now()



# Dropando a tabela caso ela já exista
sql = 'DROP TABLE IF EXISTS public.usuario'
f.criar_db(sql)


# Dropando a tabela caso ela já exista
sql = 'DROP TABLE IF EXISTS public.grupo'
f.criar_db(sql)

# Dropando a tabela caso ela já exista
sql = 'DROP TABLE IF EXISTS public.cliente'
f.criar_db(sql)

# Dropando a tabela caso ela já exista
sql = 'DROP TABLE IF EXISTS public.tipos_autorizacao'
f.criar_db(sql)


# Dropando a tabela caso ela já exista
sql = 'DROP TABLE IF EXISTS public.controle_operacao'
f.criar_db(sql)



# Criando a tabela de Controle de Operação
sql = '''
CREATE SEQUENCE ctop_codtransacao_seq;
    
CREATE TABLE public.controle_operacao 
  ( 
	CodTransacao      integer PRIMARY KEY NOT NULL DEFAULT nextval('ctop_codtransacao_seq'),
	TipoOperacao      character varying(20) not null,
	TimeStamp         timestamp without time zone not null default now(),
	DataInicio        character varying(20) not null,
    DataFim           character varying(20) not null
  )

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.controle_operacao
    OWNER to postgres;

ALTER SEQUENCE ctop_codtransacao_seq
OWNED BY controle_operacao.codtransacao;'''
f.criar_db(sql)



# Criando a tabela de Tipos de Autorização
sql = '''
CREATE SEQUENCE tpaut_codautorizacao_seq;
    
CREATE TABLE public.tipos_autorizacao 
  ( 
	CodAutorizacao    integer PRIMARY KEY NOT NULL DEFAULT nextval('tpaut_codautorizacao_seq'),
	CodTransacao      integer not null,
    DsAutorizacao     character varying(100) not null,
    FOREIGN KEY (CodTransacao) references controle_operacao (CodTransacao)
  )

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.tipos_autorizacao
    OWNER to postgres;

ALTER SEQUENCE tpaut_codautorizacao_seq
OWNED BY tipos_autorizacao.codautorizacao;'''

f.criar_db(sql)



# Criando a tabela de Cliente
sql = '''CREATE TABLE public.cliente 
      (  
        CNPJ                 character varying(20) PRIMARY KEY,
        CodTransacao         integer not null,
        RazaoSocial          character varying(100) not null, 
        Responsavel          character varying(50) not null, 
        Telefone             character varying(15) not null, 
        Email                character varying(50) not null, 
        DataInicio           character varying(20) not null, 
        DataFim              character varying(20) not null,
        FOREIGN KEY (CodTransacao) references controle_operacao (CodTransacao)
      )'''
f.criar_db(sql)




# Criando a tabela de Grupo
sql = '''
    CREATE SEQUENCE grupo_codgrupo_seq;
    
    CREATE TABLE public.grupo 
      ( 
        CodGrupo              integer PRIMARY KEY NOT NULL DEFAULT nextval('grupo_codgrupo_seq'),
        CNPJ                  character varying(20) not null, 
        CodPermissao          integer not null, 
        CodTpUsuario          integer not null,
        CodTransacao          integer not null,
        DsGrupo               character varying(100) not null,
        FOREIGN KEY (CNPJ) references cliente (CNPJ),
        FOREIGN KEY (CodPermissao) references tipos_autorizacao (CodAutorizacao),
        FOREIGN KEY (CodTransacao) references controle_operacao (CodTransacao)
      )

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.grupo
    OWNER to postgres;

ALTER SEQUENCE grupo_codgrupo_seq
OWNED BY grupo.codgrupo;'''
f.criar_db(sql)



# Criando a tabela de Usuario
sql = '''CREATE TABLE public.usuario
      ( CPF                   character varying(20) PRIMARY KEY, 
        CNPJ                  character varying(20) not null, 
        CodGrupo              integer not null, 
        CodTransacao          integer not null,
        Nome                  character varying(20) not null, 
        Email                 character varying(30) not null, 
        Senha                 character varying(30) not null,
        FOREIGN KEY (CNPJ) references cliente (CNPJ),
        FOREIGN KEY (CodGrupo) references grupo (CodGrupo),
        FOREIGN KEY (CodTransacao) references controle_operacao (CodTransacao)
      )'''
f.criar_db(sql)

#############################################################################
