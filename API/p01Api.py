import sFuncoes as f
import pandas as pd
from flask import Flask, jsonify, request
from datetime import datetime
import smtplib
from datetime import datetime
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email import encoders
from grpc import server
import json

app = Flask(__name__)


data_hoje_banco = datetime.now().strftime('%Y%m%d')
data_hoje_normal = datetime.now().strftime('%d/%m/%Y')

def user(lista):
    df = pd.DataFrame(lista, columns=["cpf", "cnpj","codgrupo","codtransacao","nome","email"])
    js = df.to_json(orient='records')
    js = js.replace('[', '').replace(']', '')
    if lista == []:
        json = {
            "success": "false",
            "message": "CPF ou senha nao coincidem"
        }
    else:
        json = {
            "body": f"{js}",
            "success": "true",
            "message": "Usuario autenticado com sucesso"
        }
        print (js)
    return json


def entrar(cpf, password):
    SQL_LOGIN = f"select cpf, cnpj, codgrupo, codtransacao, nome, email from usuario where cpf = '{cpf}' and senha = '{password}'"
    resultado = f.consulta_bd(SQL_LOGIN)
    return user(resultado)



################################### Usuário #################################################

@app.route('/login', methods=['POST'])
def login():
    json = request.get_json()
    cpf = json['cpf']
    password = json['password']
    acesso = entrar(cpf, password)
    return acesso



@app.route('/registerUser', methods=['POST'])
def registerUser():
    json = request.get_json()
    dsautorizacao = json['dsautorizacao']
    SQL_INSERCAO = f"INSERT INTO public.controle_operacao(tipooperacao, datainicio, datafim) VALUES ('incluir', '{data_hoje_normal}', '{data_hoje_normal}')"
    f.inserir_bd(SQL_INSERCAO)
    SQL_COD = f"select codtransacao from controle_operacao order by codtransacao desc limit 1"
    consulta = f.consulta_bd(SQL_COD)
    df = pd.DataFrame(consulta, columns=["codtransacao"])
    codtransacao = df['codtransacao'][0]
    if codtransacao != [] or codtransacao != None or codtransacao != '':
        cpf = json['cpf']
        cnpj = json['cnpj']
        codgrupo = json['codgrupo']
        nome = json['nome']
        email = json['email']
        SQL_CONSULTA_CPF = f"select cpf from usuario where cpf = '{cpf}'"
        consulta = f.consulta_bd(SQL_CONSULTA_CPF)
        if consulta == []:
            SQL_INSERE = f"insert into usuario (cpf, cnpj, codgrupo, codtransacao, nome, email, senha) values ('{cpf}', '{cnpj}', '{codgrupo}', '{codtransacao}', '{nome}', '{email}', '{senha}')"
            f.inserir_bd(SQL_INSERE)
            return {
                "success": "true",
                "message": f"Usuario {nome} cadastrado com sucesso"
            }
        else:
            return {
                    "success": "false",
                    "message": "CPF já cadastrado"
                }



@app.route('/forgetPassword', methods=['POST'])
def forgetPassword():
    json = request.get_json()
    cpf = json['cpf']
    SQL_CPF = f"select cpf from usuario where cpf = '{cpf}'"
    retorno = f.consulta_bd(SQL_CPF)

    if retorno == [] or retorno == None or retorno == '':
        return {
            "success": "false",
            "message": "O CPF informado não se encontra cadastrado"
        }
    else:
        df_retorno = pd.DataFrame(retorno, columns=["cpf"])
        retorno = df_retorno['cpf'][0]

        SQL_Email = f"select email from usuario where cpf = '{cpf}'"
        resEmail = f.consulta_bd(SQL_Email)
        df2_retorno = pd.DataFrame(resEmail, columns=["email"])
        resEmail = df2_retorno['email'][0]

        SQL_REDEFINE = f"update usuario set senha = 'a5solutions' where cpf = '{cpf}'"
        update = f.editar_bd(SQL_REDEFINE)
        SQL_SENHA = f"select senha from usuario where cpf = '{cpf}'"
        resultado = f.consulta_bd(SQL_SENHA)

        corpo_email = f"""
                Olá!
        
                Segue abaixo a sua senha de acesso Wizard:
        
                a5solutions
        
        
                 Rio de Janeiro, {data_hoje_normal}
        
                Atenciosamente,
                A5 Solutions
                """
        msg = MIMEMultipart()  # Cria o objeto da mensagem, cria o e-mail
        msg['From'] = 'mpimentel@a5solutions.com'
        msg['To'] = f"{resEmail}"  # inserir e-mail do usuário
        msg['Subject'] = "Recuperação de Senha - a5 Digital "
        msg.attach(MIMEText(corpo_email))

        password = 'MP0407!@#$'  # Senha do e-mail "From"
        server = smtplib.SMTP('smtp.gmail.com: 587')  # Faz a conexão com o seu e-mail de forma segura
        server.starttls()  # Cria um grau de segurança com a conexão
        server.login(msg['From'], password)  # Faz o login

        server.sendmail(msg['From'], [msg['To']], msg.as_string())  # Envia a menssagem
        print('Email enviado com sucesso!')

        server.quit()
    return {
        "body": f"{retorno}, {resEmail}",
        "success": "true",
        "message": f"Senha alterada e e-mail de recuperação de senha enviado."
    }


@app.route('/updatePassword', methods=['POST'])
def updatePassword():
    json = request.get_json()
    dsautorizacao = json['dsautorizacao']
    SQL_INSERCAO = f"INSERT INTO public.controle_operacao(tipooperacao, datainicio, datafim) VALUES ('alterar', '{data_hoje_normal}', '{data_hoje_normal}')"
    f.inserir_bd(SQL_INSERCAO)
    SQL_COD = f"select codtransacao from controle_operacao order by codtransacao desc limit 1"
    consulta = f.consulta_bd(SQL_COD)
    df = pd.DataFrame(consulta, columns=["codtransacao"])
    codtransacao = df['codtransacao'][0]
    if codtransacao != [] or codtransacao != None or codtransacao != '':
        cpf = json['cpf']
        password = json['password']
        SQL_SENHA = f"select senha from usuario where senha = '{password}' and cpf = '{cpf}'"
        resultado = f.consulta_bd(SQL_SENHA)
        if resultado == 'a5solutions':
            return {
                "success": "false",
                "message": "A nova senha não pode ser a mesma da padrão. Por favor, tente outra."
            }
        else:
            SQL_UPDATE = f"update usuario set senha = '{password}' where cpf = '{cpf}'"
            update = f.editar_bd(SQL_UPDATE)
            SQL_CONSULTA = f"select senha from usuario where cpf = '{cpf}'"
            consulta = f.consulta_bd(SQL_CONSULTA)
            return {
                "body": f"{consulta}",
                "success": "true",
                "message": f"Senha alterada com sucesso."
            }



@app.route('/deleteUser', methods=['POST'])
def deleteUser():
    json = request.get_json()
    dsautorizacao = json['dsautorizacao']
    SQL_INSERCAO = f"INSERT INTO public.controle_operacao(tipooperacao, datainicio, datafim) VALUES ('deletar', '{data_hoje_normal}', '{data_hoje_normal}')"
    f.inserir_bd(SQL_INSERCAO)
    SQL_COD = f"select codtransacao from controle_operacao order by codtransacao desc limit 1"
    consulta = f.consulta_bd(SQL_COD)
    df = pd.DataFrame(consulta, columns=["codtransacao"])
    codtransacao = df['codtransacao'][0]
    if codtransacao != [] or codtransacao != None or codtransacao != '':
        cpf = json['cpf']
        SQL_DELETA = f"delete from usuario where cpf = '{cpf}'"
        resultado = f.deletar_bd(SQL_DELETA)
        return {
            "body": f"{cpf}",
            "success": "true",
            "message": f"Usuario '{cpf}' deletado com sucesso'"
        }



@app.route('/updateUser', methods=['POST'])
def updateUser():
    json = request.get_json()
    dsautorizacao = json['dsautorizacao']
    SQL_INSERCAO = f"INSERT INTO public.controle_operacao(tipooperacao, datainicio, datafim) VALUES ('alterar', '{data_hoje_normal}', '{data_hoje_normal}')"
    f.inserir_bd(SQL_INSERCAO)
    SQL_COD = f"select codtransacao from controle_operacao order by codtransacao desc limit 1"
    consulta = f.consulta_bd(SQL_COD)
    df = pd.DataFrame(consulta, columns=["codtransacao"])
    codtransacao = df['codtransacao'][0]
    if codtransacao != [] or codtransacao != None or codtransacao != '':
        cnpj = json['cnpj']
        codgrupo = json['codgrupo']
        nome = json['nome']
        email = json['email']
        cpf = json['cpf']
        SQL_UPDATE = f"update usuario set cnpj = '{cnpj}', codgrupo = '{codgrupo}', nome = '{nome}', email = '{email}'  where cpf = '{cpf}'"
        f.editar_bd(SQL_CONSULTA)
        return {
            "success": "true",
            "message": f"Alteração realizada com sucesso"
        }



################################### Cliente #################################################
@app.route('/selectClient', methods=['POST'])
def selectClient():
    json = request.get_json()
    cnpj = json['cnpj']
    SQL_CNPJ = f"select * from cliente where cnpj = '{cnpj}'"
    consulta = f.consulta_bd(SQL_CNPJ)
    df = pd.DataFrame(consulta, columns=["cnpj", "codtransacao", "razaosocial", "responsavel", "telefone", "email", "datainicio", "datafim"])
    js = df.to_json(orient='records')
    js = js.replace('[', '').replace(']', '').replace('/', '').replace('\\', '')
    return {
        "body": f"{js}",
        "success": "true"
    }




@app.route('/registerClient', methods=['POST'])
def registerClient():
    json = request.get_json()
    dsautorizacao = json['dsautorizacao']
    SQL_INSERCAO = f"INSERT INTO public.controle_operacao(tipooperacao, datainicio, datafim) VALUES ('incluir', '{data_hoje_normal}', '{data_hoje_normal}')"
    f.inserir_bd(SQL_INSERCAO)
    SQL_COD = f"select codtransacao from controle_operacao order by codtransacao desc limit 1"
    consulta = f.consulta_bd(SQL_COD)
    df = pd.DataFrame(consulta, columns=["codtransacao"])
    codtransacao = df['codtransacao'][0]
    if codtransacao != [] or codtransacao != None or codtransacao != '':
        cnpj = json['cnpj']
        codtransacao = json['codtransacao']
        razaosocial = json['razaosocial']
        responsavel = json['responsavel']
        telefone = json['telefone']
        email = json['email']
        datainicio = json['datainicio']
        datafim = json['datafim']
        SQL_CNPJ = f"select * from cliente where cnpj = '{cnpj}'"
        consulta = f.consulta_bd(SQL_CNPJ)
        df = pd.DataFrame(consulta,columns=["cnpj", "codtransacao", "razaosocial", "responsavel", "telefone", "email", "datainicio","datafim"])
        js = df.to_json(orient='records')
        js = js.replace('[', '').replace(']', '').replace('/', '').replace('\\', '')
        if js == [] or js == None or js == '':
            SQL_CRIA = f"insert into cliente (cnpj, codtransacao, razaosocial, responsavel, telefone, email, datainicio, datafim) values ('{cnpj}', '{codtransacao}', '{razaosocial}', '{responsavel}', '{telefone}', '{email}', '{datainicio}', '{datafim}')"
            f.inserir_bd(SQL_CRIA)
            return {
                "body": f"{cnpj}",
                "success": "true",
                "message": f"Cliente cadastrado com sucesso'"
            }
        else:
            return {
                "body": f"{cnpj}",
                "success": "false",
                "message": "O cliente informado já se encontra cadastrado."
            }


@app.route('/deleteClient', methods=['POST'])
def deleteClient():
    json = request.get_json()
    dsautorizacao = json['dsautorizacao']
    SQL_INSERCAO = f"INSERT INTO public.controle_operacao(tipooperacao, datainicio, datafim) VALUES ('deletar', '{data_hoje_normal}', '{data_hoje_normal}')"
    f.inserir_bd(SQL_INSERCAO)
    SQL_COD = f"select codtransacao from controle_operacao order by codtransacao desc limit 1"
    consulta = f.consulta_bd(SQL_COD)
    df = pd.DataFrame(consulta, columns=["codtransacao"])
    codtransacao = df['codtransacao'][0]
    if codtransacao != [] or codtransacao != None or codtransacao != '':
        cnpj = json['cnpj']
        SQL_DELETA = f"delete from cliente where cnpj = '{cnpj}'"
        resultado = f.deletar_bd(SQL_DELETA)
        return {
            "body": f"{cnpj}",
            "success": "true",
            "message": f"Cliente '{cnpj}' deletado com sucesso'"
        }


@app.route('/updateClient', methods=['POST'])
def updateClient():
    json = request.get_json()
    dsautorizacao = json['dsautorizacao']
    SQL_INSERCAO = f"INSERT INTO public.controle_operacao(tipooperacao, datainicio, datafim) VALUES ('alterar', '{data_hoje_normal}', '{data_hoje_normal}')"
    f.inserir_bd(SQL_INSERCAO)
    SQL_COD = f"select codtransacao from controle_operacao order by codtransacao desc limit 1"
    consulta = f.consulta_bd(SQL_COD)
    df = pd.DataFrame(consulta, columns=["codtransacao"])
    codtransacao = df['codtransacao'][0]
    if codtransacao != [] or codtransacao != None or codtransacao != '':
        cnpj = json['cnpj']
        razaosocial = json['razaosocial']
        responsavel = json['responsavel']
        telefone = json['telefone']
        email = json['email']
        datainicio = json['datainicio']
        datafim = json['datafim']
        SQL_UPDATE = f"update cliente set razaosocial = '{razaosocial}', responsavel = '{responsavel}', telefone = '{telefone}',  email = '{email}', datainicio = '{datainicio}', datafim = '{datafim}'  where cnpj = '{cnpj}'"
        resultado = f.editar_bd(SQL_UPDATE)
        return {
            "body": f"{razaosocial}', {responsavel}, {telefone}, {email}, {datainicio}, {datafim},'{cnpj}'",
            "success": "true",
            "message": f"Cliente alterado com sucesso'"
        }


################################### Controle Operacional #################################################

@app.route('/selectOpControl', methods=['POST'])
def selectControlOp():
    json = request.get_json()
    codtransacao = json['codtransacao']
    SQL_OP = f"select * from controle_operacao where codtransacao = '{codtransacao}'"
    operacao = f.consulta_bd(SQL_OP)
    df = pd.DataFrame(operacao,columns=["codtransacao", "tipooperacao", "timestamp", "datainicio", "datafim"])
    js = df.to_json(orient='records')
    js = js.replace('[', '').replace(']', '').replace('/', '').replace('\\', '')
    if js == [] or js == None or js == '':
        return {
            "success": "false",
            "message": "Codigo Trasacao nao cadastrado no sistema. Por favor, tente outro."
        }
    else:
        return {
            "body": f"'{js}'",
            "success": "true"
        }


@app.route('/registerOpControl', methods=['POST'])
def registerControlOp():
    json = request.get_json()
    dsautorizacao = json['dsautorizacao']
    SQL_INSERCAO = f"INSERT INTO public.controle_operacao(tipooperacao, datainicio, datafim) VALUES ('alterar', '{data_hoje_normal}', '{data_hoje_normal}')"
    f.inserir_bd(SQL_INSERCAO)
    SQL_COD = f"select codtransacao from controle_operacao order by codtransacao desc limit 1"
    consulta = f.consulta_bd(SQL_COD)
    df = pd.DataFrame(consulta, columns=["codtransacao"])
    codtransacao = df['codtransacao'][0]
    if codtransacao != [] or codtransacao != None or codtransacao != '':
        tipooperacao = json['tipooperacao']
        timestamp = json['timestamp']
        datainicio = json['datainicio']
        datafim = json['datafim']
        SQL_CRIA = f"insert into controle_operacao (tipooperacao, timestamp, datainicio, datafim) values ('{tipooperacao}', '{timestamp}', '{datainicio}', '{datafim}')"
        f.inserir_bd(SQL_CRIA)
        return {
            "body": f"'{tipooperacao}', '{timestamp}', '{datainicio}', '{datafim}'",
            "success": "true",
            "message": "Codigo da transação cadastrado com sucesso!"
        }


################################### Grupo #################################################

@app.route('/selectGroup', methods=['POST'])   ###  INSERIR CONDIÇÃO QUANDO NÃO TIVER CADASTRADO
def selectGroup():
    json = request.get_json()
    cnpj = json['cnpj']
    SQL_GRUPO = f"select * from grupo"
    grupo = f.consulta_bd(SQL_GRUPO)
    df = pd.DataFrame(grupo,columns=["codgrupo", "cnpj", "codpermissao", "codtpusuario", "codtransacao", "dsgrupo"])
    js = df.to_json(orient='records')
    js = js.replace('[', '').replace(']', '').replace('/', '').replace('\\', '')
    if js == [] or js == None or js == '':
        return {
            "body": f"'{js}'",
            "success": "true"
        }
    else: ## AJUSTAR PARA MOSTRAR CPF ESPECIFICO E QUANDO NÃO TIVER CADASTRO
        cnpj = json['cnpj']
        SQL_CONSULTA = f"select cnpj from grupo where cnpj = '{cnpj}'"
        resultado = f.consulta_bd(SQL_CONSULTA)
        df2 = pd.DataFrame(resultado,columns=["cnpj"])
        js2 = df2.to_json(orient='records')
        js2 = js2.replace('[', '').replace(']', '').replace('/', '').replace('\\', '')
        print(js2)
        if js2 == [] or js2 == None or js2 == '':
            return {
                "success": "false",
                "message": "Cnpj nao cadastrado. Por favor, tente outro."
            }
        else:
            return {
                "body": f"'{js2}'",
                "success": "true"
            }


@app.route('/registerGroup', methods=['POST'])   ##################
def registerGroup():
    json = request.get_json()
    cnpj = json['cnpj']
    codpermissao = json['codpermissao']
    codtpusuario = json['codtpusuario']
    codtransacao = json['codtransacao']
    dsgrupo = json['dsgrupo']
    SQL_CNPJ = f"select * from grupo where cnpj = '{cnpj}'"
    consulta = f.consulta_bd(SQL_CNPJ)
    df = pd.DataFrame(consulta, columns=["cnpj", "codpermissao", "codtpusuario", "codtransacao", "dsgrupo"])
    js = df.to_json(orient='records')
    js = js.replace('[', '').replace(']', '').replace('/', '').replace('\\', '')
    if js == [] or js == None or js == '':
        SQL_CRIA = f"insert into grupo (cnpj, codpermissao, codtpusuario, codtransacao, dsgrupo) values ('{cnpj}', '{codpermissao}', '{codtpusuario}', '{codtransacao}', '{dsgrupo}')"
        f.inserir_bd(SQL_CRIA)
        return {
            "body": f"'{cnpj}', '{codpermissao}', '{codtpusuario}', '{codtransacao}', '{dsgrupo}'",
            "success": "true",
            "message": f"Grupo cadastrado com sucesso'"
        }
    else:
        return {
            "body": f"{cnpj}",
            "success": "false",
            "message": f"O grupo '{cnpj}' informado já se encontra cadastrado."
        }



@app.route('/deleteGroup', methods=['POST'])
def deleteGroup():
    json = request.get_json()
    cnpj = json['cnpj']
    SQL_DELETA = f"delete from grupo where cnpj = '{cnpj}'"
    f.deletar_bd(SQL_DELETA)
    return {
        "body": f"{cnpj}",
        "success": "true",
        "message": f"O grupo '{cnpj}' deletado com sucesso'"
    }



@app.route('/updateGroup', methods=['POST'])
def updateGroup():
    json = request.get_json()
    cnpj = json['cnpj']
    codpermissao = json['codpermissao']
    codtpusuario = json['codtpusuario']
    codtransacao = json['codtransacao']
    dsgrupo = json['dsgrupo']
    SQL_UPDATE = f"update grupo set codpermissao = '{codpermissao}',  codtpusuario = '{codtpusuario}', codtransacao = '{codtransacao}', dsgrupo = '{dsgrupo}'  where cnpj = '{cnpj}'"
    f.editar_bd(SQL_UPDATE)
    return {
        "body": f"{razaosocial}', {responsavel}, {telefone}, {email}, {datainicio}, {datafim},'{cnpj}'",
        "success": "true",
        "message": f"Cliente '{cnpj}' alterado com sucesso'"
    }


################################### Tipos de Autorização #################################################

@app.route('/selectAutType', methods=['POST'])
def selectAutType():
    json = request.get_json()
    dsautorizacao = json['dsautorizacao']
    SQL_TIPO = f"select * from tipos_autorizacao where dsautorizacao = '{dsautorizacao}'"
    resultado = f.consulta_bd(SQL_TIPO)
    df = pd.DataFrame(resultado,columns=["codautorizacao","dsautorizacao","codtransacao"])
    js = df.to_json(orient='records')
    js = js.replace('[', '').replace(']', '').replace('/', '').replace('\\', '')
    print(js)
    if js == [] or js == None or js == '':
        return {
            "success": "false",
            "message": "Descricao nao cadastrada no sistema. Por favor, tente outra."
        }
    else:
        return {
            "body": f"{js}",
            "success": "true"
        }



@app.route('/registerAutType', methods=['POST'])
def registerAutType():
    json = request.get_json()
    dsautorizacao = json['dsautorizacao']
    SQL_INSERCAO = f"INSERT INTO public.controle_operacao(tipooperacao, datainicio, datafim) VALUES ('incluir', '{data_hoje_normal}', '{data_hoje_normal}')"
    f.inserir_bd(SQL_INSERCAO)
    SQL_COD = f"select codtransacao from controle_operacao order by codtransacao desc limit 1"
    consulta = f.consulta_bd(SQL_COD)
    df = pd.DataFrame(consulta, columns=["codtransacao"])
    codtransacao = df['codtransacao'][0]
    if codtransacao != [] or codtransacao != None or codtransacao != '':
        SQL_INSERE = f"insert into tipos_autorizacao (codtransacao, dsautorizacao) values ({codtransacao}, '{dsautorizacao}')"
        f.inserir_bd(SQL_INSERE)
        return {
            "body": f"{codtransacao},'{dsautorizacao}'",
            "success": "true",
            "message": f"Cadastro realizado com sucesso'"
        }
    else:
        return {
            "success": "false",
            "message": f"Transacao ja cadastrada."
        }



@app.route('/deleteAutType', methods=['POST'])
def deleteAutType():
    json = request.get_json()
    dsautorizacao = json['dsautorizacao']
    codtransacao = json['codtransacao']
    SQL_DELETA = f"delete from tipos_autorizacao where dsautorizacao = '{dsautorizacao}'"
    return 'Deletar Usuario'





@app.route('/updateAutType', methods=['POST'])
def updateAutType():
    json = request.get_json()
    dsautorizacao = json['dsautorizacao']
    SQL_INSERCAO = f"INSERT INTO public.controle_operacao(tipooperacao, datainicio, datafim) VALUES ('alterar', '{data_hoje_normal}', '{data_hoje_normal}')"
    f.inserir_bd(SQL_INSERCAO)
    SQL_COD = f"select codtransacao from controle_operacao order by codtransacao desc limit 1"
    consulta = f.consulta_bd(SQL_COD)
    df = pd.DataFrame(consulta, columns=["codtransacao"])
    codtransacao = df['codtransacao'][0]
    if codtransacao != [] or codtransacao != None or codtransacao != '':
        dsautorizacao = json['dsautorizacao']
        SQL_UPDATE = f"update tipos_autorizacao set dsautorizacao = '{dsautorizacao}' where codautorizacao = '{codtransacao}'"
        f.editar_bd(SQL_UPDATE)
        return {
            "body": f"{dsautorizacao}'",
            "success": "true",
            "message": f"Autorizacao alterada com sucesso'"
        }
    else:
        return {
            "success": "false",
            "message": f"Autorizacao nao encontrada. Tente outro."
        }



################################### Recebe Json #################################################
@app.route('/submitFlow', methods=['POST'])
def submitFlow():
    req = request.get_json()
    js_string = json.dumps(req)
    with open('01Saida/fluxo.json', 'w') as outfile:
        outfile.write(js_string)
    api = { "result":"OK" }

    return jsonify(api)


######################## Verifica Telefones diferentes ##################################
@app.route('/verifyPhone', methods=['POST'])
def verifyPhone():
    req = request.get_json()
    js_string = json.dumps(req)
    #Pega o CNPJ
    #Busca na tabela de empresas
    #Pega o nome da empresa
    #Busca no database ingenium pelo nome da empresa
    #Verifica os telefones existentes no servicein
    api = {}

    return jsonify(api)

######################## Verifica Empresa a5digital ##################################
@app.route('/verifyCompany', methods=['POST'])
def verifyCompany():
    req = request.get_json()
    js_string = json.dumps(req)
    #Consulta Empresas disponiveis no Servidor
    api = {}

    return jsonify(api)


if __name__ == '__main__':   
    app.run(debug=True, port=4000)